<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:schema="https://schema.org/"
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:map="http://www.w3.org/2005/xpath-functions/map"
                version="3.0">
   <xsl:variable name="wikidata_pers_ref" as="map(xs:string, xs:string)">
      <xsl:map>
         <xsl:map-entry key="'AbbotCharles'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q334391.rdf'"/>
         <xsl:map-entry key="'AdamsonJohn'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q6218239.rdf'"/>
         <xsl:map-entry key="'Adderley'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q76119869.rdf'"/>
         <xsl:map-entry key="'AikinArthur'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q708372.rdf'"/>
         <xsl:map-entry key="'AllstonWashington'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q468272.rdf'"/>
         <xsl:map-entry key="'AmyotThomas'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q7787058.rdf'"/>
         <xsl:map-entry key="'ArrowsmithAaron'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q301678.rdf'"/>
         <xsl:map-entry key="'BallantyneJames'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q6129308.rdf'"/>
         <xsl:map-entry key="'BallantyneJohn'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q6220475.rdf'"/>
         <xsl:map-entry key="'BarbauldAnnaLetitia'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q268305.rdf'"/>
         <xsl:map-entry key="'BeaumontSirGeorge'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q7526834.rdf'"/>
         <xsl:map-entry key="'BeddoesAnnaMaria'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q65080734.rdf'"/>
         <xsl:map-entry key="'BeddoesThomas'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q2422365.rdf'"/>
         <xsl:map-entry key="'BedfordGrosvenorCharles'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q63306855.rdf'"/>
         <xsl:map-entry key="'BethamMatilda'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q6780272.rdf'"/>
         <xsl:map-entry key="'BloomfieldRobert'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q3434598.rdf'"/>
         <xsl:map-entry key="'BowlesWilliamLisle'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q5584023.rdf'"/>
         <xsl:map-entry key="'BrittonJohn'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q15462617.rdf'"/>
         <xsl:map-entry key="'BroughamHenryPeter'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q333220.rdf'"/>
         <xsl:map-entry key="'BrydgesSirEgerton'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q168890.rdf'"/>
         <xsl:map-entry key="'BunburyCharlesJohn'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q52154432.rdf'"/>
         <xsl:map-entry key="'BunburySirHenryEdward'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q7527111.rdf'"/>
         <xsl:map-entry key="'BunburyHenryWilliam'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q3133158.rdf'"/>
         <xsl:map-entry key="'Burdett'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q334207.rdf'"/>
         <xsl:map-entry key="'BurnettGeorge'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q5537444.rdf'"/>
         <xsl:map-entry key="'BurneyJames'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q615435.rdf'"/>
         <xsl:map-entry key="'ButlerCharles'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q2958645.rdf'"/>
         <xsl:map-entry key="'ButlerEleanor'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q18528457.rdf'"/>
         <xsl:map-entry key="'Byron'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q5679.rdf'"/>
         <xsl:map-entry key="'CadellThomas'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q18528913.rdf'"/>
         <xsl:map-entry key="'CanningGeorge'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q219731.rdf'"/>
         <xsl:map-entry key="'CarlisleAnthony'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q572978.rdf'"/>
         <xsl:map-entry key="'CenaculoManuel'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q4902673.rdf'"/>
         <xsl:map-entry key="'ClarksonCatherine'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q94764642.rdf'"/>
         <xsl:map-entry key="'ClarksonThomas'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q2032151.rdf'"/>
         <xsl:map-entry key="'CobbettWilliam'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q2141938.rdf'"/>
         <xsl:map-entry key="'ColburnHenry'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q2846305.rdf'"/>
         <xsl:map-entry key="'ColeridgeDavidHartley'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q960709.rdf'"/>
         <xsl:map-entry key="'ColeridgeDerwent'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q5263323.rdf'"/>
         <xsl:map-entry key="'ColeridgeGeorge'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q75611530.rdf'"/>
         <xsl:map-entry key="'ColeridgeJohnTaylor'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q6260401.rdf'"/>
         <xsl:map-entry key="'ColeridgeSamuelTaylor'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q82409.rdf'"/>
         <xsl:map-entry key="'ColeridgeSaraSTCdaughter'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q1960565.rdf'"/>
         <xsl:map-entry key="'ColeridgeWilliamHart'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q8006984.rdf'"/>
         <xsl:map-entry key="'ConderJosiah'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q6290456.rdf'"/>
         <xsl:map-entry key="'CorryIsaac'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q6076170.rdf'"/>
         <xsl:map-entry key="'CottleAmos'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q4747950.rdf'"/>
         <xsl:map-entry key="'CottleJoseph'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q6282295.rdf'"/>
         <xsl:map-entry key="'CroftHerbert'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q3133445.rdf'"/>
         <xsl:map-entry key="'CrokerJohnWilson'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q320849.rdf'"/>
         <xsl:map-entry key="'DavyHumphry'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q131761.rdf'"/>
         <xsl:map-entry key="'DavyJane'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q17353839.rdf'"/>
         <xsl:map-entry key="'DeQuinceyThomas'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q317160.rdf'"/>
         <xsl:map-entry key="'DoddJamesWilliam'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q16237408.rdf'"/>
         <xsl:map-entry key="'DuppaRichard'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q76228633.rdf'"/>
         <xsl:map-entry key="'DyerGeorge'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q5538694.rdf'"/>
         <xsl:map-entry key="'EdridgePainter'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q4529914.rdf'"/>
         <xsl:map-entry key="'EllisGeorge'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q5538967.rdf'"/>
         <xsl:map-entry key="'ElmsleyPeter'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q7173848.rdf'"/>
         <xsl:map-entry key="'ErskineHenry'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q5720908.rdf'"/>
         <xsl:map-entry key="'EstlinJohnPrior'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q6253450.rdf'"/>
         <xsl:map-entry key="'FlowerBenjamin'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q4888593.rdf'"/>
         <xsl:map-entry key="'FoxCharlesJames'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q438131.rdf'"/>
         <xsl:map-entry key="'FoxElizabethVassall'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q5362794.rdf'"/>
         <xsl:map-entry key="'FoxHenryRichard'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q723938.rdf'"/>
         <xsl:map-entry key="'FrereJohnHookham'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q6239771.rdf'"/>
         <xsl:map-entry key="'FrickerEdith'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q75440491.rdf'"/>
         <xsl:map-entry key="'FrickerSarah'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q75611558.rdf'"/>
         <xsl:map-entry key="'GeorgePrinceRegent'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q130586.rdf'"/>
         <xsl:map-entry key="'GiffordWilliam'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q331198.rdf'"/>
         <xsl:map-entry key="'GodwinWilliam'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q188569.rdf'"/>
         <xsl:map-entry key="'GoochDrRobert'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q7344843.rdf'"/>
         <xsl:map-entry key="'GrahameJames'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q6134848.rdf'"/>
         <xsl:map-entry key="'Gutch'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q6247146.rdf'"/>
         <xsl:map-entry key="'HaslewoodJoseph'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q6283838.rdf'"/>
         <xsl:map-entry key="'HaysMary'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q1797831.rdf'"/>
         <xsl:map-entry key="'HazlittWilliam'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q126596.rdf'"/>
         <xsl:map-entry key="'HeberReginald'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q3271284.rdf'"/>
         <xsl:map-entry key="'HeberRichard'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q4502479.rdf'"/>
         <xsl:map-entry key="'HerriesJohnCharles'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q332824.rdf'"/>
         <xsl:map-entry key="'HillThomas'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q18672508.rdf'"/>
         <xsl:map-entry key="'HoggEdward'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q94912478.rdf'"/>
         <xsl:map-entry key="'HoodAlexanderViscount'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q332644.rdf'"/>
         <xsl:map-entry key="'HookJames'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q15072644.rdf'"/>
         <xsl:map-entry key="'HuntLeigh'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q655213.rdf'"/>
         <xsl:map-entry key="'HutchinsonSara'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q52119200.rdf'"/>
         <xsl:map-entry key="'JamesPaulMoon'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q7152575.rdf'"/>
         <xsl:map-entry key="'JardineDavid'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q5235595.rdf'"/>
         <xsl:map-entry key="'JeffreyFrancis'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q5881202.rdf'"/>
         <xsl:map-entry key="'JenningsJames'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q18917276.rdf'"/>
         <xsl:map-entry key="'JervisAdmiral'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q335254.rdf'"/>
         <xsl:map-entry key="'KenyonJohn'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q3181862.rdf'"/>
         <xsl:map-entry key="'KiddJohn'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q723419.rdf'"/>
         <xsl:map-entry key="'KingMrs'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q75490272.rdf'"/>
         <xsl:map-entry key="'KnightonSirWilliam'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q8014086.rdf'"/>
         <xsl:map-entry key="'LambCharles'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q372984.rdf'"/>
         <xsl:map-entry key="'LambThomasDavis'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q26453518.rdf'"/>
         <xsl:map-entry key="'LambThomasPhillipps'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q26453395.rdf'"/>
         <xsl:map-entry key="'LancasterJoseph'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q723877.rdf'"/>
         <xsl:map-entry key="'LandorRobertEyres'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q7344149.rdf'"/>
         <xsl:map-entry key="'LandorWalterSavage'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q645138.rdf'"/>
         <xsl:map-entry key="'LittletonEdwardJohn'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q7526589.rdf'"/>
         <xsl:map-entry key="'LloydCharles'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q5080292.rdf'"/>
         <xsl:map-entry key="'LockerEdwardHawke'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q2917781.rdf'"/>
         <xsl:map-entry key="'LongmanThomas'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q18672605.rdf'"/>
         <xsl:map-entry key="'LoshJames'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q6138234.rdf'"/>
         <xsl:map-entry key="'LovellRobert'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q7347010.rdf'"/>
         <xsl:map-entry key="'LowtherWilliam'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q8014746.rdf'"/>
         <xsl:map-entry key="'MayJohn'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q6247303.rdf'"/>
         <xsl:map-entry key="'MitfordJohn'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q6249024.rdf'"/>
         <xsl:map-entry key="'MontaguBasil'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q4867224.rdf'"/>
         <xsl:map-entry key="'MontgomeryJames'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q3269483.rdf'"/>
         <xsl:map-entry key="'MoreHannah'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q25880.rdf'"/>
         <xsl:map-entry key="'MurrayJohn'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q4310130.rdf'"/>
         <xsl:map-entry key="'NaresRobert'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q7347956.rdf'"/>
         <xsl:map-entry key="'NashEdward'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q37520571.rdf'"/>
         <xsl:map-entry key="'NealeCornelius'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q5171425.rdf'"/>
         <xsl:map-entry key="'NicholsJohn'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q6250441.rdf'"/>
         <xsl:map-entry key="'OpieAmelia'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q1076511.rdf'"/>
         <xsl:map-entry key="'ParsonsJohn'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q6251990.rdf'"/>
         <xsl:map-entry key="'ParsonsWilliam'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q76333024.rdf'"/>
         <xsl:map-entry key="'PeggeChristopher'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q14945450.rdf'"/>
         <xsl:map-entry key="'PembertonSophia'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q96078058.rdf'"/>
         <xsl:map-entry key="'PercevalSpencer'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q233992.rdf'"/>
         <xsl:map-entry key="'PhillimoreJoseph'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q6286234.rdf'"/>
         <xsl:map-entry key="'PhillipsRichard'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q7528648.rdf'"/>
         <xsl:map-entry key="'PonsonbySarah'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q18759402.rdf'"/>
         <xsl:map-entry key="'PooleThomas'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q18759411.rdf'"/>
         <xsl:map-entry key="'ProbyJohnCarysfort'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q6253475.rdf'"/>
         <xsl:map-entry key="'ProbyWilliam'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q8017103.rdf'"/>
         <xsl:map-entry key="'PugheWilliamOwen'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q8016424.rdf'"/>
         <xsl:map-entry key="'ReesOwen'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q18674291.rdf'"/>
         <xsl:map-entry key="'ReesThomas'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q7793450.rdf'"/>
         <xsl:map-entry key="'ReeveHenry'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q18671457.rdf'"/>
         <xsl:map-entry key="'ReeveSusan'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q19802364.rdf'"/>
         <xsl:map-entry key="'RickmanJohn'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q6254862.rdf'"/>
         <xsl:map-entry key="'RobertsBarreCharles'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q18592951.rdf'"/>
         <xsl:map-entry key="'RobinsonHenryCrabb'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q1989484.rdf'"/>
         <xsl:map-entry key="'RoscoeWilliam'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q2635090.rdf'"/>
         <xsl:map-entry key="'RoughWilliam'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q18784061.rdf'"/>
         <xsl:map-entry key="'RushtonEdward'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q5345137.rdf'"/>
         <xsl:map-entry key="'SayersFrank'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q5489424.rdf'"/>
         <xsl:map-entry key="'ScottWalter'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q79025.rdf'"/>
         <xsl:map-entry key="'ScottMargaretCharlotte'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q42861192.rdf'"/>
         <xsl:map-entry key="'SewardAnna'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q1131816.rdf'"/>
         <xsl:map-entry key="'SharpRichard'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q7328980.rdf'"/>
         <xsl:map-entry key="'ShelleyPercyBysshe'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q93343.rdf'"/>
         <xsl:map-entry key="'SouthcottJoanna'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q449005.rdf'"/>
         <xsl:map-entry key="'SoutheyBertha'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q75440501.rdf'"/>
         <xsl:map-entry key="'SoutheyEdithMay'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q75440497.rdf'"/>
         <xsl:map-entry key="'SoutheyEmma'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q75440500.rdf'"/>
         <xsl:map-entry key="'SoutheyHerbert'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q75440498.rdf'"/>
         <xsl:map-entry key="'SoutheyIsabel'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q75440504.rdf'"/>
         <xsl:map-entry key="'SoutheyMargaretEdithdau'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q75440495.rdf'"/>
         <xsl:map-entry key="'StuartDaniel'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q5218862.rdf'"/>
         <xsl:map-entry key="'TaylorHenrySir'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q1607302.rdf'"/>
         <xsl:map-entry key="'TaylorWilliam'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q2580729.rdf'"/>
         <xsl:map-entry key="'ThelwallJohn'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q519393.rdf'"/>
         <xsl:map-entry key="'TobinJamesWebbe'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q19325079.rdf'"/>
         <xsl:map-entry key="'TurnerSharon'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q3481531.rdf'"/>
         <xsl:map-entry key="'VardonMr'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q18759421.rdf'"/>
         <xsl:map-entry key="'VincentWilliam'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q8019736.rdf'"/>
         <xsl:map-entry key="'WakefieldGilbert'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q3105890.rdf'"/>
         <xsl:map-entry key="'WatsonRichardBishopLlandaff'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q7329838.rdf'"/>
         <xsl:map-entry key="'WedgwoodThomas'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q933545.rdf'"/>
         <xsl:map-entry key="'WellesleyArthurWellington'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q131691.rdf'"/>
         <xsl:map-entry key="'WellesleyMarquis'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q335205.rdf'"/>
         <xsl:map-entry key="'WestallWilliam'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q8020218.rdf'"/>
         <xsl:map-entry key="'WhiteHenryKirke'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q5724396.rdf'"/>
         <xsl:map-entry key="'WhiteJosephBlanco'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q3047406.rdf'"/>
         <xsl:map-entry key="'WilberforceWilliam'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q207672.rdf'"/>
         <xsl:map-entry key="'WordsworthCatherine'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q52154726.rdf'"/>
         <xsl:map-entry key="'WordsworthDorothy'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q448992.rdf'"/>
         <xsl:map-entry key="'WordsworthMary'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q22248691.rdf'"/>
         <xsl:map-entry key="'WordsworthThomas'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q75462137.rdf'"/>
         <xsl:map-entry key="'WordsworthWilliam'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q45546.rdf'"/>
         <xsl:map-entry key="'WynnCharlesWW'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q213150.rdf'"/>
         <xsl:map-entry key="'WynnSirWatkinWilliams'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q7529424.rdf'"/>
      </xsl:map>
   </xsl:variable>
   <xsl:variable name="getty_pers_ref" as="map(xs:string, xs:string)">
      <xsl:map>
         <xsl:map-entry key="'AllstonWashington'"
                        select="'http://vocab.getty.edu/ulan/500016005.rdf'"/>
         <xsl:map-entry key="'BeaumontSirGeorge'"
                        select="'http://vocab.getty.edu/ulan/500019648.rdf'"/>
         <xsl:map-entry key="'BethamMatilda'"
                        select="'http://vocab.getty.edu/ulan/500027213.rdf'"/>
         <xsl:map-entry key="'BrittonJohn'"
                        select="'http://vocab.getty.edu/ulan/500004450.rdf'"/>
         <xsl:map-entry key="'BunburyHenryWilliam'"
                        select="'http://vocab.getty.edu/ulan/500013604.rdf'"/>
         <xsl:map-entry key="'Byron'" select="'http://vocab.getty.edu/ulan/500254532.rdf'"/>
         <xsl:map-entry key="'ColeridgeSamuelTaylor'"
                        select="'http://vocab.getty.edu/ulan/500287105.rdf'"/>
         <xsl:map-entry key="'EdridgePainter'"
                        select="'http://vocab.getty.edu/ulan/500017407.rdf'"/>
         <xsl:map-entry key="'GeorgePrinceRegent'"
                        select="'http://vocab.getty.edu/ulan/500234768.rdf'"/>
         <xsl:map-entry key="'HazlittWilliam'"
                        select="'http://vocab.getty.edu/ulan/500016253.rdf'"/>
         <xsl:map-entry key="'LancasterJoseph'"
                        select="'http://vocab.getty.edu/ulan/500250439.rdf'"/>
         <xsl:map-entry key="'LockerEdwardHawke'"
                        select="'http://vocab.getty.edu/ulan/500014392.rdf'"/>
         <xsl:map-entry key="'NashEdward'" select="'http://vocab.getty.edu/ulan/500011701.rdf'"/>
         <xsl:map-entry key="'RoscoeWilliam'"
                        select="'http://vocab.getty.edu/ulan/500325119.rdf'"/>
         <xsl:map-entry key="'ScottWalter'"
                        select="'http://vocab.getty.edu/ulan/500069589.rdf'"/>
         <xsl:map-entry key="'ShelleyPercyBysshe'"
                        select="'http://vocab.getty.edu/ulan/500005193.rdf'"/>
         <xsl:map-entry key="'TaylorWilliam'"
                        select="'http://vocab.getty.edu/ulan/500185471.rdf'"/>
         <xsl:map-entry key="'WedgwoodThomas'"
                        select="'http://vocab.getty.edu/ulan/500347932.rdf'"/>
         <xsl:map-entry key="'WellesleyArthurWellington'"
                        select="'http://vocab.getty.edu/ulan/500088735.rdf'"/>
         <xsl:map-entry key="'WestallWilliam'"
                        select="'http://vocab.getty.edu/ulan/500017672.rdf'"/>
         <xsl:map-entry key="'WordsworthWilliam'"
                        select="'http://vocab.getty.edu/ulan/500071872.rdf'"/>
         <xsl:map-entry key="'BloomfieldRobert'"
                        select="'http://vocab.getty.edu/ulan/500447835'"/>
         <xsl:map-entry key="'ButlerEleanor'" select="'http://vocab.getty.edu/ulan/500197716'"/>
         <xsl:map-entry key="'FoxCharlesJames'"
                        select="'http://vocab.getty.edu/ulan/500011218'"/>
         <xsl:map-entry key="'HeberRichard'" select="'http://vocab.getty.edu/ulan/500441225'"/>
         <xsl:map-entry key="'HertfordLord'" select="'http://vocab.getty.edu/ulan/500434248'"/>
         <xsl:map-entry key="'HoggJamesEttrick'"
                        select="'http://vocab.getty.edu/ulan/500043631'"/>
         <xsl:map-entry key="'LongmanThomas'" select="'http://vocab.getty.edu/ulan/500438945'"/>
         <xsl:map-entry key="'LowtherWilliam'" select="'http://vocab.getty.edu/ulan/500435511'"/>
         <xsl:map-entry key="'SharpRichard'" select="'http://vocab.getty.edu/ulan/500442835'"/>
         <xsl:map-entry key="'SmithThomas'" select="'http://vocab.getty.edu/ulan/500028003'"/>
         <xsl:map-entry key="'ThelwallJohn'" select="'http://vocab.getty.edu/ulan/500441234'"/>
         <xsl:map-entry key="'WilkinsonThomas'"
                        select="'http://vocab.getty.edu/ulan/500283230'"/>
         <xsl:map-entry key="'WynnSirWatkinWilliams'"
                        select="'http://vocab.getty.edu/ulan/500436053'"/>
      </xsl:map>
   </xsl:variable>
   <xsl:variable name="viaf_pers_ref" as="map(xs:string, xs:string)">
      <xsl:map>
         <xsl:map-entry key="'AbbotCharles'" select="'https://viaf.org/viaf/13211719/rdf.xml'"/>
         <xsl:map-entry key="'AdamsonJohn'" select="'https://viaf.org/viaf/95319619/rdf.xml'"/>
         <xsl:map-entry key="'AikinArthur'" select="'https://viaf.org/viaf/69999076/rdf.xml'"/>
         <xsl:map-entry key="'AllstonWashington'"
                        select="'https://viaf.org/viaf/57408203/rdf.xml'"/>
         <xsl:map-entry key="'AmyotThomas'" select="'https://viaf.org/viaf/61687473/rdf.xml'"/>
         <xsl:map-entry key="'ArrowsmithAaron'"
                        select="'https://viaf.org/viaf/18101110/rdf.xml'"/>
         <xsl:map-entry key="'BallantyneJames'"
                        select="'https://viaf.org/viaf/9402397/rdf.xml'"/>
         <xsl:map-entry key="'BallantyneJohn'"
                        select="'https://viaf.org/viaf/78334777/rdf.xml'"/>
         <xsl:map-entry key="'BarbauldAnnaLetitia'"
                        select="'https://viaf.org/viaf/9896232/rdf.xml'"/>
         <xsl:map-entry key="'BeaumontSirGeorge'"
                        select="'https://viaf.org/viaf/69726932/rdf.xml'"/>
         <xsl:map-entry key="'BeddoesThomas'" select="'https://viaf.org/viaf/64080402/rdf.xml'"/>
         <xsl:map-entry key="'BethamMatilda'" select="'https://viaf.org/viaf/64734447/rdf.xml'"/>
         <xsl:map-entry key="'BloomfieldRobert'"
                        select="'https://viaf.org/viaf/74761131/rdf.xml'"/>
         <xsl:map-entry key="'BowlesWilliamLisle'"
                        select="'https://viaf.org/viaf/696177/rdf.xml'"/>
         <xsl:map-entry key="'BrittonJohn'" select="'https://viaf.org/viaf/15044364/rdf.xml'"/>
         <xsl:map-entry key="'BroughamHenryPeter'"
                        select="'https://viaf.org/viaf/34592194/rdf.xml'"/>
         <xsl:map-entry key="'BrydgesSirEgerton'"
                        select="'https://viaf.org/viaf/27405359/rdf.xml'"/>
         <xsl:map-entry key="'BunburySirHenryEdward'"
                        select="'https://viaf.org/viaf/69790749/rdf.xml'"/>
         <xsl:map-entry key="'BunburyHenryWilliam'"
                        select="'https://viaf.org/viaf/7616263/rdf.xml'"/>
         <xsl:map-entry key="'Burdett'" select="'https://viaf.org/viaf/75338116/rdf.xml'"/>
         <xsl:map-entry key="'BurnettGeorge'" select="'https://viaf.org/viaf/96796088/rdf.xml'"/>
         <xsl:map-entry key="'BurneyJames'" select="'https://viaf.org/viaf/64345371/rdf.xml'"/>
         <xsl:map-entry key="'ButlerCharles'" select="'https://viaf.org/viaf/37031130/rdf.xml'"/>
         <xsl:map-entry key="'ButlerEleanor'" select="'https://viaf.org/viaf/53076588/rdf.xml'"/>
         <xsl:map-entry key="'Byron'" select="'https://viaf.org/viaf/95230688/rdf.xml'"/>
         <xsl:map-entry key="'CadellThomas'" select="'https://viaf.org/viaf/120729088/rdf.xml'"/>
         <xsl:map-entry key="'CanningGeorge'" select="'https://viaf.org/viaf/7443935/rdf.xml'"/>
         <xsl:map-entry key="'CarlisleAnthony'"
                        select="'https://viaf.org/viaf/32387457/rdf.xml'"/>
         <xsl:map-entry key="'CenaculoManuel'"
                        select="'https://viaf.org/viaf/66518340/rdf.xml'"/>
         <xsl:map-entry key="'ClarksonCatherine'"
                        select="'https://viaf.org/viaf/839145857928623020410/rdf.xml'"/>
         <xsl:map-entry key="'ClarksonThomas'"
                        select="'https://viaf.org/viaf/76414258/rdf.xml'"/>
         <xsl:map-entry key="'CobbettWilliam'"
                        select="'https://viaf.org/viaf/101963169/rdf.xml'"/>
         <xsl:map-entry key="'ColburnHenry'" select="'https://viaf.org/viaf/90644401/rdf.xml'"/>
         <xsl:map-entry key="'ColeridgeDavidHartley'"
                        select="'https://viaf.org/viaf/40220624/rdf.xml'"/>
         <xsl:map-entry key="'ColeridgeDerwent'"
                        select="'https://viaf.org/viaf/10629861/rdf.xml'"/>
         <xsl:map-entry key="'ColeridgeJohnTaylor'"
                        select="'https://viaf.org/viaf/34513362/rdf.xml'"/>
         <xsl:map-entry key="'ColeridgeSamuelTaylor'"
                        select="'https://viaf.org/viaf/24599809/rdf.xml'"/>
         <xsl:map-entry key="'ColeridgeSaraSTCdaughter'"
                        select="'https://viaf.org/viaf/29578781/rdf.xml'"/>
         <xsl:map-entry key="'ColeridgeWilliamHart'"
                        select="'https://viaf.org/viaf/25945682/rdf.xml'"/>
         <xsl:map-entry key="'ConderJosiah'" select="'https://viaf.org/viaf/50069134/rdf.xml'"/>
         <xsl:map-entry key="'CorryIsaac'" select="'https://viaf.org/viaf/11327534/rdf.xml'"/>
         <xsl:map-entry key="'CottleAmos'" select="'https://viaf.org/viaf/73901862/rdf.xml'"/>
         <xsl:map-entry key="'CottleJoseph'" select="'https://viaf.org/viaf/22196268/rdf.xml'"/>
         <xsl:map-entry key="'CroftHerbert'" select="'https://viaf.org/viaf/64335678/rdf.xml'"/>
         <xsl:map-entry key="'CrokerJohnWilson'"
                        select="'https://viaf.org/viaf/51766984/rdf.xml'"/>
         <xsl:map-entry key="'DavyHumphry'" select="'https://viaf.org/viaf/29630108/rdf.xml'"/>
         <xsl:map-entry key="'DavyJane'" select="'https://viaf.org/viaf/186845362/rdf.xml'"/>
         <xsl:map-entry key="'DeQuinceyThomas'"
                        select="'https://viaf.org/viaf/14768427/rdf.xml'"/>
         <xsl:map-entry key="'DoddJamesWilliam'"
                        select="'https://viaf.org/viaf/44194606/rdf.xml'"/>
         <xsl:map-entry key="'DyerGeorge'" select="'https://viaf.org/viaf/13484941/rdf.xml'"/>
         <xsl:map-entry key="'EdridgePainter'"
                        select="'https://viaf.org/viaf/33149994/rdf.xml'"/>
         <xsl:map-entry key="'EllisGeorge'" select="'https://viaf.org/viaf/27099724/rdf.xml'"/>
         <xsl:map-entry key="'ElmsleyPeter'" select="'https://viaf.org/viaf/3386202/rdf.xml'"/>
         <xsl:map-entry key="'ErskineHenry'" select="'https://viaf.org/viaf/235152625/rdf.xml'"/>
         <xsl:map-entry key="'EstlinJohnPrior'"
                        select="'https://viaf.org/viaf/55387812/rdf.xml'"/>
         <xsl:map-entry key="'FlowerBenjamin'"
                        select="'https://viaf.org/viaf/34302718/rdf.xml'"/>
         <xsl:map-entry key="'FoxCharlesJames'"
                        select="'https://viaf.org/viaf/39462521/rdf.xml'"/>
         <xsl:map-entry key="'FoxElizabethVassall'"
                        select="'https://viaf.org/viaf/74223582/rdf.xml'"/>
         <xsl:map-entry key="'FoxHenryRichard'"
                        select="'https://viaf.org/viaf/56654210/rdf.xml'"/>
         <xsl:map-entry key="'FrereJohnHookham'"
                        select="'https://viaf.org/viaf/1141974/rdf.xml'"/>
         <xsl:map-entry key="'GeorgePrinceRegent'"
                        select="'https://viaf.org/viaf/265481029/rdf.xml'"/>
         <xsl:map-entry key="'GiffordWilliam'"
                        select="'https://viaf.org/viaf/12297538/rdf.xml'"/>
         <xsl:map-entry key="'GodwinWilliam'" select="'https://viaf.org/viaf/68929729/rdf.xml'"/>
         <xsl:map-entry key="'GoochDrRobert'" select="'https://viaf.org/viaf/61943023/rdf.xml'"/>
         <xsl:map-entry key="'GrahameJames'" select="'https://viaf.org/viaf/71433779/rdf.xml'"/>
         <xsl:map-entry key="'Gutch'" select="'https://viaf.org/viaf/29768912/rdf.xml'"/>
         <xsl:map-entry key="'HaslewoodJoseph'"
                        select="'https://viaf.org/viaf/27420814/rdf.xml'"/>
         <xsl:map-entry key="'HaysMary'" select="'https://viaf.org/viaf/29629662/rdf.xml'"/>
         <xsl:map-entry key="'HazlittWilliam'"
                        select="'https://viaf.org/viaf/87145187/rdf.xml'"/>
         <xsl:map-entry key="'HeberReginald'" select="'https://viaf.org/viaf/353958/rdf.xml'"/>
         <xsl:map-entry key="'HeberRichard'" select="'https://viaf.org/viaf/5314321/rdf.xml'"/>
         <xsl:map-entry key="'HerriesJohnCharles'"
                        select="'https://viaf.org/viaf/13857642/rdf.xml'"/>
         <xsl:map-entry key="'HillThomas'" select="'https://viaf.org/viaf/24163783/rdf.xml'"/>
         <xsl:map-entry key="'HoggEdward'" select="'https://viaf.org/viaf/35236569/rdf.xml'"/>
         <xsl:map-entry key="'HoodAlexanderViscount'"
                        select="'https://viaf.org/viaf/295316362/rdf.xml'"/>
         <xsl:map-entry key="'HookJames'" select="'https://viaf.org/viaf/44633875/rdf.xml'"/>
         <xsl:map-entry key="'HuntLeigh'" select="'https://viaf.org/viaf/54166412/rdf.xml'"/>
         <xsl:map-entry key="'HutchinsonSara'"
                        select="'https://viaf.org/viaf/39611923/rdf.xml'"/>
         <xsl:map-entry key="'JamesPaulMoon'" select="'https://viaf.org/viaf/1557877/rdf.xml'"/>
         <xsl:map-entry key="'JardineDavid'" select="'https://viaf.org/viaf/74201130/rdf.xml'"/>
         <xsl:map-entry key="'JeffreyFrancis'"
                        select="'https://viaf.org/viaf/52485486/rdf.xml'"/>
         <xsl:map-entry key="'JenningsJames'"
                        select="'https://viaf.org/viaf/130605012/rdf.xml'"/>
         <xsl:map-entry key="'JervisAdmiral'" select="'https://viaf.org/viaf/14749635/rdf.xml'"/>
         <xsl:map-entry key="'KenyonJohn'" select="'https://viaf.org/viaf/68469709/rdf.xml'"/>
         <xsl:map-entry key="'KiddJohn'" select="'https://viaf.org/viaf/33398575/rdf.xml'"/>
         <xsl:map-entry key="'KnightonSirWilliam'"
                        select="'https://viaf.org/viaf/25382261/rdf.xml'"/>
         <xsl:map-entry key="'LambCharles'" select="'https://viaf.org/viaf/64007908/rdf.xml'"/>
         <xsl:map-entry key="'LambThomasDavis'"
                        select="'https://viaf.org/viaf/111145541773596601618/rdf.xml'"/>
         <xsl:map-entry key="'LancasterJoseph'"
                        select="'https://viaf.org/viaf/51823093/rdf.xml'"/>
         <xsl:map-entry key="'LandorRobertEyres'"
                        select="'https://viaf.org/viaf/3833473/rdf.xml'"/>
         <xsl:map-entry key="'LandorWalterSavage'"
                        select="'https://viaf.org/viaf/46796707/rdf.xml'"/>
         <xsl:map-entry key="'LittletonEdwardJohn'"
                        select="'https://viaf.org/viaf/96799079/rdf.xml'"/>
         <xsl:map-entry key="'LloydCharles'" select="'https://viaf.org/viaf/120746691/rdf.xml'"/>
         <xsl:map-entry key="'LockerEdwardHawke'"
                        select="'https://viaf.org/viaf/35280319/rdf.xml'"/>
         <xsl:map-entry key="'LongmanThomas'" select="'https://viaf.org/viaf/12361408/rdf.xml'"/>
         <xsl:map-entry key="'LoshJames'" select="'https://viaf.org/viaf/48290965/rdf.xml'"/>
         <xsl:map-entry key="'LovellRobert'" select="'https://viaf.org/viaf/73726715/rdf.xml'"/>
         <xsl:map-entry key="'LowtherWilliam'"
                        select="'https://viaf.org/viaf/18765475/rdf.xml'"/>
         <xsl:map-entry key="'MitfordJohn'" select="'https://viaf.org/viaf/27838479/rdf.xml'"/>
         <xsl:map-entry key="'MontaguBasil'" select="'https://viaf.org/viaf/17571171/rdf.xml'"/>
         <xsl:map-entry key="'MontgomeryJames'" select="'https://viaf.org/viaf/183634/rdf.xml'"/>
         <xsl:map-entry key="'MoreHannah'" select="'https://viaf.org/viaf/51802053/rdf.xml'"/>
         <xsl:map-entry key="'MurrayJohn'" select="'https://viaf.org/viaf/88737799/rdf.xml'"/>
         <xsl:map-entry key="'NaresRobert'" select="'https://viaf.org/viaf/32788887/rdf.xml'"/>
         <xsl:map-entry key="'NashEdward'" select="'https://viaf.org/viaf/317138022/rdf.xml'"/>
         <xsl:map-entry key="'NicholsJohn'" select="'https://viaf.org/viaf/100250810/rdf.xml'"/>
         <xsl:map-entry key="'OpieAmelia'" select="'https://viaf.org/viaf/39410771/rdf.xml'"/>
         <xsl:map-entry key="'ParsonsJohn'" select="'https://viaf.org/viaf/43366674/rdf.xml'"/>
         <xsl:map-entry key="'PeggeChristopher'"
                        select="'https://viaf.org/viaf/40818427/rdf.xml'"/>
         <xsl:map-entry key="'PercevalSpencer'"
                        select="'https://viaf.org/viaf/13050320/rdf.xml'"/>
         <xsl:map-entry key="'PhillimoreJoseph'"
                        select="'https://viaf.org/viaf/30427130/rdf.xml'"/>
         <xsl:map-entry key="'PhillipsRichard'"
                        select="'https://viaf.org/viaf/37243190/rdf.xml'"/>
         <xsl:map-entry key="'PonsonbySarah'" select="'https://viaf.org/viaf/69733295/rdf.xml'"/>
         <xsl:map-entry key="'PooleThomas'" select="'https://viaf.org/viaf/30781049/rdf.xml'"/>
         <xsl:map-entry key="'ProbyJohnCarysfort'"
                        select="'https://viaf.org/viaf/57966485/rdf.xml'"/>
         <xsl:map-entry key="'PugheWilliamOwen'"
                        select="'https://viaf.org/viaf/61534972/rdf.xml'"/>
         <xsl:map-entry key="'ReesOwen'" select="'https://viaf.org/viaf/76369684/rdf.xml'"/>
         <xsl:map-entry key="'ReesThomas'" select="'https://viaf.org/viaf/74360148/rdf.xml'"/>
         <xsl:map-entry key="'ReeveHenry'" select="'https://viaf.org/viaf/39731901/rdf.xml'"/>
         <xsl:map-entry key="'RickmanJohn'" select="'https://viaf.org/viaf/56169311/rdf.xml'"/>
         <xsl:map-entry key="'RobinsonHenryCrabb'"
                        select="'https://viaf.org/viaf/34599013/rdf.xml'"/>
         <xsl:map-entry key="'RoscoeWilliam'" select="'https://viaf.org/viaf/15550259/rdf.xml'"/>
         <xsl:map-entry key="'RoughWilliam'" select="'https://viaf.org/viaf/68356692/rdf.xml'"/>
         <xsl:map-entry key="'RushtonEdward'" select="'https://viaf.org/viaf/5358949/rdf.xml'"/>
         <xsl:map-entry key="'SayersFrank'" select="'https://viaf.org/viaf/42207948/rdf.xml'"/>
         <xsl:map-entry key="'ScottWalter'" select="'https://viaf.org/viaf/95207079/rdf.xml'"/>
         <xsl:map-entry key="'ScottMargaretCharlotte'"
                        select="'https://viaf.org/viaf/12146029537935822200/rdf.xml'"/>
         <xsl:map-entry key="'SewardAnna'" select="'https://viaf.org/viaf/27208300/rdf.xml'"/>
         <xsl:map-entry key="'SharpRichard'" select="'https://viaf.org/viaf/78653883/rdf.xml'"/>
         <xsl:map-entry key="'ShelleyPercyBysshe'"
                        select="'https://viaf.org/viaf/95159449/rdf.xml'"/>
         <xsl:map-entry key="'SouthcottJoanna'"
                        select="'https://viaf.org/viaf/22972595/rdf.xml'"/>
         <xsl:map-entry key="'StuartDaniel'" select="'https://viaf.org/viaf/28314223/rdf.xml'"/>
         <xsl:map-entry key="'TaylorHenrySir'"
                        select="'https://viaf.org/viaf/64022130/rdf.xml'"/>
         <xsl:map-entry key="'TaylorWilliam'" select="'https://viaf.org/viaf/5986440/rdf.xml'"/>
         <xsl:map-entry key="'ThelwallJohn'" select="'https://viaf.org/viaf/37726570/rdf.xml'"/>
         <xsl:map-entry key="'TurnerSharon'" select="'https://viaf.org/viaf/35239427/rdf.xml'"/>
         <xsl:map-entry key="'VardonMr'" select="'https://viaf.org/viaf/1558665/rdf.xml'"/>
         <xsl:map-entry key="'VincentWilliam'"
                        select="'https://viaf.org/viaf/88072523/rdf.xml'"/>
         <xsl:map-entry key="'WakefieldGilbert'"
                        select="'https://viaf.org/viaf/8157129/rdf.xml'"/>
         <xsl:map-entry key="'WatsonRichardBishopLlandaff'"
                        select="'https://viaf.org/viaf/22173223/rdf.xml'"/>
         <xsl:map-entry key="'WedgwoodThomas'"
                        select="'https://viaf.org/viaf/57488292/rdf.xml'"/>
         <xsl:map-entry key="'WellesleyArthurWellington'"
                        select="'https://viaf.org/viaf/2465308/rdf.xml'"/>
         <xsl:map-entry key="'WellesleyMarquis'"
                        select="'https://viaf.org/viaf/62284378/rdf.xml'"/>
         <xsl:map-entry key="'WestallWilliam'"
                        select="'https://viaf.org/viaf/42192548/rdf.xml'"/>
         <xsl:map-entry key="'WhiteHenryKirke'"
                        select="'https://viaf.org/viaf/32736220/rdf.xml'"/>
         <xsl:map-entry key="'WhiteJosephBlanco'"
                        select="'https://viaf.org/viaf/49268120/rdf.xml'"/>
         <xsl:map-entry key="'WilberforceWilliam'"
                        select="'https://viaf.org/viaf/36925883/rdf.xml'"/>
         <xsl:map-entry key="'WordsworthDorothy'"
                        select="'https://viaf.org/viaf/19695644/rdf.xml'"/>
         <xsl:map-entry key="'WordsworthMary'"
                        select="'https://viaf.org/viaf/67364888/rdf.xml'"/>
         <xsl:map-entry key="'WordsworthWilliam'"
                        select="'https://viaf.org/viaf/35723133/rdf.xml'"/>
         <xsl:map-entry key="'WynnCharlesWW'" select="'https://viaf.org/viaf/35231813/rdf.xml'"/>
         <xsl:map-entry key="'AbellaManuel'" select="'https://viaf.org/viaf/74229167/xml.rdf'"/>
         <xsl:map-entry key="'AikinJohn'" select="'https://viaf.org/viaf/59180443/xml.rdf'"/>
         <xsl:map-entry key="'BaillieJoanna'" select="'https://viaf.org/viaf/56750310/xml.rdf'"/>
         <xsl:map-entry key="'BartonBernard'" select="'https://viaf.org/viaf/47543211/xml.rdf'"/>
         <xsl:map-entry key="'BeaumontLadyMary'"
                        select="'https://viaf.org/viaf/51498407/xml.rdf'"/>
         <xsl:map-entry key="'Bedfordfamily'"
                        select="'https://viaf.org/viaf/316573568/xml.rdf'"/>
         <xsl:map-entry key="'BiggsNathaniel'"
                        select="'https://viaf.org/viaf/14365518/xml.rdf'"/>
         <xsl:map-entry key="'BoucherJonathan'"
                        select="'https://viaf.org/viaf/32159397306719300303/xml.rdf'"/>
         <xsl:map-entry key="'ButtJohnMarten'"
                        select="'https://viaf.org/viaf/282152636180520052696/xml.rdf'"/>
         <xsl:map-entry key="'CollinsJeremiah'"
                        select="'https://viaf.org/viaf/315617990/xml.rdf'"/>
         <xsl:map-entry key="'CoplestonEdward'"
                        select="'https://viaf.org/viaf/32235709/xml.rdf'"/>
         <xsl:map-entry key="'Cottlefamily'" select="'https://viaf.org/viaf/316583760/xml.rdf'"/>
         <xsl:map-entry key="'DuppaRichard'" select="'https://viaf.org/viaf/66813237/xml.rdf'"/>
         <xsl:map-entry key="'EdmondsonDr'" select="'https://viaf.org/viaf/93290517/xml.rdf'"/>
         <xsl:map-entry key="'ElliottEbenezer'"
                        select="'https://viaf.org/viaf/52850012/xml.rdf'"/>
         <xsl:map-entry key="'FreelingFrancis'"
                        select="'https://viaf.org/viaf/12737560/xml.rdf'"/>
         <xsl:map-entry key="'FrickerMary'" select="'https://viaf.org/viaf/77875712/xml.rdf'"/>
         <xsl:map-entry key="'FrickerSarah'" select="'https://viaf.org/viaf/30783477/xml.rdf'"/>
         <xsl:map-entry key="'GrenvilleLord'" select="'https://viaf.org/viaf/61556077/xml.rdf'"/>
         <xsl:map-entry key="'HillErrol'" select="'https://viaf.org/viaf/34588961/xml.rdf'"/>
         <xsl:map-entry key="'HoggJamesEttrick'"
                        select="'https://viaf.org/viaf/22151505/xml.rdf'"/>
         <xsl:map-entry key="'HorsemanJohn'" select="'https://viaf.org/viaf/134112145/xml.rdf'"/>
         <xsl:map-entry key="'HucksJoseph'" select="'https://viaf.org/viaf/13601203/xml.rdf'"/>
         <xsl:map-entry key="'JardineAlexander'"
                        select="'https://viaf.org/viaf/107637007/xml.rdf'"/>
         <xsl:map-entry key="'KosterHenry'" select="'https://viaf.org/viaf/46950751/xml.rdf'"/>
         <xsl:map-entry key="'KosterJohnTheodore'"
                        select="'https://viaf.org/viaf/244212573/xml.rdf'"/>
         <xsl:map-entry key="'LambMaryAnne'" select="'https://viaf.org/viaf/24603799/xml.rdf'"/>
         <xsl:map-entry key="'Lovellfamily'" select="'https://viaf.org/viaf/316745781/xml.rdf'"/>
         <xsl:map-entry key="'MooreThomas'" select="'https://viaf.org/viaf/24616222/xml.rdf'"/>
         <xsl:map-entry key="'NealeCornelius'"
                        select="'https://viaf.org/viaf/2228159248379804870008/xml.rdf'"/>
         <xsl:map-entry key="'ParryCalebHillier'"
                        select="'https://viaf.org/viaf/42579160/xml.rdf'"/>
         <xsl:map-entry key="'PeckwellRobertHenry'"
                        select="'https://viaf.org/viaf/91583399/xml.rdf'"/>
         <xsl:map-entry key="'Robinsonfamily'"
                        select="'https://viaf.org/viaf/295570226/xml.rdf'"/>
         <xsl:map-entry key="'Sewardfamily'" select="'https://viaf.org/viaf/316580379/xml.rdf'"/>
         <xsl:map-entry key="'ShelleyHarriet'"
                        select="'https://viaf.org/viaf/65248526/xml.rdf'"/>
         <xsl:map-entry key="'SoutheyTom'" select="'https://viaf.org/viaf/80507566/xml.rdf'"/>
         <xsl:map-entry key="'WalweindeTervliet'"
                        select="'https://viaf.org/viaf/74745771/xml.rdf'"/>
      </xsl:map>
   </xsl:variable>
</xsl:stylesheet>
