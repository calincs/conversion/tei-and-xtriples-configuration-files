<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0" version="3.0">
    <xsl:output method="text" encoding="utf-8" />
    
    <xsl:param name="delim" select="';'" />
    <xsl:param name="quote" select="'&quot;'" />
    <xsl:variable name="newline" select="'&#10;'" />
    
    <xsl:template match="TEI">
        <xsl:value-of select="$quote"/>
        <xsl:text>Xml_id</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>name</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>dates</xsl:text>
        <xsl:value-of select="$quote"/>        
        <xsl:value-of select="$newline"/>
        <xsl:apply-templates select="./text/body/div[@type='paratext']/list[@type='simple']"/>
    </xsl:template>
    
    <xsl:template match="list[@type='simple']">
        <xsl:apply-templates select="item[@n]"/>
    </xsl:template>
    
    <xsl:template match="item[@n]">
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="term/@xml:id"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:apply-templates select="term"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$newline"/>
    </xsl:template>
    <xsl:template match="term">
        <xsl:value-of select="$quote"/>        
        <xsl:value-of select="replace(replace(./text()[1],' \(.*',''),'[\)|:|;|\[.*\]]','')" />
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        
        <xsl:value-of select="$quote"/>
        <xsl:analyze-string select="./text()[1]" regex="(\d{{4}}(–)\d{{4}})">
            <xsl:matching-substring>
                <xsl:value-of select="regex-group(1)"/>
            </xsl:matching-substring>
        </xsl:analyze-string>
        
        
        <xsl:value-of select="$quote"/>
        
        <!--<xsl:value-of select="replace(term/text(), '*\\(.*', '')"/>-->
        
    </xsl:template>
    
</xsl:stylesheet>