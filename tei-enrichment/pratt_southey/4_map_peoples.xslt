<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:schema="https://schema.org/"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    xmlns:map="http://www.w3.org/2005/xpath-functions/map"  
    xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias"
    
     version="3.0">
    <xsl:output method="xml" encoding="UTF-8" indent="yes"/>
 
    <xsl:namespace-alias stylesheet-prefix="axsl" result-prefix="xsl"/>
    <xsl:template match="/">
        <axsl:stylesheet
            xmlns:xs="http://www.w3.org/2001/XMLSchema"
            version="3.0"
            >
            <xsl:variable name="wikidata_pers_ref" as="element()*">
                  
                <xsl:for-each select="/list/file">
                        <xsl:for-each select="document(@name)/rdf:RDF">
                            <xsl:for-each select="rdf:Description/child::*">
                                
                                <xsl:if test="contains(text(),'Wikidata')">
                                    <xsl:element name='ref'>
                                        <xsl:element name="entry">

                                        <xsl:attribute name='key'>
                                            <xsl:value-of select="replace(parent::node()/@rdf:about,'http://localhost:3333/','')" />
                                        </xsl:attribute>
                                            <xsl:value-of select="."/>
                                        </xsl:element>
                                    </xsl:element>
                                </xsl:if>                    
                            </xsl:for-each>
                        </xsl:for-each>
                </xsl:for-each>
                
            </xsl:variable>
            <axsl:variable name="wikidata_pers_ref" as="map(xs:string, xs:string)">
                <axsl:map>    

                    <xsl:for-each-group select="$wikidata_pers_ref/entry" group-by="@key">
                        <xsl:variable name="k" select="@key" />
                        <axsl:map-entry  key="'{$k}'" select="'{current-group()[1]/text()}'"/>         
                    </xsl:for-each-group>
                </axsl:map>
            </axsl:variable>

            <xsl:variable name="getty_pers_ref" as="element()*">
                  
                <xsl:for-each select="/list/file">
                        <xsl:for-each select="document(@name)/rdf:RDF">
                            <xsl:for-each select="rdf:Description/child::*">
                                
                                <xsl:if test="contains(text(),'getty')">
                                    <xsl:element name='ref'>
                                        <xsl:element name="entry">

                                        <xsl:attribute name='key'>
                                            <xsl:value-of select="replace(parent::node()/@rdf:about,'http://localhost:3333/','')" />
                                        </xsl:attribute>
                                            <xsl:value-of select="."/>
                                        </xsl:element>
                                    </xsl:element>
                                </xsl:if>                    
                            </xsl:for-each>
                        </xsl:for-each>
                </xsl:for-each>
                
            </xsl:variable>
            <axsl:variable name="getty_pers_ref" as="map(xs:string, xs:string)">
                <axsl:map>    
                    <xsl:for-each-group select="$getty_pers_ref/entry" group-by="@key">
                        <xsl:variable name="k" select="@key" />
                        <axsl:map-entry  key="'{$k}'" select="'{current-group()[1]/text()}'"/>         
                    </xsl:for-each-group>
                </axsl:map>
            </axsl:variable>



            <xsl:variable name="viaf_pers_ref" as="element()*">
                  
                <xsl:for-each select="/list/file">
                        <xsl:for-each select="document(@name)/rdf:RDF">
                            <xsl:for-each select="rdf:Description/child::*">
                                
                                <xsl:if test="contains(text(),'viaf')">
                                    <xsl:element name='ref'>
                                        <xsl:element name="entry">

                                        <xsl:attribute name='key'>
                                            <xsl:value-of select="replace(parent::node()/@rdf:about,'http://localhost:3333/','')" />
                                        </xsl:attribute>
                                            <xsl:value-of select="."/>
                                        </xsl:element>
                                    </xsl:element>
                                </xsl:if>                    
                            </xsl:for-each>
                        </xsl:for-each>
                </xsl:for-each>
                
            </xsl:variable>
            <axsl:variable name="viaf_pers_ref" as="map(xs:string, xs:string)">
                <axsl:map>    
                    <xsl:for-each-group select="$viaf_pers_ref/entry" group-by="@key">
                        <xsl:variable name="k" select="@key" />
                        <axsl:map-entry  key="'{$k}'" select="'{current-group()[1]/text()}'"/>         
                    </xsl:for-each-group>
                </axsl:map>
            </axsl:variable>
        
                
                
        </axsl:stylesheet>
    </xsl:template>

</xsl:stylesheet>
