<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
    xmlns="http://www.tei-c.org/ns/1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:map="http://www.w3.org/2005/xpath-functions/map"
    xmlns:tei="http://www.tei-c/ns/1.0"     
    xpath-default-namespace="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes=" xsl   tei xs"
    version="3.0">
    <xsl:strip-space elements="*"/>
    <xsl:output method="xml" indent="yes"/>
    <xsl:include href="5_places_map.xsl"/>
    
    <xsl:template match="node()| @*">
        <xsl:copy>
            <xsl:apply-templates select="node() | @*"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="item[@n]/term[@xml:id]" >
        <xsl:variable name="myId" select="./@xml:id"/>
        
        <xsl:copy>
           
            <xsl:apply-templates select="@*"/>
            <xsl:if test="map:contains($wikidata_place_ref, $myId)">
                <xsl:element name="idno" >
                    <xsl:attribute name="type">WIKIDATA</xsl:attribute>
                    <xsl:value-of select="$wikidata_place_ref($myId)"/>
                </xsl:element>
            </xsl:if>
            <xsl:if test="map:contains($geonames_place_ref, $myId)">
                <xsl:element name="idno" >
                    <xsl:attribute name="type">GEONAMES</xsl:attribute>
                    <xsl:value-of select="$geonames_place_ref($myId)"/>
                </xsl:element>
            </xsl:if>
            <xsl:if test="map:contains($getty_place_ref, $myId)">
                <xsl:element name="idno" >
                    <xsl:attribute name="type">GETTY</xsl:attribute>
                    <xsl:value-of select="$getty_place_ref($myId)"/>     
                </xsl:element>
            </xsl:if>
            
            <xsl:apply-templates select="node()"/>
        </xsl:copy>
   </xsl:template>
</xsl:stylesheet>