<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:schema="https://schema.org/"
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:map="http://www.w3.org/2005/xpath-functions/map"
                version="3.0">
   <xsl:variable name="getty_place_ref" as="map(xs:string, xs:string)">
      <xsl:map>
         <xsl:map-entry key="'Dublin'" select="'http://vocab.getty.edu/tgn/7001306.rdf'"/>
         <xsl:map-entry key="'Norwich'" select="'http://vocab.getty.edu/tgn/7012005.rdf'"/>
         <xsl:map-entry key="'Abbotsford'"
                        select="'http://vocab.getty.edu/tgn/tgn/7465039.rdf'"/>
         <xsl:map-entry key="'Ambleside'" select="'http://vocab.getty.edu/tgn/tgn/7010279.rdf'"/>
         <xsl:map-entry key="'Brixton'" select="'http://vocab.getty.edu/tgn/tgn/7466913.rdf'"/>
         <xsl:map-entry key="'Cintra'" select="'http://vocab.getty.edu/tgn/tgn/7011007.rdf'"/>
         <xsl:map-entry key="'DanversKingsdown'"
                        select="'http://vocab.getty.edu/tgn/tgn/7012301.rdf'"/>
         <xsl:map-entry key="'FurnessAbbey'"
                        select="'http://vocab.getty.edu/tgn/tgn/1100046.rdf'"/>
         <xsl:map-entry key="'GrasmereDove'"
                        select="'http://vocab.getty.edu/tgn/tgn/7010264.rdf'"/>
         <xsl:map-entry key="'Keswick'" select="'http://vocab.getty.edu/tgn/tgn/7460811.rdf'"/>
         <xsl:map-entry key="'Llanthony'" select="'http://vocab.getty.edu/tgn/tgn/7460725.rdf'"/>
         <xsl:map-entry key="'Lowtherestate'"
                        select="'http://vocab.getty.edu/tgn/tgn/4006613.rdf'"/>
         <xsl:map-entry key="'MerthyrTidvil'"
                        select="'http://vocab.getty.edu/tgn/tgn/7008473.rdf'"/>
         <xsl:map-entry key="'Skiddaw'" select="'http://vocab.getty.edu/tgn/tgn/1106488.rdf'"/>
         <xsl:map-entry key="'StauntonOnWye'"
                        select="'http://vocab.getty.edu/tgn/tgn/7027142.rdf'"/>
         <xsl:map-entry key="'Stowey'" select="'http://vocab.getty.edu/tgn/tgn/7459263.rdf'"/>
         <xsl:map-entry key="'Streatham'" select="'http://vocab.getty.edu/tgn/tgn/1005668.rdf'"/>
         <xsl:map-entry key="'Ullswater'" select="'http://vocab.getty.edu/tgn/tgn/1118502.rdf'"/>
         <xsl:map-entry key="'Westbury'" select="'http://vocab.getty.edu/tgn/tgn/1136766.rdf'"/>
         <xsl:map-entry key="'Wynnstay'" select="'http://vocab.getty.edu/tgn/tgn/7444281.rdf'"/>
         <xsl:map-entry key="'Yarmouth'" select="'http://vocab.getty.edu/tgn/tgn/1031247.rdf'"/>
      </xsl:map>
   </xsl:variable>
   <xsl:variable name="wikidata_place_ref" as="map(xs:string, xs:string)">
      <xsl:map>
         <xsl:map-entry key="'Abbotsford'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q246076.rdf'"/>
         <xsl:map-entry key="'Ambleside'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q391805.rdf'"/>
         <xsl:map-entry key="'BalliolOxford'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q805285.rdf'"/>
         <xsl:map-entry key="'BullandMouth'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q48699035.rdf'"/>
         <xsl:map-entry key="'Burton'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q659927.rdf'"/>
         <xsl:map-entry key="'Cintra'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q190187.rdf'"/>
         <xsl:map-entry key="'CollegeGreenBristol'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q5146396.rdf'"/>
         <xsl:map-entry key="'Crickhowel'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q2536016.rdf'"/>
         <xsl:map-entry key="'Dublin'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q1761.rdf'"/>
         <xsl:map-entry key="'FurnessAbbey'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q1475061.rdf'"/>
         <xsl:map-entry key="'GraysInn'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q157412.rdf'"/>
         <xsl:map-entry key="'HollandHouse'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q2342832.rdf'"/>
         <xsl:map-entry key="'Keswick'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q1025076.rdf'"/>
         <xsl:map-entry key="'Llanthony'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q6661791.rdf'"/>
         <xsl:map-entry key="'Mirehouse'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q6873094.rdf'"/>
         <xsl:map-entry key="'Norwich'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q130191.rdf'"/>
         <xsl:map-entry key="'PaternosterRow'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q11698324.rdf'"/>
         <xsl:map-entry key="'RydalMount'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q782981.rdf'"/>
         <xsl:map-entry key="'Skiddaw'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q2292191.rdf'"/>
         <xsl:map-entry key="'StauntonOnWye'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q7604747.rdf'"/>
         <xsl:map-entry key="'Stowey'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q7620707.rdf'"/>
         <xsl:map-entry key="'Streatham'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q146667.rdf'"/>
         <xsl:map-entry key="'Ullswater'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q1973568.rdf'"/>
         <xsl:map-entry key="'Westbury'"
                        select="'http://www.Wikidata.org/wiki/Special:EntityData/Q1234797.rdf'"/>
      </xsl:map>
   </xsl:variable>
   <xsl:variable name="geonames_place_ref" as="map(xs:string, xs:string)">
      <xsl:map>
         <xsl:map-entry key="'Abbotsford'"
                        select="'https://sws.geonames.org/2657867/about.rdf'"/>
         <xsl:map-entry key="'Ambleside'" select="'https://sws.geonames.org/2657360/about.rdf'"/>
         <xsl:map-entry key="'Burton'" select="'https://sws.geonames.org/2654217/about.rdf'"/>
         <xsl:map-entry key="'Cintra'" select="'https://sws.geonames.org/8010565/about.rdf'"/>
         <xsl:map-entry key="'Crickhowel'"
                        select="'https://sws.geonames.org/2651986/about.rdf'"/>
         <xsl:map-entry key="'Dublin'" select="'https://sws.geonames.org/2964574/about.rdf'"/>
         <xsl:map-entry key="'FurnessAbbey'"
                        select="'https://sws.geonames.org/6289316/about.rdf'"/>
         <xsl:map-entry key="'Keswick'" select="'https://sws.geonames.org/2645756/about.rdf'"/>
         <xsl:map-entry key="'Llanthony'" select="'https://sws.geonames.org/2643939/about.rdf'"/>
         <xsl:map-entry key="'Norwich'" select="'https://sws.geonames.org/2641181/about.rdf'"/>
         <xsl:map-entry key="'Skiddaw'" select="'https://sws.geonames.org/2637715/about.rdf'"/>
         <xsl:map-entry key="'StauntonOnWye'"
                        select="'https://sws.geonames.org/2636997/about.rdf'"/>
         <xsl:map-entry key="'Stowey'" select="'https://sws.geonames.org/2636751/about.rdf'"/>
         <xsl:map-entry key="'Streatham'" select="'https://sws.geonames.org/2636675/about.rdf'"/>
         <xsl:map-entry key="'Ullswater'" select="'https://sws.geonames.org/2635191/about.rdf'"/>
         <xsl:map-entry key="'Westbury'" select="'https://sws.geonames.org/2634483/about.rdf'"/>
      </xsl:map>
   </xsl:variable>
</xsl:stylesheet>
