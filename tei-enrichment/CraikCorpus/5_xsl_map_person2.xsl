<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:schema="https://schema.org/"
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:map="http://www.w3.org/2005/xpath-functions/map"
                version="3.0">
   <xsl:variable name="viaf_pers_ref" as="map(xs:string, xs:string)">
      <xsl:map>
         <xsl:map-entry key="'DMC'" select="'https://viaf.org/viaf/46793285/rdf.xml'"/>
         <xsl:map-entry key="'AldenHenry'" select="'https://viaf.org/viaf/70174323/rdf.xml'"/>
         <xsl:map-entry key="'LowSampson'" select="'https://viaf.org/viaf/22131048/rdf.xml'"/>
         <xsl:map-entry key="'HallSamuelCarter'"
                        select="'https://viaf.org/viaf/67216250/rdf.xml'"/>
         <xsl:map-entry key="'HallAnnaMaria'" select="'https://viaf.org/viaf/59344020/rdf.xml'"/>
         <xsl:map-entry key="'MiersEdward'" select="'https://viaf.org/viaf/270092022/rdf.xml'"/>
         <xsl:map-entry key="'MiersHarry'" select="'https://viaf.org/viaf/10612317/rdf.xml'"/>
         <xsl:map-entry key="'MarstonWestland'"
                        select="'https://viaf.org/viaf/34445221/rdf.xml'"/>
         <xsl:map-entry key="'GaskellElizabeth'"
                        select="'https://viaf.org/viaf/39377536/rdf.xml'"/>
         <xsl:map-entry key="'DobellSydney'" select="'https://viaf.org/viaf/68944196/rdf.xml'"/>
         <xsl:map-entry key="'DobellClarence'"
                        select="'https://viaf.org/viaf/121276155/rdf.xml'"/>
         <xsl:map-entry key="'RiviereBriton'" select="'https://viaf.org/viaf/14300596/rdf.xml'"/>
         <xsl:map-entry key="'JollyPaulSr'" select="'https://viaf.org/viaf/229369479/rdf.xml'"/>
         <xsl:map-entry key="'DrysdaleCharles'"
                        select="'https://viaf.org/viaf/66032984/rdf.xml'"/>
         <xsl:map-entry key="'MacreadyWilliamCharles'"
                        select="'https://viaf.org/viaf/5743196/rdf.xml'"/>
         <xsl:map-entry key="'HolmanHunt'" select="'https://viaf.org/viaf/73884600/rdf.xml'"/>
         <xsl:map-entry key="'BrightJohn'" select="'https://viaf.org/viaf/69003330/rdf.xml'"/>
         <xsl:map-entry key="'GladstoneWilliam'"
                        select="'https://viaf.org/viaf/71411983/rdf.xml'"/>
         <xsl:map-entry key="'BensonEdward'" select="'https://viaf.org/viaf/3252348/rdf.xml'"/>
         <xsl:map-entry key="'TempleFrederick'"
                        select="'https://viaf.org/viaf/52501167/rdf.xml'"/>
         <xsl:map-entry key="'MacmillanAlexander'"
                        select="'https://viaf.org/viaf/94871964/rdf.xml'"/>
         <xsl:map-entry key="'MacmillanDaniel'"
                        select="'https://viaf.org/viaf/188686583/rdf.xml'"/>
         <xsl:map-entry key="'DeWittHenriette'"
                        select="'https://viaf.org/viaf/89507376/rdf.xml'"/>
         <xsl:map-entry key="'ShawRichardNorman'"
                        select="'https://viaf.org/viaf/77145857793823020594/rdf.xml'"/>
         <xsl:map-entry key="'ChapmanFrederic'"
                        select="'https://viaf.org/viaf/90580915/rdf.xml'"/>
         <xsl:map-entry key="'BrowningElizabethBarrett'"
                        select="'https://viaf.org/viaf/66464493/rdf.xml'"/>
         <xsl:map-entry key="'BrowningRobert'"
                        select="'https://viaf.org/viaf/24598774/rdf.xml'"/>
         <xsl:map-entry key="'BulwerLyttonEdward'"
                        select="'https://viaf.org/viaf/99871326/rdf.xml'"/>
         <xsl:map-entry key="'HerfordLaura'" select="'https://viaf.org/viaf/95769937/rdf.xml'"/>
         <xsl:map-entry key="'AllinghamHelen'"
                        select="'https://viaf.org/viaf/34727518/rdf.xml'"/>
         <xsl:map-entry key="'AllinghamWilliam'"
                        select="'https://viaf.org/viaf/56777098/rdf.xml'"/>
         <xsl:map-entry key="'MillaisJohn'" select="'https://viaf.org/viaf/51738621/rdf.xml'"/>
         <xsl:map-entry key="'LeightonFrederick'"
                        select="'https://viaf.org/viaf/18031604/rdf.xml'"/>
         <xsl:map-entry key="'PatonJosephNoel'"
                        select="'https://viaf.org/viaf/77119089/rdf.xml'"/>
         <xsl:map-entry key="'CraikHenry'" select="'https://viaf.org/viaf/10218254/rdf.xml'"/>
         <xsl:map-entry key="'CraikGeorgiana'"
                        select="'https://viaf.org/viaf/13326113/rdf.xml'"/>
         <xsl:map-entry key="'CraikGeorgeLillieSr'"
                        select="'https://viaf.org/viaf/19771695/rdf.xml'"/>
         <xsl:map-entry key="'CockerellSydney'"
                        select="'https://viaf.org/viaf/64149146/rdf.xml'"/>
         <xsl:map-entry key="'BarrettWilson'"
                        select="'https://viaf.org/viaf/100337911/rdf.xml'"/>
         <xsl:map-entry key="'TennysonAlfred'"
                        select="'https://viaf.org/viaf/61540536/rdf.xml'"/>
         <xsl:map-entry key="'ChapmanEdward'"
                        select="'https://viaf.org/viaf/315623675/rdf.xml'"/>
         <xsl:map-entry key="'CundallJoseph'" select="'https://viaf.org/viaf/14829138/rdf.xml'"/>
         <xsl:map-entry key="'HowittMary'" select="'https://viaf.org/viaf/42648131/rdf.xml'"/>
         <xsl:map-entry key="'HowittWilliam'" select="'https://viaf.org/viaf/22908301/rdf.xml'"/>
         <xsl:map-entry key="'MorrisJane'" select="'https://viaf.org/viaf/16220010/rdf.xml'"/>
         <xsl:map-entry key="'MorrisWilliam'" select="'https://viaf.org/viaf/22146194/rdf.xml'"/>
         <xsl:map-entry key="'OliphantMargaret'"
                        select="'https://viaf.org/viaf/49256072/rdf.xml'"/>
         <xsl:map-entry key="'RosettiDanteGabriel'"
                        select="'https://viaf.org/viaf/41848725/rdf.xml'"/>
         <xsl:map-entry key="'BrowningPen'" select="'https://viaf.org/viaf/100839870/rdf.xml'"/>
         <xsl:map-entry key="'ChalmersThomas'"
                        select="'https://viaf.org/viaf/64076481/rdf.xml'"/>
         <xsl:map-entry key="'CharlesElizabeth'"
                        select="'https://viaf.org/viaf/95236972/rdf.xml'"/>
         <xsl:map-entry key="'DuplessisGeorges'"
                        select="'https://viaf.org/viaf/31995889/rdf.xml'"/>
         <xsl:map-entry key="'GoldsmidAnnaMaria'"
                        select="'https://viaf.org/viaf/14668078/rdf.xml'"/>
         <xsl:map-entry key="'HillDavidOctavius'"
                        select="'https://viaf.org/viaf/69725368/rdf.xml'"/>
         <xsl:map-entry key="'KingsleyFanny'" select="'https://viaf.org/viaf/74734046/rdf.xml'"/>
         <xsl:map-entry key="'LovellGeorge'" select="'https://viaf.org/viaf/3845962/rdf.xml'"/>
         <xsl:map-entry key="'MontaguLordRobert'"
                        select="'https://viaf.org/viaf/15538017/rdf.xml'"/>
         <xsl:map-entry key="'OShaughnessyArthur'"
                        select="'https://viaf.org/viaf/22975288/rdf.xml'"/>
         <xsl:map-entry key="'ParkerJohnWilliam'"
                        select="'https://viaf.org/viaf/41709233/rdf.xml'"/>
         <xsl:map-entry key="'ThackerayWilliamMakepeace'"
                        select="'https://viaf.org/viaf/95208604/rdf.xml'"/>
         <xsl:map-entry key="'GowerGeorgeLeveson'"
                        select="'https://viaf.org/viaf/30412195/rdf.xml'"/>
         <xsl:map-entry key="'AdderleyCharles'"
                        select="'https://viaf.org/viaf/38329858/rdf.xml'"/>
         <xsl:map-entry key="'BentleyGeorge'" select="'https://viaf.org/viaf/64749967/rdf.xml'"/>
         <xsl:map-entry key="'ChambersRobert'"
                        select="'https://viaf.org/viaf/61590299/rdf.xml'"/>
         <xsl:map-entry key="'ChambersWilliam'"
                        select="'https://viaf.org/viaf/32051544/rdf.xml'"/>
         <xsl:map-entry key="'FairholtFrederickWilliam'"
                        select="'https://viaf.org/viaf/15138755/rdf.xml'"/>
         <xsl:map-entry key="'IronsWilliamJosiah'"
                        select="'https://viaf.org/viaf/58230523/rdf.xml'"/>
         <xsl:map-entry key="'LoudonJane'" select="'https://viaf.org/viaf/89021272/rdf.xml'"/>
         <xsl:map-entry key="'ShakespeareWilliam'"
                        select="'https://viaf.org/viaf/96994048/rdf.xml'"/>
         <xsl:map-entry key="'MrsValentine'" select="'https://viaf.org/viaf/35787112/rdf.xml'"/>
         <xsl:map-entry key="'WaddingtonRichard'"
                        select="'https://viaf.org/viaf/104505582/rdf.xml'"/>
         <xsl:map-entry key="'WillsWilliamHenry'"
                        select="'https://viaf.org/viaf/25851348/rdf.xml'"/>
         <xsl:map-entry key="'HooperJane'" select="'https://viaf.org/viaf/11860163/rdf.xml'"/>
         <xsl:map-entry key="'KeanCharles'" select="'https://viaf.org/viaf/66621983/rdf.xml'"/>
         <xsl:map-entry key="'LanziLuigi'" select="'https://viaf.org/viaf/78760428/rdf.xml'"/>
         <xsl:map-entry key="'MartinFrances'"
                        select="'https://viaf.org/viaf/313535496/rdf.xml'"/>
         <xsl:map-entry key="'NewbyThomasCautley'"
                        select="'https://viaf.org/viaf/34325653/rdf.xml'"/>
         <xsl:map-entry key="'PilkingtonMatthew'"
                        select="'https://viaf.org/viaf/26938896/rdf.xml'"/>
         <xsl:map-entry key="'ProcterBryanWaller'"
                        select="'https://viaf.org/viaf/27855302/rdf.xml'"/>
         <xsl:map-entry key="'VasariGeorgio'" select="'https://viaf.org/viaf/46768219/rdf.xml'"/>
         <xsl:map-entry key="'WrightGeorge'" select="'https://viaf.org/viaf/91362449/rdf.xml'"/>
         <xsl:map-entry key="'HillOctavia'" select="'https://viaf.org/viaf/47523034/rdf.xml'"/>
         <xsl:map-entry key="'PilkingtonGeorge'"
                        select="'https://viaf.org/viaf/62489325/rdf.xml'"/>
         <xsl:map-entry key="'SpeerCharlton'"
                        select="'https://viaf.org/viaf/163170394/rdf.xml'"/>
         <xsl:map-entry key="'KendalWH'" select="'https://viaf.org/viaf/58643494/rdf.xml'"/>
         <xsl:map-entry key="'ElliottMartha'" select="'https://viaf.org/viaf/33786707/rdf.xml'"/>
         <xsl:map-entry key="'YongeCharlotte'"
                        select="'https://viaf.org/viaf/71399881/rdf.xml'"/>
         <xsl:map-entry key="'GattyMargaret'" select="'https://viaf.org/viaf/27092632/rdf.xml'"/>
         <xsl:map-entry key="'BellGeorge'" select="'https://viaf.org/viaf/95866634/rdf.xml'"/>
         <xsl:map-entry key="'RuskinJohn'" select="'https://viaf.org/viaf/73859585/rdf.xml'"/>
         <xsl:map-entry key="'WildeOscar'" select="'https://viaf.org/viaf/34464414/rdf.xml'"/>
         <xsl:map-entry key="'SmedleyFrank'" select="'https://viaf.org/viaf/77336643/rdf.xml'"/>
         <xsl:map-entry key="'MayallJoe'" select="'https://viaf.org/viaf/21914934/rdf.xml'"/>
         <xsl:map-entry key="'MayallJohn'" select="'https://viaf.org/viaf/22405041/rdf.xml'"/>
         <xsl:map-entry key="'VignolesCharles'"
                        select="'https://viaf.org/viaf/69707335/rdf.xml'"/>
         <xsl:map-entry key="'FieldsAnnie'" select="'https://viaf.org/viaf/69069190/rdf.xml'"/>
         <xsl:map-entry key="'StrahanAlexander'"
                        select="'https://viaf.org/viaf/40810077/rdf.xml'"/>
         <xsl:map-entry key="'MacleodNorman'" select="'https://viaf.org/viaf/34561376/rdf.xml'"/>
         <xsl:map-entry key="'AndersonAlexander'"
                        select="'https://viaf.org/viaf/34808308/rdf.xml'"/>
         <xsl:map-entry key="'MeadeElizabeth'"
                        select="'https://viaf.org/viaf/54180425/rdf.xml'"/>
         <xsl:map-entry key="'MassonDavid'" select="'https://viaf.org/viaf/12373598/rdf.xml'"/>
         <xsl:map-entry key="'BlakeWilliam'" select="'https://viaf.org/viaf/54144439/rdf.xml'"/>
         <xsl:map-entry key="'GilchristAlexander'"
                        select="'https://viaf.org/viaf/27224099/rdf.xml'"/>
         <xsl:map-entry key="'BarryTress'" select="'https://viaf.org/viaf/12224555/rdf.xml'"/>
         <xsl:map-entry key="'NewlandsJames'"
                        select="'https://viaf.org/viaf/271423054/rdf.xml'"/>
         <xsl:map-entry key="'TuemanAllen'" select="'https://viaf.org/viaf/7439632/rdf.xml'"/>
         <xsl:map-entry key="'DeCervantesMiguel'"
                        select="'https://viaf.org/viaf/17220427/rdf.xml'"/>
         <xsl:map-entry key="'AdamsWBridges'"
                        select="'https://viaf.org/viaf/106391319/rdf.xml'"/>
         <xsl:map-entry key="'MiersJohn'" select="'https://viaf.org/viaf/8154761/rdf.xml'"/>
         <xsl:map-entry key="'MacmillanFrederick'"
                        select="'https://viaf.org/viaf/17312368/rdf.xml'"/>
      </xsl:map>
   </xsl:variable>
   <xsl:variable name="wikidata_pers_ref" as="map(xs:string, xs:string)">
      <xsl:map>
         <xsl:map-entry key="'DMC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2331155.rdf'"/>
         <xsl:map-entry key="'AldenHenry'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5725891.rdf'"/>
         <xsl:map-entry key="'LowSampson'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q7410238.rdf'"/>
         <xsl:map-entry key="'HallSamuelCarter'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2218070.rdf'"/>
         <xsl:map-entry key="'HallAnnaMaria'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q562000.rdf'"/>
         <xsl:map-entry key="'MiersEdward'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5343666.rdf'"/>
         <xsl:map-entry key="'MiersHarry'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q375078.rdf'"/>
         <xsl:map-entry key="'MiersFrancis'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q49576588.rdf'"/>
         <xsl:map-entry key="'MarstonWestland'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q711012.rdf'"/>
         <xsl:map-entry key="'GaskellElizabeth'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q229226.rdf'"/>
         <xsl:map-entry key="'DobellSydney'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3540415.rdf'"/>
         <xsl:map-entry key="'DobellClarence'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5126474.rdf'"/>
         <xsl:map-entry key="'RiviereBriton'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2470305.rdf'"/>
         <xsl:map-entry key="'JollyPaulSr'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3371538.rdf'"/>
         <xsl:map-entry key="'DrysdaleCharles'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q17046220.rdf'"/>
         <xsl:map-entry key="'MacreadyWilliamCharles'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q637374.rdf'"/>
         <xsl:map-entry key="'HolmanHunt'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q211763.rdf'"/>
         <xsl:map-entry key="'BlackettHenry'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5718246.rdf'"/>
         <xsl:map-entry key="'BrightJohn'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q176524.rdf'"/>
         <xsl:map-entry key="'GladstoneWilliam'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q160852.rdf'"/>
         <xsl:map-entry key="'BensonEdward'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q336433.rdf'"/>
         <xsl:map-entry key="'TempleFrederick'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q336295.rdf'"/>
         <xsl:map-entry key="'MacmillanAlexander'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q4719518.rdf'"/>
         <xsl:map-entry key="'MacmillanDaniel'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5218014.rdf'"/>
         <xsl:map-entry key="'DeWittHenriette'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q16211338.rdf'"/>
         <xsl:map-entry key="'ShawRichardNorman'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1374863.rdf'"/>
         <xsl:map-entry key="'ChapmanFrederic'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q18526932.rdf'"/>
         <xsl:map-entry key="'BrowningElizabethBarrett'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q228494.rdf'"/>
         <xsl:map-entry key="'BrowningRobert'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q233265.rdf'"/>
         <xsl:map-entry key="'BulwerLyttonEdward'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q318462.rdf'"/>
         <xsl:map-entry key="'HerfordLaura'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q18821760.rdf'"/>
         <xsl:map-entry key="'AllinghamHelen'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q444581.rdf'"/>
         <xsl:map-entry key="'AllinghamWilliam'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q261007.rdf'"/>
         <xsl:map-entry key="'MillaisJohn'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q159606.rdf'"/>
         <xsl:map-entry key="'LeightonFrederick'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q160252.rdf'"/>
         <xsl:map-entry key="'PatonJosephNoel'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1364698.rdf'"/>
         <xsl:map-entry key="'CraikHenry'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q7527120.rdf'"/>
         <xsl:map-entry key="'CraikJames'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q45401320.rdf'"/>
         <xsl:map-entry key="'CraikGeorgiana'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q4240054.rdf'"/>
         <xsl:map-entry key="'CraikGeorgeLillieSr'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q4240056.rdf'"/>
         <xsl:map-entry key="'CockerellSydney'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q966843.rdf'"/>
         <xsl:map-entry key="'BarrettWilson'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3208122.rdf'"/>
         <xsl:map-entry key="'TennysonAlfred'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q173869.rdf'"/>
         <xsl:map-entry key="'ChapmanEdward'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q19039619.rdf'"/>
         <xsl:map-entry key="'CundallJoseph'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q6282376.rdf'"/>
         <xsl:map-entry key="'HallWilliam'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q19325856.rdf'"/>
         <xsl:map-entry key="'HowittMary'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q521991.rdf'"/>
         <xsl:map-entry key="'HowittWilliam'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1412583.rdf'"/>
         <xsl:map-entry key="'MorrisJane'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q269360.rdf'"/>
         <xsl:map-entry key="'MorrisWilliam'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q182589.rdf'"/>
         <xsl:map-entry key="'OliphantMargaret'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2415217.rdf'"/>
         <xsl:map-entry key="'RosettiDanteGabriel'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q186748.rdf'"/>
         <xsl:map-entry key="'BrowningPen'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2514899.rdf'"/>
         <xsl:map-entry key="'ChalmersThomas'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q563069.rdf'"/>
         <xsl:map-entry key="'CharlesElizabeth'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5362566.rdf'"/>
         <xsl:map-entry key="'DuplessisGeorges'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3102651.rdf'"/>
         <xsl:map-entry key="'GoldsmidAnnaMaria'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q4767294.rdf'"/>
         <xsl:map-entry key="'HillDavidOctavius'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q722792.rdf'"/>
         <xsl:map-entry key="'KingsleyFanny'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q18922239.rdf'"/>
         <xsl:map-entry key="'LovellGeorge'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q18671312.rdf'"/>
         <xsl:map-entry key="'MontaguLordRobert'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q6679922.rdf'"/>
         <xsl:map-entry key="'OShaughnessyArthur'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q710896.rdf'"/>
         <xsl:map-entry key="'ParkerJohnWilliam'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q15259326.rdf'"/>
         <xsl:map-entry key="'ThackerayWilliamMakepeace'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q167768.rdf'"/>
         <xsl:map-entry key="'GrosvenorLadyOctavia'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q54859517.rdf'"/>
         <xsl:map-entry key="'GowerGeorgeLeveson'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q332722.rdf'"/>
         <xsl:map-entry key="'AdderleyCharles'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q338354.rdf'"/>
         <xsl:map-entry key="'BentleyGeorge'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q18527189.rdf'"/>
         <xsl:map-entry key="'ChambersRobert'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q515008.rdf'"/>
         <xsl:map-entry key="'ChambersWilliam'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3568493.rdf'"/>
         <xsl:map-entry key="'MissCoates'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q76329788.rdf'"/>
         <xsl:map-entry key="'FairholtFrederickWilliam'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q13476228.rdf'"/>
         <xsl:map-entry key="'IronsWilliamJosiah'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q8013787.rdf'"/>
         <xsl:map-entry key="'LoudonJane'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q514667.rdf'"/>
         <xsl:map-entry key="'ShakespeareWilliam'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q692.rdf'"/>
         <xsl:map-entry key="'MrsValentine'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q6499526.rdf'"/>
         <xsl:map-entry key="'WaddingtonRichard'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2150850.rdf'"/>
         <xsl:map-entry key="'WillsWilliamHenry'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q15092885.rdf'"/>
         <xsl:map-entry key="'HooperJane'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q56571872.rdf'"/>
         <xsl:map-entry key="'KeanCharles'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q725355.rdf'"/>
         <xsl:map-entry key="'LanziLuigi'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q972400.rdf'"/>
         <xsl:map-entry key="'MartinFrances'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q19325657.rdf'"/>
         <xsl:map-entry key="'NewbyThomasCautley'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q7788285.rdf'"/>
         <xsl:map-entry key="'PilkingtonMatthew'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q17048572.rdf'"/>
         <xsl:map-entry key="'ProcterBryanWaller'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q4980322.rdf'"/>
         <xsl:map-entry key="'VasariGeorgio'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q128027.rdf'"/>
         <xsl:map-entry key="'WrightGeorge'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5542854.rdf'"/>
         <xsl:map-entry key="'HillOctavia'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q437462.rdf'"/>
         <xsl:map-entry key="'PilkingtonGeorge'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q18876429.rdf'"/>
         <xsl:map-entry key="'SpeerCharlton'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q18546482.rdf'"/>
         <xsl:map-entry key="'KendalWH'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q8012696.rdf'"/>
         <xsl:map-entry key="'ElliottMartha'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q55988629.rdf'"/>
         <xsl:map-entry key="'YongeCharlotte'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q433098.rdf'"/>
         <xsl:map-entry key="'GattyMargaret'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1382113.rdf'"/>
         <xsl:map-entry key="'BellGeorge'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5536873.rdf'"/>
         <xsl:map-entry key="'RuskinJohn'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q179126.rdf'"/>
         <xsl:map-entry key="'WildeOscar'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q30875.rdf'"/>
         <xsl:map-entry key="'SmedleyFrank'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5480834.rdf'"/>
         <xsl:map-entry key="'MayallJoe'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q48566542.rdf'"/>
         <xsl:map-entry key="'MayallJohn'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1700595.rdf'"/>
         <xsl:map-entry key="'IsbisterWilliam'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q63877303.rdf'"/>
         <xsl:map-entry key="'VignolesCharles'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q537504.rdf'"/>
         <xsl:map-entry key="'FieldsAnnie'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q566895.rdf'"/>
         <xsl:map-entry key="'StrahanAlexander'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q15627353.rdf'"/>
         <xsl:map-entry key="'MacleodNorman'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q7052555.rdf'"/>
         <xsl:map-entry key="'CorbettArchibald'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q4786274.rdf'"/>
         <xsl:map-entry key="'AndersonAlexander'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q4718220.rdf'"/>
         <xsl:map-entry key="'MeadeElizabeth'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q6456649.rdf'"/>
         <xsl:map-entry key="'MassonDavid'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5237204.rdf'"/>
         <xsl:map-entry key="'BlakeWilliam'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q41513.rdf'"/>
         <xsl:map-entry key="'GilchristAlexander'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q4718950.rdf'"/>
         <xsl:map-entry key="'BarryTress'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q7526675.rdf'"/>
         <xsl:map-entry key="'NewlandsJames'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q6140333.rdf'"/>
         <xsl:map-entry key="'TuemanAllen'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3612474.rdf'"/>
         <xsl:map-entry key="'DeCervantesMiguel'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5682.rdf'"/>
         <xsl:map-entry key="'AdamsWBridges'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2578132.rdf'"/>
         <xsl:map-entry key="'MiersJohn'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q575253.rdf'"/>
         <xsl:map-entry key="'MacmillanFrederick'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q23011965.rdf'"/>
      </xsl:map>
   </xsl:variable>
   <xsl:variable name="isni_pers_ref" as="map(xs:string, xs:string)">
      <xsl:map>
         <xsl:map-entry key="'DMC'" select="'http://www.isni.org/0000000108957219.xml'"/>
         <xsl:map-entry key="'AldenHenry'" select="'http://www.isni.org/0000000081502993.xml'"/>
         <xsl:map-entry key="'LowSampson'" select="'http://www.isni.org/0000000040396637.xml'"/>
         <xsl:map-entry key="'HallSamuelCarter'"
                        select="'http://www.isni.org/0000000080776490.xml'"/>
         <xsl:map-entry key="'HallAnnaMaria'"
                        select="'http://www.isni.org/0000000081388166.xml'"/>
         <xsl:map-entry key="'MiersEdward'" select="'http://www.isni.org/0000000383539986.xml'"/>
         <xsl:map-entry key="'MiersHarry'" select="'http://www.isni.org/0000000081962535.xml'"/>
         <xsl:map-entry key="'MarstonWestland'"
                        select="'http://www.isni.org/0000000081129084.xml'"/>
         <xsl:map-entry key="'GaskellElizabeth'"
                        select="'http://www.isni.org/0000000122785134.xml'"/>
         <xsl:map-entry key="'DobellSydney'"
                        select="'http://www.isni.org/0000000081481547.xml'"/>
         <xsl:map-entry key="'RiviereBriton'"
                        select="'http://www.isni.org/0000000067040411.xml'"/>
         <xsl:map-entry key="'JollyPaulSr'" select="'http://www.isni.org/0000000364960783.xml'"/>
         <xsl:map-entry key="'DrysdaleCharles'"
                        select="'http://www.isni.org/000000008253847X.xml'"/>
         <xsl:map-entry key="'MacreadyWilliamCharles'"
                        select="'http://www.isni.org/000000008353370X.xml'"/>
         <xsl:map-entry key="'HolmanHunt'" select="'http://www.isni.org/0000000083939822.xml'"/>
         <xsl:map-entry key="'BrightJohn'" select="'http://www.isni.org/0000000081485150.xml'"/>
         <xsl:map-entry key="'GladstoneWilliam'"
                        select="'http://www.isni.org/0000000121387062.xml'"/>
         <xsl:map-entry key="'BensonEdward'"
                        select="'http://www.isni.org/0000000092470130.xml'"/>
         <xsl:map-entry key="'TempleFrederick'"
                        select="'http://www.isni.org/000000008232296X.xml'"/>
         <xsl:map-entry key="'MacmillanAlexander'"
                        select="'http://www.isni.org/0000000110782438.xml'"/>
         <xsl:map-entry key="'DeWittHenriette'"
                        select="'http://www.isni.org/0000000109232527.xml'"/>
         <xsl:map-entry key="'ShawRichardNorman'"
                        select="'http://www.isni.org/0000000066752994.xml'"/>
         <xsl:map-entry key="'ChapmanFrederic'"
                        select="'http://www.isni.org/0000000062011566.xml'"/>
         <xsl:map-entry key="'BrowningElizabethBarrett'"
                        select="'http://www.isni.org/000000012101969X.xml'"/>
         <xsl:map-entry key="'BrowningRobert'"
                        select="'http://www.isni.org/0000000120999083.xml'"/>
         <xsl:map-entry key="'BulwerLyttonEdward'"
                        select="'http://www.isni.org/0000000121450076.xml'"/>
         <xsl:map-entry key="'AllinghamHelen'"
                        select="'http://www.isni.org/0000000083715642.xml'"/>
         <xsl:map-entry key="'AllinghamWilliam'"
                        select="'http://www.isni.org/0000000109752020.xml'"/>
         <xsl:map-entry key="'MillaisJohn'" select="'http://www.isni.org/000000012132781X.xml'"/>
         <xsl:map-entry key="'LeightonFrederick'"
                        select="'http://www.isni.org/0000000083396135.xml'"/>
         <xsl:map-entry key="'PatonJosephNoel'"
                        select="'http://www.isni.org/000000011878231X.xml'"/>
         <xsl:map-entry key="'CraikHenry'" select="'http://www.isni.org/0000000108696063.xml'"/>
         <xsl:map-entry key="'CraikGeorgiana'"
                        select="'http://www.isni.org/0000000108714700.xml'"/>
         <xsl:map-entry key="'CraikGeorgeLillieSr'"
                        select="'http://www.isni.org/0000000108766666.xml'"/>
         <xsl:map-entry key="'CockerellSydney'"
                        select="'http://www.isni.org/0000000083442082.xml'"/>
         <xsl:map-entry key="'BarrettWilson'"
                        select="'http://www.isni.org/0000000114533906.xml'"/>
         <xsl:map-entry key="'TennysonAlfred'"
                        select="'http://www.isni.org/0000000121017424.xml'"/>
         <xsl:map-entry key="'CundallJoseph'"
                        select="'http://www.isni.org/0000000066323516.xml'"/>
         <xsl:map-entry key="'HowittMary'" select="'http://www.isni.org/0000000081218407.xml'"/>
         <xsl:map-entry key="'HowittWilliam'"
                        select="'http://www.isni.org/0000000109598107.xml'"/>
         <xsl:map-entry key="'MorrisJane'" select="'http://www.isni.org/0000000122090359.xml'"/>
         <xsl:map-entry key="'MorrisWilliam'"
                        select="'http://www.isni.org/0000000121235747.xml'"/>
         <xsl:map-entry key="'OliphantMargaret'"
                        select="'http://www.isni.org/0000000121319350.xml'"/>
         <xsl:map-entry key="'RosettiDanteGabriel'"
                        select="'http://www.isni.org/0000000121296670.xml'"/>
         <xsl:map-entry key="'BrowningPen'" select="'http://www.isni.org/0000000116922331.xml'"/>
         <xsl:map-entry key="'ChalmersThomas'"
                        select="'http://www.isni.org/0000000121365226.xml'"/>
         <xsl:map-entry key="'CharlesElizabeth'"
                        select="'http://www.isni.org/0000000112090304.xml'"/>
         <xsl:map-entry key="'DuplessisGeorges'"
                        select="'http://www.isni.org/0000000121265903.xml'"/>
         <xsl:map-entry key="'GoldsmidAnnaMaria'"
                        select="'http://www.isni.org/0000000120213044.xml'"/>
         <xsl:map-entry key="'HillDavidOctavius'"
                        select="'http://www.isni.org/0000000109131187.xml'"/>
         <xsl:map-entry key="'KingsleyFanny'"
                        select="'http://www.isni.org/0000000083948008.xml'"/>
         <xsl:map-entry key="'LovellGeorge'"
                        select="'http://www.isni.org/0000000084181910.xml'"/>
         <xsl:map-entry key="'MontaguLordRobert'"
                        select="'http://www.isni.org/0000000073572211.xml'"/>
         <xsl:map-entry key="'OShaughnessyArthur'"
                        select="'http://www.isni.org/0000000083645402.xml'"/>
         <xsl:map-entry key="'ParkerJohnWilliam'"
                        select="'http://www.isni.org/0000000064149171.xml'"/>
         <xsl:map-entry key="'ThackerayWilliamMakepeace'"
                        select="'http://www.isni.org/0000000121441903.xml'"/>
         <xsl:map-entry key="'GowerGeorgeLeveson'"
                        select="'http://www.isni.org/0000000061467596.xml'"/>
         <xsl:map-entry key="'AdderleyCharles'"
                        select="'http://www.isni.org/0000000082205763.xml'"/>
         <xsl:map-entry key="'BentleyGeorge'"
                        select="'http://www.isni.org/0000000051859668.xml'"/>
         <xsl:map-entry key="'ChambersRobert'"
                        select="'http://www.isni.org/0000000118768972.xml'"/>
         <xsl:map-entry key="'ChambersWilliam'"
                        select="'http://www.isni.org/0000000110513916.xml'"/>
         <xsl:map-entry key="'FairholtFrederickWilliam'"
                        select="'http://www.isni.org/0000000080948416.xml'"/>
         <xsl:map-entry key="'IronsWilliamJosiah'"
                        select="'http://www.isni.org/0000000082420877.xml'"/>
         <xsl:map-entry key="'LoudonJane'" select="'http://www.isni.org/0000000086660026.xml'"/>
         <xsl:map-entry key="'ShakespeareWilliam'"
                        select="'http://www.isni.org/0000000121032683.xml'"/>
         <xsl:map-entry key="'MrsValentine'"
                        select="'http://www.isni.org/0000000063379856.xml'"/>
         <xsl:map-entry key="'WaddingtonRichard'"
                        select="'http://www.isni.org/0000000075718691.xml'"/>
         <xsl:map-entry key="'WillsWilliamHenry'"
                        select="'http://www.isni.org/0000000119918426.xml'"/>
         <xsl:map-entry key="'HooperJane'" select="'http://www.isni.org/0000000037442903.xml'"/>
         <xsl:map-entry key="'KeanCharles'" select="'http://www.isni.org/0000000063108992.xml'"/>
         <xsl:map-entry key="'LanziLuigi'" select="'http://www.isni.org/0000000121407780.xml'"/>
         <xsl:map-entry key="'PilkingtonMatthew'"
                        select="'http://www.isni.org/0000000118857748.xml'"/>
         <xsl:map-entry key="'ProcterBryanWaller'"
                        select="'http://www.isni.org/000000008107284X.xml'"/>
         <xsl:map-entry key="'VasariGeorgio'"
                        select="'http://www.isni.org/0000000121309590.xml'"/>
         <xsl:map-entry key="'WrightGeorge'"
                        select="'http://www.isni.org/0000000110309982.xml'"/>
         <xsl:map-entry key="'HillOctavia'" select="'http://www.isni.org/0000000110610863.xml'"/>
         <xsl:map-entry key="'PilkingtonGeorge'"
                        select="'http://www.isni.org/0000000116568096.xml'"/>
         <xsl:map-entry key="'SpeerCharlton'"
                        select="'http://www.isni.org/0000000112073950.xml'"/>
         <xsl:map-entry key="'KendalWH'" select="'http://www.isni.org/0000000374566405.xml'"/>
         <xsl:map-entry key="'ElliottMartha'"
                        select="'http://www.isni.org/0000000398515485.xml'"/>
         <xsl:map-entry key="'YongeCharlotte'"
                        select="'http://www.isni.org/0000000368556550.xml'"/>
         <xsl:map-entry key="'GattyMargaret'"
                        select="'http://www.isni.org/000000012125207X.xml'"/>
         <xsl:map-entry key="'BellGeorge'" select="'http://www.isni.org/000000011776710X.xml'"/>
         <xsl:map-entry key="'RuskinJohn'" select="'http://www.isni.org/0000000121393446.xml'"/>
         <xsl:map-entry key="'WildeOscar'" select="'http://www.isni.org/0000000121272652.xml'"/>
         <xsl:map-entry key="'SmedleyFrank'"
                        select="'http://www.isni.org/0000000116767108.xml'"/>
         <xsl:map-entry key="'MayallJohn'" select="'http://www.isni.org/0000000116064568.xml'"/>
         <xsl:map-entry key="'VignolesCharles'"
                        select="'http://www.isni.org/0000000063199521.xml'"/>
         <xsl:map-entry key="'FieldsAnnie'" select="'http://www.isni.org/0000000368518159.xml'"/>
         <xsl:map-entry key="'StrahanAlexander'"
                        select="'http://www.isni.org/0000000028893075.xml'"/>
         <xsl:map-entry key="'MacleodNorman'"
                        select="'http://www.isni.org/0000000108867651.xml'"/>
         <xsl:map-entry key="'AndersonAlexander'"
                        select="'http://www.isni.org/0000000108872194.xml'"/>
         <xsl:map-entry key="'MeadeElizabeth'"
                        select="'http://www.isni.org/0000000081325384.xml'"/>
         <xsl:map-entry key="'MassonDavid'" select="'http://www.isni.org/0000000121209426.xml'"/>
         <xsl:map-entry key="'BlakeWilliam'"
                        select="'http://www.isni.org/000000012096135X.xml'"/>
         <xsl:map-entry key="'GilchristAlexander'"
                        select="'http://www.isni.org/0000000108822840.xml'"/>
         <xsl:map-entry key="'NewlandsJames'"
                        select="'http://www.isni.org/0000000063956713.xml'"/>
         <xsl:map-entry key="'TuemanAllen'" select="'http://www.isni.org/0000000080863412.xml'"/>
         <xsl:map-entry key="'DeCervantesMiguel'"
                        select="'http://www.isni.org/0000000121221919.xml'"/>
         <xsl:map-entry key="'AdamsWBridges'"
                        select="'http://www.isni.org/0000000116958449.xml'"/>
         <xsl:map-entry key="'MiersJohn'" select="'http://www.isni.org/0000000030191259.xml'"/>
         <xsl:map-entry key="'MacmillanFrederick'"
                        select="'http://www.isni.org/0000000063066323.xml'"/>
      </xsl:map>
   </xsl:variable>
   <xsl:variable name="getty_pers_ref" as="map(xs:string, xs:string)">
      <xsl:map>
         <xsl:map-entry key="'DobellClarence'"
                        select="'http://vocab.getty.edu/ulan/500057536.rdf'"/>
         <xsl:map-entry key="'RiviereBriton'"
                        select="'http://vocab.getty.edu/ulan/500020920.rdf'"/>
         <xsl:map-entry key="'HolmanHunt'" select="'http://vocab.getty.edu/ulan/500001633.rdf'"/>
         <xsl:map-entry key="'ShawRichardNorman'"
                        select="'http://vocab.getty.edu/ulan/500001508.rdf'"/>
         <xsl:map-entry key="'BrowningElizabethBarrett'"
                        select="'http://vocab.getty.edu/ulan/500335881.rdf'"/>
         <xsl:map-entry key="'BrowningRobert'"
                        select="'http://vocab.getty.edu/ulan/500064807.rdf'"/>
         <xsl:map-entry key="'BulwerLyttonEdward'"
                        select="'http://vocab.getty.edu/ulan/500292901.rdf'"/>
         <xsl:map-entry key="'HerfordLaura'"
                        select="'http://vocab.getty.edu/ulan/500014506.rdf'"/>
         <xsl:map-entry key="'AllinghamHelen'"
                        select="'http://vocab.getty.edu/ulan/500003736.rdf'"/>
         <xsl:map-entry key="'MillaisJohn'"
                        select="'http://vocab.getty.edu/ulan/500031367.rdf'"/>
         <xsl:map-entry key="'LeightonFrederick'"
                        select="'http://vocab.getty.edu/ulan/500030022.rdf'"/>
         <xsl:map-entry key="'PatonJosephNoel'"
                        select="'http://vocab.getty.edu/ulan/500009018.rdf'"/>
         <xsl:map-entry key="'CockerellSydney'"
                        select="'http://vocab.getty.edu/ulan/500315990.rdf'"/>
         <xsl:map-entry key="'BarrettWilson'"
                        select="'http://vocab.getty.edu/ulan/500355268.rdf'"/>
         <xsl:map-entry key="'TennysonAlfred'"
                        select="'http://vocab.getty.edu/ulan/500332962.rdf'"/>
         <xsl:map-entry key="'CundallJoseph'"
                        select="'http://vocab.getty.edu/ulan/500033324.rdf'"/>
         <xsl:map-entry key="'MorrisJane'" select="'http://vocab.getty.edu/ulan/500329385.rdf'"/>
         <xsl:map-entry key="'MorrisWilliam'"
                        select="'http://vocab.getty.edu/ulan/500030629.rdf'"/>
         <xsl:map-entry key="'RosettiDanteGabriel'"
                        select="'http://vocab.getty.edu/ulan/500022594.rdf'"/>
         <xsl:map-entry key="'BrowningPen'"
                        select="'http://vocab.getty.edu/ulan/500024092.rdf'"/>
         <xsl:map-entry key="'HillDavidOctavius'"
                        select="'http://vocab.getty.edu/ulan/500019476.rdf'"/>
         <xsl:map-entry key="'ThackerayWilliamMakepeace'"
                        select="'http://vocab.getty.edu/ulan/500026742.rdf'"/>
         <xsl:map-entry key="'FairholtFrederickWilliam'"
                        select="'http://vocab.getty.edu/ulan/500003698.rdf'"/>
         <xsl:map-entry key="'LoudonJane'" select="'http://vocab.getty.edu/ulan/500018283.rdf'"/>
         <xsl:map-entry key="'ShakespeareWilliam'"
                        select="'http://vocab.getty.edu/ulan/500272240.rdf'"/>
         <xsl:map-entry key="'LanziLuigi'" select="'http://vocab.getty.edu/ulan/500321073.rdf'"/>
         <xsl:map-entry key="'VasariGeorgio'"
                        select="'http://vocab.getty.edu/ulan/500017608.rdf'"/>
         <xsl:map-entry key="'RuskinJohn'" select="'http://vocab.getty.edu/ulan/500006262.rdf'"/>
         <xsl:map-entry key="'WildeOscar'" select="'http://vocab.getty.edu/ulan/500094070.rdf'"/>
         <xsl:map-entry key="'MayallJohn'" select="'http://vocab.getty.edu/ulan/500115530.rdf'"/>
         <xsl:map-entry key="'BlakeWilliam'"
                        select="'http://vocab.getty.edu/ulan/500012489.rdf'"/>
         <xsl:map-entry key="'DeCervantesMiguel'"
                        select="'http://vocab.getty.edu/ulan/500371871.rdf'"/>
      </xsl:map>
   </xsl:variable>
</xsl:stylesheet>
