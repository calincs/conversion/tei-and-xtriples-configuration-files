<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
    xmlns="http://www.tei-c.org/ns/1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:map="http://www.w3.org/2005/xpath-functions/map"
    xmlns:tei="http://www.tei-c/ns/1.0"     
    xpath-default-namespace="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes=" xsl   tei xs"
    version="3.0">
    <xsl:strip-space elements="*"/>
    <xsl:output method="xml" indent="yes"/>
    
    <xsl:include href="5_xsl_map_person.xsl"/>
    <xsl:include href="5_xsl_map_work.xslt"/>
    <xsl:include href="5_xsl_map_place.xslt"/>
    <xsl:include href="5_xsl_map_periodic.xslt"/>
    <xsl:include href="5_xsl_map_organization.xsl"/>
    <xsl:include href="5_xsl_map_event.xslt"/>
    
    <xsl:template match="node()| @*">
        <xsl:copy>
            <xsl:apply-templates select="node() | @*"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="person[@xml:id]" >
        <xsl:variable name="myId" select="@xml:id"/>
        
        <xsl:copy>
           
            <xsl:apply-templates select="@*"/>
            <xsl:if test="map:contains($wikidata_pers_ref, $myId)">
                <xsl:element name="idno" >
                    <xsl:attribute name="type">WIKIDATA</xsl:attribute>
                    <xsl:value-of select="$wikidata_pers_ref($myId)"/>
                </xsl:element>
            </xsl:if>
            <xsl:if test="map:contains($isni_pers_ref, $myId)">
                <xsl:element name="idno" >
                    <xsl:attribute name="type">INSI</xsl:attribute>
                    <xsl:value-of select="$isni_pers_ref($myId)"/>
                </xsl:element>
            </xsl:if>
            <xsl:if test="map:contains($viaf_pers_ref, $myId)">
                <xsl:element name="idno" >
                    <xsl:attribute name="type">VIAF</xsl:attribute>
                    <xsl:value-of select="$viaf_pers_ref($myId)"/>
                </xsl:element>
            </xsl:if>
            <xsl:if test="map:contains($getty_pers_ref, $myId)">
                <xsl:element name="idno" >
                    <xsl:attribute name="type">GETTY</xsl:attribute>
                    <xsl:value-of select="$getty_pers_ref($myId)"/>
                </xsl:element>
            </xsl:if>

            <xsl:apply-templates select="node()"/>
        </xsl:copy>
   </xsl:template>
   <xsl:template match="event[@xml:id]" >
        <xsl:variable name="myId" select="@xml:id"/>
        
        <xsl:copy>           
            <xsl:apply-templates select="@*"/>
            <xsl:if test="map:contains($wikidata_event_ref, $myId)">
                <xsl:element name="idno" >
                    <xsl:attribute name="type">WIKIDATA</xsl:attribute>
                    <xsl:value-of select="$wikidata_event_ref($myId)"/>
                </xsl:element>
            </xsl:if>

            <xsl:apply-templates select="node()"/>
        </xsl:copy>
   </xsl:template>
   <xsl:template match="org[@xml:id]" >
        <xsl:variable name="myId" select="@xml:id"/>
        
        <xsl:copy>
           
            <xsl:apply-templates select="@*"/>
            <xsl:if test="map:contains($wikidata_pers_ref, $myId)">
                <xsl:element name="idno" >
                    <xsl:attribute name="type">WIKIDATA</xsl:attribute>
                    <xsl:value-of select="$wikidata_org_ref($myId)"/>
                </xsl:element>
            </xsl:if>
            <xsl:if test="map:contains($isni_org_ref, $myId)">
                <xsl:element name="idno" >
                    <xsl:attribute name="type">INSI</xsl:attribute>
                    <xsl:value-of select="$isni_org_ref($myId)"/>
                </xsl:element>
            </xsl:if>
            <xsl:if test="map:contains($viaf_org_ref, $myId)">
                <xsl:element name="idno" >
                    <xsl:attribute name="type">VIAF</xsl:attribute>
                    <xsl:value-of select="$viaf_org_ref($myId)"/>                    
                </xsl:element>
            </xsl:if>
            
            <xsl:apply-templates select="node()"/>
        </xsl:copy>
   </xsl:template>
   <xsl:template match="bibl[@xml:id]" >
        <xsl:variable name="myId" select="@xml:id"/>
        
        <xsl:copy>
           
            <xsl:apply-templates select="@*"/>
            <xsl:if test="map:contains($wikidata_work_ref, $myId)">
                <xsl:element name="idno" >
                    <xsl:attribute name="type">WIKIDATA</xsl:attribute>
                    <xsl:value-of select="$wikidata_work_ref($myId)"/>
                </xsl:element>
            </xsl:if>
            <xsl:if test="map:contains($wikidata_periodic_ref, $myId)">
                <xsl:element name="idno" >
                    <xsl:attribute name="type">WIKIDATA</xsl:attribute>
                    <xsl:value-of select="$wikidata_periodic_ref($myId)"/>
                 </xsl:element>
            </xsl:if>
            <xsl:if test="map:contains($viaf_periodic_ref, $myId)">
                <xsl:element name="idno" >
                    <xsl:attribute name="type">VIAF</xsl:attribute>
                    <xsl:value-of select="$viaf_periodic_ref($myId)"/>
                </xsl:element>
            </xsl:if>
           
            <xsl:apply-templates select="node()"/>
        </xsl:copy>
   </xsl:template>
   <xsl:template match="place[@xml:id]" >
        <xsl:variable name="myId" select="@xml:id"/>
        
        <xsl:copy>
           
            <xsl:apply-templates select="@*"/>
            <xsl:if test="map:contains($geoname_place_ref, $myId)">
                <xsl:element name="idno" >
                    <xsl:attribute name="type">GEONAME</xsl:attribute>
                    <xsl:value-of select="$geoname_place_ref($myId)"/>
                 </xsl:element>
            </xsl:if>
              <xsl:if test="map:contains($wikidata_place_ref, $myId)">
                <xsl:element name="idno" >
                    <xsl:attribute name="type">WIKIDATA</xsl:attribute>
                    <xsl:value-of select="$wikidata_place_ref($myId)"/>
                </xsl:element>
            </xsl:if>
              <xsl:if test="map:contains($getty_place_ref, $myId)">
                <xsl:element name="idno" >
                    <xsl:attribute name="type">GETTY</xsl:attribute>
                    <xsl:value-of select="$getty_place_ref($myId)"/>
                </xsl:element>
            </xsl:if>
           
            <xsl:apply-templates select="node()"/>
        </xsl:copy>
   </xsl:template>
</xsl:stylesheet>