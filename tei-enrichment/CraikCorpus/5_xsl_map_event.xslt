<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:schema="https://schema.org/"
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:map="http://www.w3.org/2005/xpath-functions/map"
                version="3.0">
   <xsl:variable name="wikidata_event_ref" as="map(xs:string, xs:string)">
      <xsl:map>
         <xsl:map-entry key="'EngCivilWar'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5452958.rdf'"/>
         <xsl:map-entry key="'CivilWar'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q8676.rdf'"/>
         <xsl:map-entry key="'Easter'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q189266.rdf'"/>
         <xsl:map-entry key="'CrimeanWar'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q254106.rdf'"/>
         <xsl:map-entry key="'GreatExhibition'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q28129476.rdf'"/>
      </xsl:map>
   </xsl:variable>
</xsl:stylesheet>
