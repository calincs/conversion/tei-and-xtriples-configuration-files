<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:schema="https://schema.org/"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    xmlns:map="http://www.w3.org/2005/xpath-functions/map"  
    xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias"
    
     version="3.0">
    <xsl:output method="xml" encoding="UTF-8" indent="yes"/>
 
    <xsl:namespace-alias stylesheet-prefix="axsl" result-prefix="xsl"/>
    
    <xsl:template match="/rdf:RDF">
        <axsl:stylesheet
            xmlns:xs="http://www.w3.org/2001/XMLSchema"
            version="3.0"
            >
            <axsl:variable name="geoname_place_ref" as="map(xs:string, xs:string)">
                <axsl:map>  
                    <xsl:for-each select="rdf:Description/child::*">
                        <xsl:if test="contains(text(),'geonames')">
                            
                            <axsl:map-entry  key="'{replace(parent::node()/@rdf:about,'http://localhost:3333/','')}'" select="'{.}'"/>  
                            
                        </xsl:if>                    
                    </xsl:for-each>
                </axsl:map>
            </axsl:variable>
            
            <axsl:variable name="wikidata_place_ref" as="map(xs:string, xs:string)">
                <axsl:map>  
                    <xsl:for-each select="rdf:Description/child::*">
                        <xsl:if test="contains(text(),'wikidata')">
                            
                            <axsl:map-entry  key="'{replace(parent::node()/@rdf:about,'http://localhost:3333/','')}'" select="'{.}'"/>  
                            
                        </xsl:if>                    
                    </xsl:for-each>
                </axsl:map>
            </axsl:variable>
            
   
        <axsl:variable name="getty_place_ref" as="map(xs:string, xs:string)">
            <axsl:map> 
                <xsl:for-each select="rdf:Description/child::*">
                    <xsl:if test="contains(text(),'getty')">
                        
                        <axsl:map-entry  key="'{replace(parent::node()/@rdf:about,'http://localhost:3333/','')}'" select="'{.}'"/>
                        
                    </xsl:if>                    
                </xsl:for-each>
            </axsl:map>
        </axsl:variable>
        </axsl:stylesheet>
    </xsl:template>
    
</xsl:stylesheet>
