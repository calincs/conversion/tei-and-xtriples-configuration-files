<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:schema="https://schema.org/"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    xmlns:tei="http://www.tei-c/ns/1.0"
    
    
    xpath-default-namespace="http://www.tei-c.org/ns/1.0" version="3.0">
    <xsl:output method="text" encoding="utf-8" />
    
    <xsl:param name="delim" select="';'" />
    <xsl:param name="quote" select="'&quot;'" />
    <xsl:variable name="newline" select="'&#10;'" />
    
    <xsl:template match="/">
        <xsl:value-of select="$quote"/>
        <xsl:text>Xml_id</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>Gender</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>Fornames</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>Surnames</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>Role</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>BirthDate</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>DeathDate</xsl:text>
        <xsl:value-of select="$quote"/>        
        <xsl:value-of select="$newline"/>
        <xsl:apply-templates select="descendant::div[@type != 'DigitalCraikTeam' and @type != 'PastEditors' and @type != 'Pets' ]/listPerson"/>
    </xsl:template>
    
    <xsl:template match="listPerson">
        <xsl:apply-templates select="person[@xml:id]"/>
    </xsl:template>
    
    <xsl:template match="person">
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="@xml:id"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:apply-templates select="@sex"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:apply-templates select="persName"/>        
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="occupation" separator=", "/>
        <xsl:value-of select="$quote"/>        
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="birth/@when"/>
        <xsl:value-of select="$quote"/>        
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="death/@when"/>
        <xsl:value-of select="$quote"/>        
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$newline"/>
    </xsl:template>
    
    <xsl:template match="@sex">
        <xsl:variable name="gender" as="xs:string">
            <xsl:choose>
                <xsl:when test=". eq '1'">man</xsl:when>
                <xsl:when test=". eq '2'">woman</xsl:when>
            </xsl:choose>
        </xsl:variable>
        <xsl:value-of select="$gender"/>
    </xsl:template>
    <xsl:template match="persName">
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="forename" separator=", "/>
        
        <xsl:value-of select="$quote"/>        
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:if test="surname">
            <xsl:choose>
                <xsl:when test="surname[@type='maiden'] and surname[@type='married']">
                    <xsl:value-of select="surname[@type='maiden']/text()"/>
                    <xsl:text> / </xsl:text><xsl:value-of select="surname[@type='married']/text()"/>
                </xsl:when>
                <xsl:when test="count(surname) > 1">
                    <xsl:value-of select="surname" separator=", "/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="surname"></xsl:value-of>
                </xsl:otherwise>
                
            </xsl:choose>
        </xsl:if>
        <xsl:value-of select="$quote"/>
        
    </xsl:template>
    
</xsl:stylesheet>