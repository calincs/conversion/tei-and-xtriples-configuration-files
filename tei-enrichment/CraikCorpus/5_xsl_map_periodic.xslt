<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:schema="https://schema.org/"
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:map="http://www.w3.org/2005/xpath-functions/map"
                version="3.0">
   <xsl:variable name="viaf_periodic_ref" as="map(xs:string, xs:string)">
      <xsl:map>
         <xsl:map-entry key="'IllustratedNews'"
                        select="'http://viaf.org/viaf/128325409/rdf.xml'"/>
         <xsl:map-entry key="'OnceAWeek'" select="'http://viaf.org/viaf/214009910/rdf.xml'"/>
      </xsl:map>
   </xsl:variable>
   <xsl:variable name="wikidata_periodic_ref" as="map(xs:string, xs:string)">
      <xsl:map>
         <xsl:map-entry key="'IllustratedNews'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q6682116.rdf'"/>
      </xsl:map>
   </xsl:variable>
</xsl:stylesheet>
