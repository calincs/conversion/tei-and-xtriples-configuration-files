<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:schema="https://schema.org/"
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:map="http://www.w3.org/2005/xpath-functions/map"
                version="3.0">
   <xsl:variable name="geoname_place_ref" as="map(xs:string, xs:string)">
      <xsl:map>
         <xsl:map-entry key="'Bahia'" select="'https://sws.geonames.org/3471168/about.rdf'"/>
         <xsl:map-entry key="'BootlecumLinacre'"
                        select="'https://sws.geonames.org/2655198/about.rdf'"/>
         <xsl:map-entry key="'Dunblane'" select="'https://sws.geonames.org/2650769/about.rdf'"/>
         <xsl:map-entry key="'Edinburgh'" select="'https://sws.geonames.org/2650225/about.rdf'"/>
         <xsl:map-entry key="'Farnham'" select="'https://sws.geonames.org/2649660/about.rdf'"/>
         <xsl:map-entry key="'IsleofWight'"
                        select="'https://sws.geonames.org/2646007/about.rdf'"/>
         <xsl:map-entry key="'Kensington'"
                        select="'https://sws.geonames.org/2645801/about.rdf'"/>
         <xsl:map-entry key="'KentishTown'"
                        select="'https://sws.geonames.org/6690587/about.rdf'"/>
         <xsl:map-entry key="'Liverpool'" select="'https://sws.geonames.org/2644210/about.rdf'"/>
         <xsl:map-entry key="'PallMall'" select="'https://sws.geonames.org/4647802/about.rdf'"/>
         <xsl:map-entry key="'Piccadilly'"
                        select="'https://sws.geonames.org/6615534/about.rdf'"/>
         <xsl:map-entry key="'Surrey'" select="'https://sws.geonames.org/2636512/about.rdf'"/>
         <xsl:map-entry key="'Richmond'" select="'https://sws.geonames.org/2639389/about.rdf'"/>
         <xsl:map-entry key="'Amberley'" select="'https://sws.geonames.org/2657364/about.rdf'"/>
         <xsl:map-entry key="'Beckenham'" select="'https://sws.geonames.org/2656065/about.rdf'"/>
         <xsl:map-entry key="'Bloomsbury'"
                        select="'https://sws.geonames.org/2655333/about.rdf'"/>
         <xsl:map-entry key="'Dorset'" select="'https://sws.geonames.org/2651079/about.rdf'"/>
         <xsl:map-entry key="'Wareham'" select="'https://sws.geonames.org/2634776/about.rdf'"/>
         <xsl:map-entry key="'Corrie'" select="'https://sws.geonames.org/2652339/about.rdf'"/>
         <xsl:map-entry key="'HampsteadHeath'"
                        select="'https://sws.geonames.org/6618525/about.rdf'"/>
         <xsl:map-entry key="'London'" select="'https://sws.geonames.org/2643743/about.rdf'"/>
         <xsl:map-entry key="'WemyssBay'" select="'https://sws.geonames.org/2634545/about.rdf'"/>
         <xsl:map-entry key="'Bath'" select="'https://sws.geonames.org/2656173/about.rdf'"/>
         <xsl:map-entry key="'AlderleyEdge'"
                        select="'https://sws.geonames.org/2657546/about.rdf'"/>
         <xsl:map-entry key="'Tewkesbury'"
                        select="'https://sws.geonames.org/2636071/about.rdf'"/>
         <xsl:map-entry key="'Camberley'" select="'https://sws.geonames.org/2653947/about.rdf'"/>
         <xsl:map-entry key="'Dover'" select="'https://sws.geonames.org/2651048/about.rdf'"/>
         <xsl:map-entry key="'Chatham'" select="'https://sws.geonames.org/2653305/about.rdf'"/>
         <xsl:map-entry key="'BedfordPlace'"
                        select="'https://sws.geonames.org/2656046/about.rdf'"/>
         <xsl:map-entry key="'Eglinton'" select="'https://sws.geonames.org/2650184/about.rdf'"/>
         <xsl:map-entry key="'Tore'" select="'https://sws.geonames.org/2635672/about.rdf'"/>
         <xsl:map-entry key="'Tyrrellspass'"
                        select="'https://sws.geonames.org/2961043/about.rdf'"/>
         <xsl:map-entry key="'Westmeath'" select="'https://sws.geonames.org/2960972/about.rdf'"/>
         <xsl:map-entry key="'CamdenTown'"
                        select="'https://sws.geonames.org/3345437/about.rdf'"/>
         <xsl:map-entry key="'Kilburn'" select="'https://sws.geonames.org/6690572/about.rdf'"/>
         <xsl:map-entry key="'Leipzig'" select="'https://sws.geonames.org/2879139/about.rdf'"/>
         <xsl:map-entry key="'Paris'" select="'https://sws.geonames.org/2968815/about.rdf'"/>
         <xsl:map-entry key="'Sutherland'"
                        select="'https://sws.geonames.org/2636507/about.rdf'"/>
         <xsl:map-entry key="'Stafford'" select="'https://sws.geonames.org/2637142/about.rdf'"/>
         <xsl:map-entry key="'Balaklava'" select="'https://sws.geonames.org/712930/about.rdf'"/>
         <xsl:map-entry key="'Belfast'" select="'https://sws.geonames.org/2655984/about.rdf'"/>
         <xsl:map-entry key="'Birmingham'"
                        select="'https://sws.geonames.org/2655603/about.rdf'"/>
         <xsl:map-entry key="'Brighton'" select="'https://sws.geonames.org/2654710/about.rdf'"/>
         <xsl:map-entry key="'Ireland'" select="'https://sws.geonames.org/2963597/about.rdf'"/>
         <xsl:map-entry key="'Oxford'" select="'https://sws.geonames.org/2640729/about.rdf'"/>
         <xsl:map-entry key="'Saltley'" select="'https://sws.geonames.org/2638635/about.rdf'"/>
         <xsl:map-entry key="'Donegal'" select="'https://sws.geonames.org/2964752/about.rdf'"/>
         <xsl:map-entry key="'Cushendall'"
                        select="'https://sws.geonames.org/2651676/about.rdf'"/>
         <xsl:map-entry key="'Curran'" select="'https://sws.geonames.org/2651687/about.rdf'"/>
         <xsl:map-entry key="'Athlone'" select="'https://sws.geonames.org/3313472/about.rdf'"/>
         <xsl:map-entry key="'Dublin'" select="'https://sws.geonames.org/2964574/about.rdf'"/>
         <xsl:map-entry key="'SlieveLeague'"
                        select="'https://sws.geonames.org/2963004/about.rdf'"/>
         <xsl:map-entry key="'Llanfairfechan'"
                        select="'https://sws.geonames.org/2644081/about.rdf'"/>
         <xsl:map-entry key="'Derry'" select="'https://sws.geonames.org/2643736/about.rdf'"/>
         <xsl:map-entry key="'York'" select="'https://sws.geonames.org/2633352/about.rdf'"/>
         <xsl:map-entry key="'Dalmally'" select="'https://sws.geonames.org/2651577/about.rdf'"/>
         <xsl:map-entry key="'Winchester'"
                        select="'https://sws.geonames.org/2633858/about.rdf'"/>
         <xsl:map-entry key="'Hammersmith'"
                        select="'https://sws.geonames.org/2647567/about.rdf'"/>
         <xsl:map-entry key="'Bristol'" select="'https://sws.geonames.org/2654675/about.rdf'"/>
         <xsl:map-entry key="'Alexandria'" select="'https://sws.geonames.org/361058/about.rdf'"/>
         <xsl:map-entry key="'Teignmouth'"
                        select="'https://sws.geonames.org/2636132/about.rdf'"/>
         <xsl:map-entry key="'CorfeCastle'"
                        select="'https://sws.geonames.org/2652374/about.rdf'"/>
         <xsl:map-entry key="'Swanage'" select="'https://sws.geonames.org/2636445/about.rdf'"/>
         <xsl:map-entry key="'Geneva'" select="'https://sws.geonames.org/7285902/about.rdf'"/>
         <xsl:map-entry key="'Switzerland'"
                        select="'https://sws.geonames.org/2658434/about.rdf'"/>
         <xsl:map-entry key="'Nice'" select="'https://sws.geonames.org/2990440/about.rdf'"/>
         <xsl:map-entry key="'Dieppe'" select="'https://sws.geonames.org/3021411/about.rdf'"/>
         <xsl:map-entry key="'Plymouth'" select="'https://sws.geonames.org/2640194/about.rdf'"/>
         <xsl:map-entry key="'NewSydney'" select="'https://sws.geonames.org/2147714/about.rdf'"/>
         <xsl:map-entry key="'Gourock'" select="'https://sws.geonames.org/2648259/about.rdf'"/>
         <xsl:map-entry key="'Longsight'" select="'https://sws.geonames.org/6691766/about.rdf'"/>
         <xsl:map-entry key="'Manchester'"
                        select="'https://sws.geonames.org/2643123/about.rdf'"/>
         <xsl:map-entry key="'Cheltenham'"
                        select="'https://sws.geonames.org/2653261/about.rdf'"/>
         <xsl:map-entry key="'CharltonKings'"
                        select="'https://sws.geonames.org/2653324/about.rdf'"/>
         <xsl:map-entry key="'Chelsea'" select="'https://sws.geonames.org/2653265/about.rdf'"/>
         <xsl:map-entry key="'Cromarty'" select="'https://sws.geonames.org/2651936/about.rdf'"/>
         <xsl:map-entry key="'Ramsgate'" select="'https://sws.geonames.org/2639660/about.rdf'"/>
         <xsl:map-entry key="'Leckhampton'"
                        select="'https://sws.geonames.org/2644714/about.rdf'"/>
         <xsl:map-entry key="'Melbourne'" select="'https://sws.geonames.org/2158177/about.rdf'"/>
         <xsl:map-entry key="'StAndrews'" select="'https://sws.geonames.org/2638864/about.rdf'"/>
         <xsl:map-entry key="'Cambridge'" select="'https://sws.geonames.org/2653941/about.rdf'"/>
         <xsl:map-entry key="'Niton'" select="'https://sws.geonames.org/2641480/about.rdf'"/>
         <xsl:map-entry key="'Neuchatel'" select="'https://sws.geonames.org/2659496/about.rdf'"/>
         <xsl:map-entry key="'Armitage'" select="'https://sws.geonames.org/2657054/about.rdf'"/>
         <xsl:map-entry key="'Oban'" select="'https://sws.geonames.org/2641108/about.rdf'"/>
         <xsl:map-entry key="'USA'" select="'https://sws.geonames.org/6252001/about.rdf'"/>
         <xsl:map-entry key="'Wales'" select="'https://sws.geonames.org/2634894/about.rdf'"/>
         <xsl:map-entry key="'NewYork'" select="'https://sws.geonames.org/5128581/about.rdf'"/>
         <xsl:map-entry key="'Shortlands'"
                        select="'https://sws.geonames.org/10793747/about.rdf'"/>
         <xsl:map-entry key="'Glasgow'" select="'https://sws.geonames.org/2648579/about.rdf'"/>
         <xsl:map-entry key="'Salton'" select="'https://sws.geonames.org/2638630/about.rdf'"/>
         <xsl:map-entry key="'CrystalPalace'"
                        select="'https://sws.geonames.org/6693937/about.rdf'"/>
         <xsl:map-entry key="'Rome'" select="'https://sws.geonames.org/3169071/about.rdf'"/>
      </xsl:map>
   </xsl:variable>
   <xsl:variable name="wikidata_place_ref" as="map(xs:string, xs:string)">
      <xsl:map>
         <xsl:map-entry key="'Bahia'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q36947.rdf'"/>
         <xsl:map-entry key="'BootlecumLinacre'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q893159.rdf'"/>
         <xsl:map-entry key="'Dunblane'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q614314.rdf'"/>
         <xsl:map-entry key="'Edinburgh'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q23436.rdf'"/>
         <xsl:map-entry key="'Farnham'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q141743.rdf'"/>
         <xsl:map-entry key="'IsleofWight'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q6084077.rdf'"/>
         <xsl:map-entry key="'Kensington'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q636358.rdf'"/>
         <xsl:map-entry key="'KentishTown'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q16854042.rdf'"/>
         <xsl:map-entry key="'Lancashire'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2374484.rdf'"/>
         <xsl:map-entry key="'Liverpool'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q24826.rdf'"/>
         <xsl:map-entry key="'PallMall'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q7127683.rdf'"/>
         <xsl:map-entry key="'Piccadilly'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q7190553.rdf'"/>
         <xsl:map-entry key="'Surrey'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q390583.rdf'"/>
         <xsl:map-entry key="'TempleLodge'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1454583.rdf'"/>
         <xsl:map-entry key="'Richmond'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q43421.rdf'"/>
         <xsl:map-entry key="'Amberley'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q457210.rdf'"/>
         <xsl:map-entry key="'Beckenham'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q4878642.rdf'"/>
         <xsl:map-entry key="'Bloomsbury'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1089461.rdf'"/>
         <xsl:map-entry key="'Dorset'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1903749.rdf'"/>
         <xsl:map-entry key="'OxfordStreet'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q7115463.rdf'"/>
         <xsl:map-entry key="'Wareham'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q387419.rdf'"/>
         <xsl:map-entry key="'Sussex'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q984084.rdf'"/>
         <xsl:map-entry key="'Corrie'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5172951.rdf'"/>
         <xsl:map-entry key="'Sannox'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3472354.rdf'"/>
         <xsl:map-entry key="'London'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q84.rdf'"/>
         <xsl:map-entry key="'WemyssBay'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1011545.rdf'"/>
         <xsl:map-entry key="'Bath'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q22889.rdf'"/>
         <xsl:map-entry key="'AlderleyEdge'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q986046.rdf'"/>
         <xsl:map-entry key="'Tewkesbury'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1000717.rdf'"/>
         <xsl:map-entry key="'Camberley'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q741400.rdf'"/>
         <xsl:map-entry key="'Dover'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q33518.rdf'"/>
         <xsl:map-entry key="'Chatham'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q141415.rdf'"/>
         <xsl:map-entry key="'KingsCross'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q6410917.rdf'"/>
         <xsl:map-entry key="'BedfordPlace'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q49147.rdf'"/>
         <xsl:map-entry key="'Eglinton'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q853298.rdf'"/>
         <xsl:map-entry key="'Tore'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1831575.rdf'"/>
         <xsl:map-entry key="'Tyrrellspass'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2086304.rdf'"/>
         <xsl:map-entry key="'Westmeath'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q4076766.rdf'"/>
         <xsl:map-entry key="'Brompton'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2300765.rdf'"/>
         <xsl:map-entry key="'CamdenTown'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q927522.rdf'"/>
         <xsl:map-entry key="'Kilburn'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q150214.rdf'"/>
         <xsl:map-entry key="'Leipzig'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2079.rdf'"/>
         <xsl:map-entry key="'Paris'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q90.rdf'"/>
         <xsl:map-entry key="'Prussia'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5283531.rdf'"/>
         <xsl:map-entry key="'Renfrewshire'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q4805978.rdf'"/>
         <xsl:map-entry key="'Sutherland'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1750319.rdf'"/>
         <xsl:map-entry key="'Stafford'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q826782.rdf'"/>
         <xsl:map-entry key="'Staffordshire'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2070717.rdf'"/>
         <xsl:map-entry key="'Balaklava'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q748363.rdf'"/>
         <xsl:map-entry key="'Belfast'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q10686.rdf'"/>
         <xsl:map-entry key="'Birmingham'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2256.rdf'"/>
         <xsl:map-entry key="'Brighton'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q80047.rdf'"/>
         <xsl:map-entry key="'Ireland'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1521115.rdf'"/>
         <xsl:map-entry key="'Oxford'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q34217.rdf'"/>
         <xsl:map-entry key="'Saltley'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q7406147.rdf'"/>
         <xsl:map-entry key="'Donegal'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q855330.rdf'"/>
         <xsl:map-entry key="'Cushendall'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2580652.rdf'"/>
         <xsl:map-entry key="'Curran'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q702686.rdf'"/>
         <xsl:map-entry key="'Athlone'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q369911.rdf'"/>
         <xsl:map-entry key="'Dublin'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1761.rdf'"/>
         <xsl:map-entry key="'Llanfairfechan'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q787395.rdf'"/>
         <xsl:map-entry key="'LochFyne'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3130032.rdf'"/>
         <xsl:map-entry key="'Derry'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q163584.rdf'"/>
         <xsl:map-entry key="'York'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q42462.rdf'"/>
         <xsl:map-entry key="'Morvern'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q38006215.rdf'"/>
         <xsl:map-entry key="'Dalmally'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1109293.rdf'"/>
         <xsl:map-entry key="'Winchester'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q79490.rdf'"/>
         <xsl:map-entry key="'Hammersmith'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q15222677.rdf'"/>
         <xsl:map-entry key="'Bristol'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q23154.rdf'"/>
         <xsl:map-entry key="'Alexandria'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q87.rdf'"/>
         <xsl:map-entry key="'Teignmouth'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q531311.rdf'"/>
         <xsl:map-entry key="'Devon'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2105198.rdf'"/>
         <xsl:map-entry key="'CorfeCastle'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q13341461.rdf'"/>
         <xsl:map-entry key="'Swanage'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1009298.rdf'"/>
         <xsl:map-entry key="'Geneva'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q71.rdf'"/>
         <xsl:map-entry key="'Switzerland'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q23058.rdf'"/>
         <xsl:map-entry key="'Nice'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q33959.rdf'"/>
         <xsl:map-entry key="'Dieppe'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q183557.rdf'"/>
         <xsl:map-entry key="'Plymouth'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q30990.rdf'"/>
         <xsl:map-entry key="'NewSydney'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3130.rdf'"/>
         <xsl:map-entry key="'Gourock'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2469984.rdf'"/>
         <xsl:map-entry key="'Longsight'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2423211.rdf'"/>
         <xsl:map-entry key="'Manchester'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q18125.rdf'"/>
         <xsl:map-entry key="'Cheltenham'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q206988.rdf'"/>
         <xsl:map-entry key="'CharltonKings'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2476358.rdf'"/>
         <xsl:map-entry key="'Chelsea'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q54156.rdf'"/>
         <xsl:map-entry key="'Cromarty'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1012390.rdf'"/>
         <xsl:map-entry key="'Ramsgate'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q736439.rdf'"/>
         <xsl:map-entry key="'Leckhampton'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q6511771.rdf'"/>
         <xsl:map-entry key="'Melbourne'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3141.rdf'"/>
         <xsl:map-entry key="'StAndrews'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q207736.rdf'"/>
         <xsl:map-entry key="'Cambridge'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q350.rdf'"/>
         <xsl:map-entry key="'Niton'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q7041346.rdf'"/>
         <xsl:map-entry key="'Netherlands'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q9816.rdf'"/>
         <xsl:map-entry key="'Armitage'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q4793618.rdf'"/>
         <xsl:map-entry key="'Oban'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q935702.rdf'"/>
         <xsl:map-entry key="'USA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q16931325.rdf'"/>
         <xsl:map-entry key="'UK'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q19283996.rdf'"/>
         <xsl:map-entry key="'Wales'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q80043.rdf'"/>
         <xsl:map-entry key="'StokeUponTrent'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q727839.rdf'"/>
         <xsl:map-entry key="'NewYork'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q60.rdf'"/>
         <xsl:map-entry key="'Shortlands'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q122313.rdf'"/>
         <xsl:map-entry key="'Kent'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q494228.rdf'"/>
         <xsl:map-entry key="'Glasgow'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q4093.rdf'"/>
         <xsl:map-entry key="'Salton'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2121914.rdf'"/>
         <xsl:map-entry key="'Dorsetshire'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q16833574.rdf'"/>
         <xsl:map-entry key="'CrystalPalace'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q122909.rdf'"/>
         <xsl:map-entry key="'Rome'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q220.rdf'"/>
         <xsl:map-entry key="'HollandPlace'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q79591.rdf'"/>
      </xsl:map>
   </xsl:variable>
   <xsl:variable name="getty_place_ref" as="map(xs:string, xs:string)">
      <xsl:map>
         <xsl:map-entry key="'Bahia'" select="'http://vocab.getty.edu/tgn/1001834.rdf'"/>
         <xsl:map-entry key="'Edinburgh'" select="'http://vocab.getty.edu/tgn/7009546.rdf'"/>
         <xsl:map-entry key="'Liverpool'" select="'http://vocab.getty.edu/tgn/7010597.rdf'"/>
         <xsl:map-entry key="'Surrey'" select="'http://vocab.getty.edu/tgn/7008175.rdf'"/>
         <xsl:map-entry key="'Dorset'" select="'http://vocab.getty.edu/tgn/7011961.rdf'"/>
         <xsl:map-entry key="'HampsteadHeath'"
                        select="'http://vocab.getty.edu/tgn/1102857.rdf'"/>
         <xsl:map-entry key="'London'" select="'http://vocab.getty.edu/tgn/7011781.rdf'"/>
         <xsl:map-entry key="'Bath'" select="'http://vocab.getty.edu/tgn/7011197.rdf'"/>
         <xsl:map-entry key="'Brompton'" select="'http://vocab.getty.edu/tgn/4012686.rdf'"/>
         <xsl:map-entry key="'Leipzig'" select="'http://vocab.getty.edu/tgn/7012329.rdf'"/>
         <xsl:map-entry key="'Paris'" select="'http://vocab.getty.edu/tgn/7008038.rdf'"/>
         <xsl:map-entry key="'Belfast'" select="'http://vocab.getty.edu/tgn/7012112.rdf'"/>
         <xsl:map-entry key="'Birmingham'" select="'http://vocab.getty.edu/tgn/7010955.rdf'"/>
         <xsl:map-entry key="'Brighton'" select="'http://vocab.getty.edu/tgn/7011483.rdf'"/>
         <xsl:map-entry key="'Ireland'" select="'http://vocab.getty.edu/tgn/1000078.rdf'"/>
         <xsl:map-entry key="'Oxford'" select="'http://vocab.getty.edu/tgn/7011931.rdf'"/>
         <xsl:map-entry key="'Dublin'" select="'http://vocab.getty.edu/tgn/7001306.rdf'"/>
         <xsl:map-entry key="'York'" select="'http://vocab.getty.edu/tgn/7011995.rdf'"/>
         <xsl:map-entry key="'Bristol'" select="'http://vocab.getty.edu/tgn/7011198.rdf'"/>
         <xsl:map-entry key="'Geneva'" select="'http://vocab.getty.edu/tgn/7007279.rdf'"/>
         <xsl:map-entry key="'Switzerland'" select="'http://vocab.getty.edu/tgn/7011731.rdf'"/>
         <xsl:map-entry key="'Nice'" select="'http://vocab.getty.edu/tgn/7008773.rdf'"/>
         <xsl:map-entry key="'NewSydney'" select="'http://vocab.getty.edu/tgn/7001923.rdf'"/>
         <xsl:map-entry key="'Manchester'" select="'http://vocab.getty.edu/tgn/7010477.rdf'"/>
         <xsl:map-entry key="'Melbourne'" select="'http://vocab.getty.edu/tgn/7001933.rdf'"/>
         <xsl:map-entry key="'Cambridge'" select="'http://vocab.getty.edu/tgn/7010874.rdf'"/>
         <xsl:map-entry key="'Netherlands'" select="'http://vocab.getty.edu/tgn/7016845.rdf'"/>
         <xsl:map-entry key="'USA'" select="'http://vocab.getty.edu/tgn/7012149.rdf'"/>
         <xsl:map-entry key="'NewYork'" select="'http://vocab.getty.edu/tgn/7007567.rdf'"/>
         <xsl:map-entry key="'Glasgow'" select="'http://vocab.getty.edu/tgn/7017283.rdf'"/>
         <xsl:map-entry key="'Rome'" select="'http://vocab.getty.edu/tgn/7000874.rdf'"/>
         <xsl:map-entry key="'HollandPlace'" select="'http://vocab.getty.edu/tgn/7016845.rdf'"/>
      </xsl:map>
   </xsl:variable>
</xsl:stylesheet>
