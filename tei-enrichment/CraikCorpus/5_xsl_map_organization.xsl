<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:schema="https://schema.org/"
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:map="http://www.w3.org/2005/xpath-functions/map"
                version="3.0">
   <xsl:variable name="viaf_org_ref" as="map(xs:string, xs:string)">
      <xsl:map>
         <xsl:map-entry key="'Chambers'" select="'https://viaf.org/viaf/123761543'"/>
         <xsl:map-entry key="'ParkerAndSon'" select="'https://viaf.org/viaf/41709233'"/>
         <xsl:map-entry key="'StationersCompany'" select="'https://viaf.org/viaf/134959913'"/>
         <xsl:map-entry key="'MertonCollege'" select="'https://viaf.org/viaf/311112809'"/>
         <xsl:map-entry key="'UniversityOfOxford'" select="'https://viaf.org/viaf/142129514'"/>
         <xsl:map-entry key="'BradburyEvans'" select="'https://viaf.org/viaf/297015946'"/>
         <xsl:map-entry key="'Macmillan'" select="'https://viaf.org/viaf/146646454'"/>
         <xsl:map-entry key="'HurstBlackett'" select="'https://viaf.org/viaf/151635949'"/>
         <xsl:map-entry key="'ChapmanHall'" select="'https://viaf.org/viaf/151283596'"/>
         <xsl:map-entry key="'Harpers'" select="'https://viaf.org/viaf/123151600'"/>
         <xsl:map-entry key="'Hachette'" select="'https://viaf.org/viaf/159641835'"/>
         <xsl:map-entry key="'SmithElder'" select="'https://viaf.org/viaf/147851790'"/>
         <xsl:map-entry key="'TicknorFields'" select="'https://viaf.org/viaf/262140940'"/>
         <xsl:map-entry key="'HoughtonMifflin'" select="'https://viaf.org/viaf/159556432'"/>
         <xsl:map-entry key="'WilliamsNorgate'" select="'https://viaf.org/viaf/157505963'"/>
      </xsl:map>
   </xsl:variable>
   <xsl:variable name="wikidata_org_ref" as="map(xs:string, xs:string)">
      <xsl:map>
         <xsl:map-entry key="'Chambers'" select="'http://www.wikidata.org/wiki/Q22811304'"/>
         <xsl:map-entry key="'HodgsonGraves'" select="'http://www.wikidata.org/wiki/Q27517845'"/>
         <xsl:map-entry key="'StationersCompany'"
                        select="'http://www.wikidata.org/wiki/Q1013382'"/>
         <xsl:map-entry key="'MertonCollege'" select="'http://www.wikidata.org/wiki/Q82513'"/>
         <xsl:map-entry key="'UniversityOfOxford'"
                        select="'http://www.wikidata.org/wiki/Q34433'"/>
         <xsl:map-entry key="'BradburyEvans'" select="'http://www.wikidata.org/wiki/Q2923456'"/>
         <xsl:map-entry key="'Macmillan'" select="'http://www.wikidata.org/wiki/Q2108217'"/>
         <xsl:map-entry key="'HurstBlackett'" select="'http://www.wikidata.org/wiki/Q16994498'"/>
         <xsl:map-entry key="'ChapmanHall'" select="'http://www.wikidata.org/wiki/Q2054191'"/>
         <xsl:map-entry key="'Harpers'" select="'http://www.wikidata.org/wiki/Q3127696'"/>
         <xsl:map-entry key="'Hachette'" select="'http://www.wikidata.org/wiki/Q349566'"/>
         <xsl:map-entry key="'SmithElder'" select="'http://www.wikidata.org/wiki/Q2295283'"/>
         <xsl:map-entry key="'TicknorFields'" select="'http://www.wikidata.org/wiki/Q7800720'"/>
         <xsl:map-entry key="'HoughtonMifflin'" select="'http://www.wikidata.org/wiki/Q390074'"/>
         <xsl:map-entry key="'WilliamsNorgate'"
                        select="'http://www.wikidata.org/wiki/Q2581203'"/>
      </xsl:map>
   </xsl:variable>
   <xsl:variable name="isni_org_ref" as="map(xs:string, xs:string)">
      <xsl:map>
         <xsl:map-entry key="'Chambers'" select="'http://www.isni.org/0000000121116684'"/>
         <xsl:map-entry key="'ParkerAndSon'" select="'http://www.isni.org/0000000064149171'"/>
         <xsl:map-entry key="'StationersCompany'"
                        select="'http://www.isni.org/0000000120974388'"/>
         <xsl:map-entry key="'MertonCollege'" select="'http://www.isni.org/0000000123539744'"/>
         <xsl:map-entry key="'UniversityOfOxford'"
                        select="'http://www.isni.org/0000000419368948'"/>
         <xsl:map-entry key="'Macmillan'" select="'http://www.isni.org/0000000403741701'"/>
         <xsl:map-entry key="'ChapmanHall'" select="'http://www.isni.org/0000000121853369'"/>
         <xsl:map-entry key="'Harpers'" select="'http://www.isni.org/0000000110092795'"/>
         <xsl:map-entry key="'Hachette'" select="'http://www.isni.org/0000000098944966'"/>
         <xsl:map-entry key="'HoughtonMifflin'" select="'http://www.isni.org/0000000092181988'"/>
         <xsl:map-entry key="'WilliamsNorgate'" select="'http://www.isni.org/0000000105181312'"/>
      </xsl:map>
   </xsl:variable>
</xsl:stylesheet>
