<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xi="http://www.w3.org/2001/XInclude"
    exclude-result-prefixes="xs"
    version="2.0"
    xmlns:tei="http://www.tei-c.org/ns/1.0">
    <xsl:output method="text" encoding="utf-8" />
    
    
    <xsl:param name="delim" select="';'" />
    <xsl:param name="quote" select="'&quot;'" />
    <xsl:variable name="newline" select="'&#10;'" />
    
    <xsl:template match="TEI">
        <xsl:value-of select="$quote"/>
        <xsl:text>Entity_type</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>Xml_id</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/> 
        <xsl:text>Wikidata ID</xsl:text>        
        <xsl:value-of select="$quote"/> 
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>Geonames_ID</xsl:text>
        <xsl:value-of select="$quote"/> 
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>VIAF_ID</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>ULAN_ID</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>TGN_ID</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>ISNI_ID</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$newline"/>        
        <xsl:apply-templates select="descendant::div[@type = 'HistoricalPeople' or @type='Organizations' or @type='Events' or @type='Periodicals' or contains(@type,'Places') or contains(@type,'Works')]"/>
    </xsl:template>
   
   
    <xsl:template match="div[@type = 'HistoricalPeople' ]">
        <xsl:apply-templates select="listPerson"/>
    </xsl:template>
    <xsl:template match="div[@type='Organizations']">
        <xsl:apply-templates select="listOrg"/>        
    </xsl:template> 
    <xsl:template match="div[@type='Events']">
        <xsl:apply-templates select="listEvent"/>        
    </xsl:template>
    <xsl:template match="div[@type = 'Periodicals' ]">
        <xsl:apply-templates select="listBibl"/>
    </xsl:template>
    <xsl:template match="div[contains(@type,'Places')]">
        <xsl:apply-templates select="listPlace"/>
    </xsl:template>
    <xsl:template match="div[contains(@type,'Works')]">
        <xsl:apply-templates select="listBibl"/>
    </xsl:template>
    
    <xsl:template match="listPerson">
        <xsl:apply-templates select="person[@xml:id]"/>
    </xsl:template>
    <xsl:template match="listOrg">
        <xsl:apply-templates select="org[@xml:id]"/>
    </xsl:template>   
    <xsl:template match="listEvent">
        <xsl:apply-templates select="event[@xml:id]"/>
    </xsl:template>   
    <xsl:template match="listBibl">
        <xsl:apply-templates select="bibl[@xml:id]"/>
    </xsl:template>   
    <xsl:template match="listPlace">
        <xsl:apply-templates select="place[@xml:id]"/>
    </xsl:template>  
    
    
    <xsl:template match="person">
        <xsl:value-of select="$quote"/>
        <xsl:text>person</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="@xml:id"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:choose>  
            <xsl:when test="idno[contains(@type,'WIKIDATA')]">
                <xsl:text>1</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>0</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:choose>  
            <xsl:when test="idno[contains(@type,'GEONAME')]">
                <xsl:text>1</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>0</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:value-of select="$quote"/>        
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:choose>  
            <xsl:when test="idno[contains(@type,'VIAF')]">
                <xsl:text>1</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>0</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:choose>  
            <xsl:when test="idno[contains(@type,'GETTY')]">
                <xsl:text>1</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>0</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>0</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
         <xsl:choose>  
            <xsl:when test="idno[contains(@type,'ISNI')]">
                <xsl:text>1</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>0</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:value-of select="$quote"/>  
        <xsl:value-of select="$newline"/>
    </xsl:template>   
     
    <xsl:template match="org">
        <xsl:value-of select="$quote"/>
        <xsl:text>organization</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="@xml:id"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:choose>  
            <xsl:when test="idno[contains(@type,'WIKIDATA')]">
                <xsl:text>1</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>0</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:choose>  
            <xsl:when test="idno[contains(@type,'GEONAME')]">
                <xsl:text>1</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>0</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:value-of select="$quote"/>        
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:choose>  
            <xsl:when test="idno[contains(@type,'VIAF')]">
                <xsl:text>1</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>0</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>0</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>0</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
         <xsl:choose>  
             <xsl:when test="idno[contains(@type,'INSI')]">
                <xsl:text>1</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>0</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:value-of select="$quote"/>  
        <xsl:value-of select="$newline"/>
    </xsl:template>
      
    <xsl:template match="event">
        <xsl:value-of select="$quote"/>
        <xsl:text>event</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="@xml:id"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:choose>  
            <xsl:when test="idno[contains(@type,'WIKIDATA')]">
                <xsl:text>1</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>0</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:choose>  
            <xsl:when test="idno[contains(@type,'GEONAME')]">
                <xsl:text>1</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>0</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:value-of select="$quote"/>        
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:choose>  
            <xsl:when test="idno[contains(@type,'VIAF')]">
                <xsl:text>1</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>0</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>0</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>0</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
         <xsl:choose>  
             <xsl:when test="idno[contains(@type,'INSI')]">
                <xsl:text>1</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>0</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:value-of select="$quote"/>  
        <xsl:value-of select="$newline"/>
    </xsl:template>

    <xsl:template match="bibl">
        <xsl:value-of select="$quote"/>
        <xsl:text>bibl or periodical</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="@xml:id"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:choose>  
            <xsl:when test="idno[contains(@type,'WIKIDATA')]">
                <xsl:text>1</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>0</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:choose>  
            <xsl:when test="idno[contains(@type,'GEONAME')]">
                <xsl:text>1</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>0</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:value-of select="$quote"/>        
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:choose>  
            <xsl:when test="idno[contains(@type,'VIAF')]">
                <xsl:text>1</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>0</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>0</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>0</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
         <xsl:choose>  
             <xsl:when test="idno[contains(@type,'INSI')]">
                <xsl:text>1</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>0</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:value-of select="$quote"/>  
        <xsl:value-of select="$newline"/>
    </xsl:template>
    <xsl:template match="place">
        <xsl:value-of select="$quote"/>
        <xsl:text>place</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="@xml:id"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:choose>  
            <xsl:when test="idno[contains(@type,'WIKIDATA')]">
                <xsl:text>1</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>0</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:choose>  
            <xsl:when test="idno[contains(@type,'GEONAME')]">
                <xsl:text>1</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>0</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:value-of select="$quote"/>        
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:choose>  
            <xsl:when test="idno[contains(@type,'VIAF')]">
                <xsl:text>1</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>0</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>0</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:choose>  
            <xsl:when test="idno[contains(@type,'GETTY')]">
                <xsl:text>1</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>0</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
         <xsl:choose>  
             <xsl:when test="idno[contains(@type,'INSI')]">
                <xsl:text>1</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>0</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:value-of select="$quote"/>  
        <xsl:value-of select="$newline"/>
    </xsl:template>
    
</xsl:stylesheet>
