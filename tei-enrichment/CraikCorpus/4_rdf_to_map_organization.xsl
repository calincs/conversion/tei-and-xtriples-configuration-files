<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:schema="https://schema.org/"
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:map="http://www.w3.org/2005/xpath-functions/map"
                version="3.0">
   <xsl:variable name="viaf_org_ref" as="map(xs:string, xs:string)">
      <xsl:map>
         <xsl:map-entry key="'Chambers'" select="'https://viaf.org/viaf/123761543/rdf.xml'"/>
         <xsl:map-entry key="'ParkerAndSon'" select="'https://viaf.org/viaf/41709233/rdf.xml'"/>
         <xsl:map-entry key="'StationersCompany'"
                        select="'https://viaf.org/viaf/134959913/rdf.xml'"/>
         <xsl:map-entry key="'MertonCollege'"
                        select="'https://viaf.org/viaf/311112809/rdf.xml'"/>
         <xsl:map-entry key="'UniversityOfOxford'"
                        select="'https://viaf.org/viaf/142129514/rdf.xml'"/>
         <xsl:map-entry key="'BradburyEvans'"
                        select="'https://viaf.org/viaf/297015946/rdf.xml'"/>
         <xsl:map-entry key="'Macmillan'" select="'https://viaf.org/viaf/146646454/rdf.xml'"/>
         <xsl:map-entry key="'HurstBlackett'"
                        select="'https://viaf.org/viaf/151635949/rdf.xml'"/>
         <xsl:map-entry key="'ChapmanHall'" select="'https://viaf.org/viaf/151283596/rdf.xml'"/>
         <xsl:map-entry key="'Harpers'" select="'https://viaf.org/viaf/123151600/rdf.xml'"/>
         <xsl:map-entry key="'Hachette'" select="'https://viaf.org/viaf/159641835/rdf.xml'"/>
         <xsl:map-entry key="'SmithElder'" select="'https://viaf.org/viaf/147851790/rdf.xml'"/>
         <xsl:map-entry key="'TicknorFields'"
                        select="'https://viaf.org/viaf/262140940/rdf.xml'"/>
         <xsl:map-entry key="'HoughtonMifflin'"
                        select="'https://viaf.org/viaf/159556432/rdf.xml'"/>
         <xsl:map-entry key="'WilliamsNorgate'"
                        select="'https://viaf.org/viaf/157505963/rdf.xml'"/>
      </xsl:map>
   </xsl:variable>
   <xsl:variable name="wikidata_org_ref" as="map(xs:string, xs:string)">
      <xsl:map>
         <xsl:map-entry key="'Chambers'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q22811304.rdf'"/>
         <xsl:map-entry key="'HodgsonGraves'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q27517845.rdf'"/>
         <xsl:map-entry key="'StationersCompany'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1013382.rdf'"/>
         <xsl:map-entry key="'MertonCollege'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q82513.rdf'"/>
         <xsl:map-entry key="'UniversityOfOxford'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q34433.rdf'"/>
         <xsl:map-entry key="'BradburyEvans'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2923456.rdf'"/>
         <xsl:map-entry key="'Macmillan'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2108217.rdf'"/>
         <xsl:map-entry key="'HurstBlackett'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q16994498.rdf'"/>
         <xsl:map-entry key="'ChapmanHall'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2054191.rdf'"/>
         <xsl:map-entry key="'Harpers'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3127696.rdf'"/>
         <xsl:map-entry key="'Hachette'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q349566.rdf'"/>
         <xsl:map-entry key="'SmithElder'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2295283.rdf'"/>
         <xsl:map-entry key="'TicknorFields'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q7800720.rdf'"/>
         <xsl:map-entry key="'HoughtonMifflin'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q390074.rdf'"/>
         <xsl:map-entry key="'WilliamsNorgate'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2581203.rdf'"/>
      </xsl:map>
   </xsl:variable>
   <xsl:variable name="isni_org_ref" as="map(xs:string, xs:string)">
      <xsl:map>
         <xsl:map-entry key="'Chambers'" select="'http://www.isni.org/0000000121116684.xml'"/>
         <xsl:map-entry key="'ParkerAndSon'"
                        select="'http://www.isni.org/0000000064149171.xml'"/>
         <xsl:map-entry key="'StationersCompany'"
                        select="'http://www.isni.org/0000000120974388.xml'"/>
         <xsl:map-entry key="'MertonCollege'"
                        select="'http://www.isni.org/0000000123539744.xml'"/>
         <xsl:map-entry key="'UniversityOfOxford'"
                        select="'http://www.isni.org/0000000419368948.xml'"/>
         <xsl:map-entry key="'Macmillan'" select="'http://www.isni.org/0000000403741701.xml'"/>
         <xsl:map-entry key="'ChapmanHall'" select="'http://www.isni.org/0000000121853369.xml'"/>
         <xsl:map-entry key="'Harpers'" select="'http://www.isni.org/0000000110092795.xml'"/>
         <xsl:map-entry key="'Hachette'" select="'http://www.isni.org/0000000098944966.xml'"/>
         <xsl:map-entry key="'HoughtonMifflin'"
                        select="'http://www.isni.org/0000000092181988.xml'"/>
         <xsl:map-entry key="'WilliamsNorgate'"
                        select="'http://www.isni.org/0000000105181312.xml'"/>
      </xsl:map>
   </xsl:variable>
</xsl:stylesheet>
