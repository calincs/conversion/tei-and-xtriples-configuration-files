<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:schema="https://schema.org/"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    xmlns:tei="http://www.tei-c/ns/1.0"
    
    
    xpath-default-namespace="http://www.tei-c.org/ns/1.0" version="3.0">
    <xsl:output method="text" encoding="utf-8" />
    
    <xsl:param name="delim" select="';'" />
    <xsl:param name="quote" select="'&quot;'" />
    <xsl:variable name="newline" select="'&#10;'" />
    
    <xsl:template match="/">
        <xsl:value-of select="$quote"/>
        <xsl:text>Xml_id</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>Name</xsl:text>
        <xsl:value-of select="$quote"/>       
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>Type</xsl:text>
        <xsl:value-of select="$quote"/>        
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>When</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$newline"/>
        <xsl:apply-templates select="descendant::*/listEvent"/>
    </xsl:template>
    
    <xsl:template match="listEvent">
        <xsl:apply-templates select="event[@xml:id]"/>
    </xsl:template>
    
    <xsl:template match="event">
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="@xml:id"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>  
        <xsl:value-of select="label"/>
        <xsl:value-of select="$quote"/> 
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>  
        <xsl:value-of select="@type"/> 
        <xsl:value-of select="$quote"/>   
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>  
        <xsl:choose>
            <xsl:when test="@from and @to">         
                <xsl:value-of select="@from"/> 
                <xsl:text>-</xsl:text>
                <xsl:value-of select="@to"/> 
            </xsl:when>
            <xsl:when test="@when">                    
                <xsl:value-of select="@when"/> 
            </xsl:when>
        </xsl:choose>
        <xsl:value-of select="$quote"/> 
        <xsl:value-of select="$newline"/>
    </xsl:template>
    
    
</xsl:stylesheet>