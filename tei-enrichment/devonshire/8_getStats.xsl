<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xi="http://www.w3.org/2001/XInclude"
    exclude-result-prefixes="xs"
    version="2.0"
    xmlns:tei="http://www.tei-c.org/ns/1.0">
    <xsl:output method="text" encoding="utf-8" />
    
    
    <xsl:param name="delim" select="';'" />
    <xsl:param name="quote" select="'&quot;'" />
    <xsl:variable name="newline" select="'&#10;'" />
    
    <xsl:template match="TEI">
        <xsl:value-of select="$quote"/>
        <xsl:text>Entity_type</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>Xml_id</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/> 
        <xsl:text>Wikidata ID</xsl:text>        
        <xsl:value-of select="$quote"/> 
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>Geonames_ID</xsl:text>
        <xsl:value-of select="$quote"/> 
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>VIAF_ID</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>ULAN_ID</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>TGN_ID</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>ISNI_ID</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$newline"/>        
        <xsl:apply-templates select="teiHeader/profileDesc/particDesc"/>
    </xsl:template>
   

    <xsl:template match="particDesc">
        <xsl:apply-templates select="person[@xml:id]"/>
    </xsl:template>
    
    
    <xsl:template match="person">
        <xsl:value-of select="$quote"/>
        <xsl:text>person</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="@xml:id"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:choose>  
            <xsl:when test="idno[contains(@type,'WIKIDATA')]">
                <xsl:text>1</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>0</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:choose>  
            <xsl:when test="idno[contains(@type,'GEONAME')]">
                <xsl:text>1</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>0</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:value-of select="$quote"/>        
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:choose>  
            <xsl:when test="idno[contains(@type,'VIAF')]">
                <xsl:text>1</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>0</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:choose>  
            <xsl:when test="idno[contains(@type,'GETTY')]">
                <xsl:text>1</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>0</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>0</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
         <xsl:choose>  
            <xsl:when test="idno[contains(@type,'ISNI')]">
                <xsl:text>1</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>0</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:value-of select="$quote"/>  
        <xsl:value-of select="$newline"/>
    </xsl:template>   

</xsl:stylesheet>
