<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="3.0">
    <xsl:output method="text" encoding="utf-8" />
    
    
    <xsl:param name="delim" select="';'" />
    <xsl:param name="quote" select="'&quot;'" />
    <xsl:variable name="newline" select="'&#10;'" />
    
    <xsl:template match="TEI">
        <xsl:value-of select="$quote"/>
        <xsl:text>Xml_id</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>Names</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>Role</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>DNB ID</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>ILOC ID</xsl:text>
        <xsl:value-of select="$quote"/>   
        <xsl:value-of select="$newline"/>
        <xsl:apply-templates select="teiHeader/profileDesc/particDesc/listPerson"/>
    </xsl:template>
    
    <xsl:template match="listPerson[@type='historical']">
        <xsl:apply-templates select="person[@xml:id]"/>
    </xsl:template>
    
    <xsl:template match="listPerson"/>
    <xsl:template match="person">
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="@xml:id"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:apply-templates select="persName[1]"/>        
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="@role"/>
        <xsl:value-of select="$quote"/>        
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:if test="@xml:id!='CLERE'">
            <xsl:apply-templates select="persName[1]/ref[@type='DNB']"/>  
        </xsl:if>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="replace(persName[1]/ref[@type='LOC']/text(),' ','')"/>
        <xsl:value-of select="$quote"/>  
        <xsl:value-of select="$newline"/>
    </xsl:template>
    
    <xsl:template match="persName[1]/ref[@type='DNB']"> 
        
        <xsl:param name="txt" select="tokenize(text(),'/')" />
        <xsl:value-of select="$txt[last()]"></xsl:value-of>
    </xsl:template>
    
    <xsl:template match="persName[1]">

        <xsl:value-of select="$quote"/>
        <xsl:choose>
            <xsl:when test="forename and surname">
                <xsl:value-of select="forename" separator=", "/>
                <xsl:text> </xsl:text>                
                <xsl:value-of select="surname" separator=", "/>
                
            </xsl:when>

            <xsl:when test="(forename or surname) and roleName">

                <xsl:value-of select="roleName"/>
                <xsl:text>, </xsl:text>
                <xsl:if test="forename">
                    <xsl:value-of select="forename"/>
                </xsl:if>
                <xsl:if test="surname">
                    <xsl:value-of select="surname"/>
                </xsl:if>
                  
            </xsl:when>

        </xsl:choose>

        <xsl:value-of select="$quote"/>
    </xsl:template>
    
</xsl:stylesheet>