<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:schema="https://schema.org/"
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:map="http://www.w3.org/2005/xpath-functions/map"
                version="3.0">
   <xsl:variable name="viaf_pers_ref" as="map(xs:string, xs:string)">
      <xsl:map>
         <xsl:map-entry key="'WYATT'"
                        select="'https://viaf.org/viaf/44340301/rdf.xml'"/>
         <xsl:map-entry key="'LEE'"
                        select="'https://viaf.org/viaf/121217313/rdf.xml'"/>
         <xsl:map-entry key="'CHAUCER'"
                        select="'https://viaf.org/viaf/100185203/rdf.xml'"/>
         <xsl:map-entry key="'SURREY'"
                        select="'https://viaf.org/viaf/17378697/rdf.xml'"/>
         <xsl:map-entry key="'SHELTON'"
                        select="'https://viaf.org/viaf/88671425/rdf.xml'"/>
         <xsl:map-entry key="'QUEEN'"
                        select="'https://viaf.org/viaf/29521340/rdf.xml'"/>
         <xsl:map-entry key="'MARY_SCOTS'"
                        select="'https://viaf.org/viaf/104722318/rdf.xml'"/>
         <xsl:map-entry key="'DOUGLAS'"
                        select="'https://viaf.org/viaf/69863628/rdf.xml'"/>
         <xsl:map-entry key="'M_HOWARD'"
                        select="'https://viaf.org/viaf/88670504/rdf.xml'"/>
         <xsl:map-entry key="'DARNLEY'"
                        select="'https://viaf.org/viaf/40190630/rdf.xml'"/>
         <xsl:map-entry key="'AQUILANO'"
                        select="'https://viaf.org/viaf/44449408/rdf.xml'"/>
         <xsl:map-entry key="'PETRARCH'"
                        select="'https://viaf.org/viaf/39382430/rdf.xml'"/>
         <xsl:map-entry key="'KING'"
                        select="'https://viaf.org/viaf/172419710/rdf.xml'"/>
         <xsl:map-entry key="'GOWER'"
                        select="'https://viaf.org/viaf/120695167/rdf.xml'"/>
         <xsl:map-entry key="'ALAMANNI'"
                        select="'https://viaf.org/viaf/100187087/rdf.xml'"/>
         <xsl:map-entry key="'PISAN'"
                        select="'https://viaf.org/viaf/100188547/rdf.xml'"/>
         <xsl:map-entry key="'HOCCLEVE'"
                        select="'https://viaf.org/viaf/9902409/rdf.xml'"/>
         <xsl:map-entry key="'CHARTIER'"
                        select="'https://viaf.org/viaf/57146825200007631661/rdf.xml'"/>
         <xsl:map-entry key="'ROOS'"
                        select="'https://viaf.org/viaf/69338801/rdf.xml'"/>
      </xsl:map>
   </xsl:variable>
   <xsl:variable name="wikidata_pers_ref" as="map(xs:string, xs:string)">
      <xsl:map>
         <xsl:map-entry key="'WYATT'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q314325.rdf'"/>
         <xsl:map-entry key="'LEE'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5724773.rdf'"/>
         <xsl:map-entry key="'KNYVET'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5339618.rdf'"/>
         <xsl:map-entry key="'CHAUCER'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5683.rdf'"/>
         <xsl:map-entry key="'SURREY'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q372211.rdf'"/>
         <xsl:map-entry key="'SHELTON'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q18674204.rdf'"/>
         <xsl:map-entry key="'QUEEN'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q80823.rdf'"/>
         <xsl:map-entry key="'MARY_SCOTS'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q131412.rdf'"/>
         <xsl:map-entry key="'DOUGLAS'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q433161.rdf'"/>
         <xsl:map-entry key="'M_HOWARD'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2663072.rdf'"/>
         <xsl:map-entry key="'TH_HOWARD'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q201153.rdf'"/>
         <xsl:map-entry key="'DARNLEY'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q312381.rdf'"/>
         <xsl:map-entry key="'AQUILANO'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2370791.rdf'"/>
         <xsl:map-entry key="'CLERE'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5339618.rdf'"/>
         <xsl:map-entry key="'PETRARCH'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1401.rdf'"/>
         <xsl:map-entry key="'KING'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q38370.rdf'"/>
         <xsl:map-entry key="'GOWER'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q365133.rdf'"/>
         <xsl:map-entry key="'ALAMANNI'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q721846.rdf'"/>
         <xsl:map-entry key="'PISAN'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q234816.rdf'"/>
         <xsl:map-entry key="'HOCCLEVE'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1227741.rdf'"/>
         <xsl:map-entry key="'CHARTIER'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q982038.rdf'"/>
         <xsl:map-entry key="'ROOS'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q7328733.rdf'"/>
      </xsl:map>
   </xsl:variable>
   <xsl:variable name="isni_pers_ref" as="map(xs:string, xs:string)">
      <xsl:map>
         <xsl:map-entry key="'WYATT'"
                        select="'http://www.isni.org/0000000108928784.xml'"/>
         <xsl:map-entry key="'LEE'"
                        select="'http://www.isni.org/000000007930105X.xml'"/>
         <xsl:map-entry key="'CHAUCER'"
                        select="'http://www.isni.org/000000011557454X.xml'"/>
         <xsl:map-entry key="'SURREY'"
                        select="'http://www.isni.org/0000000116008072.xml'"/>
         <xsl:map-entry key="'SHELTON'"
                        select="'http://www.isni.org/0000000062122215.xml'"/>
         <xsl:map-entry key="'QUEEN'"
                        select="'http://www.isni.org/0000000096371375.xml'"/>
         <xsl:map-entry key="'MARY_SCOTS'"
                        select="'http://www.isni.org/0000000121035913.xml'"/>
         <xsl:map-entry key="'DOUGLAS'"
                        select="'http://www.isni.org/0000000051916394.xml'"/>
         <xsl:map-entry key="'M_HOWARD'"
                        select="'http://www.isni.org/0000000062122135.xml'"/>
         <xsl:map-entry key="'DARNLEY'"
                        select="'http://www.isni.org/0000000054257814.xml'"/>
         <xsl:map-entry key="'AQUILANO'"
                        select="'http://www.isni.org/0000000121305012.xml'"/>
         <xsl:map-entry key="'PETRARCH'"
                        select="'http://www.isni.org/0000000121287790.xml'"/>
         <xsl:map-entry key="'KING'"
                        select="'http://www.isni.org/0000000122586127.xml'"/>
         <xsl:map-entry key="'GOWER'"
                        select="'http://www.isni.org/0000000109397129.xml'"/>
         <xsl:map-entry key="'ALAMANNI'"
                        select="'http://www.isni.org/0000000121451204.xml'"/>
         <xsl:map-entry key="'PISAN'"
                        select="'http://www.isni.org/0000000121033731.xml'"/>
         <xsl:map-entry key="'HOCCLEVE'"
                        select="'http://www.isni.org/0000000110377440.xml'"/>
         <xsl:map-entry key="'CHARTIER'"
                        select="'http://www.isni.org/0000000081350758.xml'"/>
      </xsl:map>
   </xsl:variable>
   <xsl:variable name="getty_pers_ref" as="map(xs:string, xs:string)">
      <xsl:map>
         <xsl:map-entry key="'CHAUCER'"
                        select="'http://vocab.getty.edu/ulan/500118907.rdf'"/>
         <xsl:map-entry key="'MARY_SCOTS'"
                        select="'http://vocab.getty.edu/ulan/500220567.rdf'"/>
         <xsl:map-entry key="'PETRARCH'"
                        select="'http://vocab.getty.edu/ulan/500323951.rdf'"/>
         <xsl:map-entry key="'KING'"
                        select="'http://vocab.getty.edu/ulan/500205051.rdf'"/>
      </xsl:map>
   </xsl:variable>
   <xsl:variable name="dnb_pers_ref" as="map(xs:string, xs:string)">
      <xsl:map>
         <xsl:map-entry key="'WYATT'" select="'30111'"/>
         <xsl:map-entry key="'LEE'" select="'16288'"/>
         <xsl:map-entry key="'KNYVET'" select="'15797'"/>
         <xsl:map-entry key="'CHAUCER'" select="'5191'"/>
         <xsl:map-entry key="'SURREY'" select="'13905'"/>
         <xsl:map-entry key="'SHELTON'" select="'68085'"/>
         <xsl:map-entry key="'QUEEN'" select="'557'"/>
         <xsl:map-entry key="'MARY_SCOTS'" select="'18248'"/>
         <xsl:map-entry key="'DOUGLAS'" select="'7911'"/>
         <xsl:map-entry key="'M_HOWARD'" select="'9638'"/>
         <xsl:map-entry key="'TH_HOWARD'" select="'70793'"/>
         <xsl:map-entry key="'DARNLEY'" select="'26473'"/>
         <xsl:map-entry key="'KING'" select="'12955'"/>
         <xsl:map-entry key="'GOWER'" select="'11176'"/>
         <xsl:map-entry key="'HOCCLEVE'" select="'13415'"/>
         <xsl:map-entry key="'ROOS'" select="'37912'"/>
      </xsl:map>
   </xsl:variable>
   <xsl:variable name="loc_pers_ref" as="map(xs:string, xs:string)">
      <xsl:map>
         <xsl:map-entry key="'WYATT'" select="'n50019425.rdf'"/>
         <xsl:map-entry key="'LEE'" select="'no2010077487.rdf'"/>
         <xsl:map-entry key="'CHAUCER'" select="'n79027228.rdf'"/>
         <xsl:map-entry key="'SURREY'" select="'n50083266.rdf'"/>
         <xsl:map-entry key="'QUEEN'" select="'n79063685.rdf'"/>
         <xsl:map-entry key="'MARY_SCOTS'" select="'n80044764.rdf'"/>
         <xsl:map-entry key="'DOUGLAS'" select="'nr93026084.rdf'"/>
         <xsl:map-entry key="'M_HOWARD'" select="'no2012098273.rdf'"/>
         <xsl:map-entry key="'DARNLEY'" select="'nr93013805.rdf'"/>
         <xsl:map-entry key="'AQUILANO'" select="'n83012051.rdf'"/>
         <xsl:map-entry key="'PETRARCH'" select="'n79092622.rdf'"/>
         <xsl:map-entry key="'KING'" select="'n79113093.rdf'"/>
         <xsl:map-entry key="'GOWER'" select="'n81039793.rdf'"/>
         <xsl:map-entry key="'ALAMANNI'" select="'n84174406.rdf'"/>
         <xsl:map-entry key="'PISAN'" select="'n50082339.rdf'"/>
         <xsl:map-entry key="'HOCCLEVE'" select="'n82125108.rdf'"/>
         <xsl:map-entry key="'CHARTIER'" select="'n84091919.rdf'"/>
      </xsl:map>
   </xsl:variable>
</xsl:stylesheet>
