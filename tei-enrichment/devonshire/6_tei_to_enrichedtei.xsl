<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
    xmlns="http://www.tei-c.org/ns/1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:map="http://www.w3.org/2005/xpath-functions/map"
    xmlns:tei="http://www.tei-c/ns/1.0"     
    exclude-result-prefixes="xsl tei xs "
    version="2.0">
    <xsl:strip-space elements="*"/>
    <xsl:output method="xml" indent="yes"/>


    <xsl:include href="5_xsl_map_person.xslt"/>
    

    <xsl:template match="node()| @*">
        <xsl:copy >
            <xsl:apply-templates select="node() | @*"/>
        </xsl:copy>
    </xsl:template>


    <xsl:template match="listPerson[@type='historical']">
        <xsl:apply-templates select="person[@xml:id]"/>
    </xsl:template>

    <xsl:template match="person[@xml:id]" >
        <xsl:variable name="myId" select="@xml:id"/>

        <xsl:copy >
            <xsl:apply-templates select="@*"/>
            <xsl:if test="map:contains($wikidata_pers_ref, $myId)">
                <xsl:element name="idno" >
                    <xsl:attribute name="type">WIKIDATA</xsl:attribute>
                    <xsl:value-of select="$wikidata_pers_ref($myId)"/>
                </xsl:element>
            </xsl:if>
            <xsl:if test="map:contains($isni_pers_ref, $myId)">
                <xsl:element name="idno" >
                    <xsl:attribute name="type">INSI</xsl:attribute>
                    <xsl:value-of select="$isni_pers_ref($myId)"/>
                </xsl:element>
            </xsl:if>
            <xsl:if test="map:contains($viaf_pers_ref, $myId)">
                <xsl:element name="idno" >
                    <xsl:attribute name="type">VIAF</xsl:attribute>
                    <xsl:value-of select="$viaf_pers_ref($myId)"/>
                </xsl:element>
            </xsl:if>
            <xsl:if test="map:contains($getty_pers_ref, $myId)">
                <xsl:element name="idno" >
                    <xsl:attribute name="type">GETTY</xsl:attribute>
                    <xsl:value-of select="$getty_pers_ref($myId)"/>
                </xsl:element>
            </xsl:if>

            <xsl:copy-of select="birth"></xsl:copy-of>
            <xsl:copy-of select="death"></xsl:copy-of>

            <xsl:for-each select="persName">
                <xsl:choose>
                    <xsl:when test="(map:contains($loc_pers_ref, $myId) or map:contains($dnb_pers_ref, $myId)) and (ref[@type='LOC'] or ref[@type='DNB']) ">
                        <xsl:copy>
                            <xsl:apply-templates select="@* | node() except ref"/>
                            <xsl:choose>
                                <xsl:when  test="map:contains($loc_pers_ref, $myId)">
                                    <xsl:element name="ref" >
                                        <xsl:attribute name="type">LOC</xsl:attribute>
                                        <xsl:value-of select="$loc_pers_ref($myId)"/>
                                    </xsl:element>
                                </xsl:when>
                                <xsl:when test="ref[@type='LOC']">
                                    
                                    <xsl:copy-of select="."></xsl:copy-of>
                                </xsl:when>
                            </xsl:choose>
                            <xsl:choose>
                                <xsl:when  test="map:contains($dnb_pers_ref, $myId)">
                                    <xsl:element name="ref" >
                                        <xsl:attribute name="type">DNB</xsl:attribute>
                                        <xsl:value-of select="$dnb_pers_ref($myId)"/>
                                    </xsl:element>
                                </xsl:when>
                                <xsl:when test="ref[@type='DNB']">
                                    
                                    <xsl:copy-of select="."></xsl:copy-of>
                                </xsl:when>
                            </xsl:choose>
                        </xsl:copy>
                        
                        
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:copy>
                            <xsl:apply-templates select="node() | @*"/>
                        </xsl:copy>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
            
        </xsl:copy>
        
        
        
        
    </xsl:template>
    
</xsl:stylesheet>