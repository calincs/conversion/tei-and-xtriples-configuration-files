<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
    xmlns="http://www.tei-c.org/ns/1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:map="http://www.w3.org/2005/xpath-functions/map"
    xmlns:tei="http://www.tei-c/ns/1.0"     
    xpath-default-namespace="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes=" xsl   tei xs"
    version="3.0">
    <xsl:strip-space elements="*"/>
    <xsl:output method="xml" indent="yes"/>
    <xsl:include href="5_person_map.xslt"/>
    <xsl:include href="5_place_map.xslt"/>
   
    <xsl:template match="node()| @*">
        <xsl:copy>
            <xsl:apply-templates select="node() | @*"/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="persName[@ref]" >
        <xsl:variable name="myId" select="@ref"/>
        <xsl:copy>
            <xsl:apply-templates select=" @*"/>            
            <xsl:if test="map:contains($wikidata_pers_ref, $myId)">
                <xsl:element name="idno" >
                    <xsl:attribute name="type">WIKIDATA</xsl:attribute>
                    <xsl:value-of select="$wikidata_pers_ref($myId)"/>
                </xsl:element>
            </xsl:if>
            <xsl:if test="map:contains($isni_pers_ref, $myId)">
                <xsl:element name="idno" >
                    <xsl:attribute name="type">INSI</xsl:attribute>
                    <xsl:value-of select="$isni_pers_ref($myId)"/>
                </xsl:element>
            </xsl:if>
            <xsl:value-of select="text()"></xsl:value-of>
        </xsl:copy>
        
    </xsl:template>
    <xsl:template match="placeName[@ref]" >
        ​<xsl:variable name="myId" select="@ref"/>
    ​    <xsl:copy>
            <xsl:apply-templates select=" @*"/>
            <xsl:if test="map:contains($wikidata_place_ref, $myId)">
                <xsl:element name="idno" >
                    <xsl:attribute name="type">WIKIDATA</xsl:attribute>
                    <xsl:value-of select="$wikidata_place_ref($myId)"/>
                </xsl:element>
            </xsl:if>            
            <xsl:value-of select="text()"></xsl:value-of>
        </xsl:copy>
        
        
    </xsl:template>
    
    
 
</xsl:stylesheet>