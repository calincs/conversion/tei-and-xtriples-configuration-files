<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:schema="https://schema.org/"
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:map="http://www.w3.org/2005/xpath-functions/map"
                version="3.0">
   <xsl:variable name="wikidata_place_ref" as="map(xs:string, xs:string)">
      <xsl:map>
         <xsl:map-entry key="'http://www.geonames.org/6453366'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5245991.rdf'"/>
         <xsl:map-entry key="'http://www.geonames.org/2673722'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q104231.rdf'"/>
         <xsl:map-entry key="'http://www.geonames.org/6548528'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q46506.rdf'"/>
      </xsl:map>
   </xsl:variable>
</xsl:stylesheet>
