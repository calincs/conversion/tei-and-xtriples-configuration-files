<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:schema="https://schema.org/"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    xmlns:tei="http://www.tei-c/ns/1.0"
    
    
    xpath-default-namespace="http://www.tei-c.org/ns/1.0" version="3.0">
    <xsl:output method="text" encoding="utf-8" />
    
    <xsl:param name="delim" select="';'" />
    <xsl:param name="quote" select="'&quot;'" />
    <xsl:variable name="newline" select="'&#10;'" />
    
    <xsl:template match="TEI">
        <xsl:value-of select="$quote"/>
        <xsl:text>Viaf_ref</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>PersName</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$newline"/>
        <xsl:apply-templates select="text/body/div"/>
    </xsl:template>
    
    <xsl:template match="text/body/div">
        <xsl:for-each select="descendant::persName[@ref]">
            <xsl:value-of select="$quote"/>
            <xsl:value-of select="@ref"/>
            <xsl:value-of select="$quote"/>
            <xsl:value-of select="$delim"/>
            <xsl:value-of select="$quote"/>  
            <xsl:value-of select="text()"/>
            <xsl:value-of select="$quote"/>        
            <xsl:value-of select="$newline"/>
        </xsl:for-each>
    </xsl:template>
    
    
</xsl:stylesheet>