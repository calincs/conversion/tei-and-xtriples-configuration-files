<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:schema="https://schema.org/"
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:map="http://www.w3.org/2005/xpath-functions/map"
                version="3.0">
   <xsl:variable name="wikidata_pers_ref" as="map(xs:string, xs:string)">
      <xsl:map>
         <xsl:map-entry key="'279399399'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q7145817.rdf'"/>
         <xsl:map-entry key="'http://viaf.org/viaf/36924137'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q33760.rdf'"/>
      </xsl:map>
   </xsl:variable>
   <xsl:variable name="isni_pers_ref" as="map(xs:string, xs:string)">
      <xsl:map>
         <xsl:map-entry key="'http://viaf.org/viaf/36924137'"
                        select="'http://www.isni.org/0000000121006514.xml'"/>
      </xsl:map>
   </xsl:variable>
</xsl:stylesheet>
