<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0" version="3.0">
    <xsl:output method="text" encoding="utf-8" />
    
    <xsl:param name="delim" select="';'" />
    <xsl:param name="quote" select="'&quot;'" />
    <xsl:variable name="newline" select="'&#10;'" />
    
    <xsl:key name="provinces" match="place[@type='province']" use="@xml:id" />
    <xsl:key name="countries" match="place[@type='country']" use="@xml:id" />
    <xsl:key name="countries" match="place[@type='country']" use="@xml:id" />
    
    <xsl:template match="TEI">
        <xsl:value-of select="$quote"/>
        <xsl:text>Xml_id</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>type</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>country</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>province</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>Placename</xsl:text>        
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>Geo lat</xsl:text>     
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>Geo long</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$newline"/>
        <xsl:apply-templates select="text/body/listPlace/listPlace/place[@xml:id]"/>
    </xsl:template>

    
    <xsl:template match="place[@xml:id]">
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="@xml:id"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>  
        <xsl:value-of select="@type"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>  
        <xsl:choose>
            <xsl:when test="@type='country'">
                <xsl:value-of select="placeName"/>
            </xsl:when>
            <xsl:when test="@type='city'">
                <xsl:choose>
                    <xsl:when test="country">                        
                        <xsl:value-of select="country"/>
                    </xsl:when>
                    <xsl:when test="location/region[@type='country']">
                        <xsl:variable name="country_id" select="replace(location/region[@type='country']/@corresp,'#','')"/>
                        <xsl:value-of select="key('countries', $country_id)/placeName"/>
                    </xsl:when>
                </xsl:choose>
            </xsl:when>
            <xsl:when test="@type='province'">
                <xsl:if test="parent::node()[@type='USstate']">
                    <xsl:text>USA</xsl:text>
                </xsl:if>
                <xsl:if test="parent::node()[@type='province']">
                    <xsl:text>Canada</xsl:text>
                </xsl:if>
            </xsl:when>
        </xsl:choose>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>   
        <xsl:choose>
            <xsl:when test="@type='province'">
                <xsl:value-of select="placeName"/>
            </xsl:when>
            <xsl:when test="@type='city'">
                <xsl:variable name="province_id" select="replace(location/region[@type='province']/@corresp,'#','')"/>
                <xsl:value-of select="key('provinces', $province_id)/placeName"/>
            </xsl:when>
            
        </xsl:choose>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/> 
        <xsl:value-of select="placeName"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/> 
        <xsl:if test="location[1]/geo[1]">
            <xsl:value-of select="tokenize(location[1]/geo[1]/text(),' ')[1]"/>
        </xsl:if>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/> 
        <xsl:if test="location[1]/geo[1]">
            <xsl:value-of select="tokenize(location[1]/geo[1]/text(),' ')[2]"/>
        </xsl:if>
        <xsl:value-of select="$quote"/>        
        <xsl:value-of select="$newline"/>
    </xsl:template>
</xsl:stylesheet>