<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0" version="2.0">
    <xsl:output method="text" encoding="utf-8" />
    
    <xsl:param name="delim" select="';'" />
    <xsl:param name="quote" select="'&quot;'" />
    <xsl:variable name="newline" select="'&#10;'" />
    
    <xsl:param name="lk" select="'0_places.xml'"/>
    <xsl:variable name="lk-doc" select="doc($lk)"/>
    
     
    <xsl:template match="TEI">
        <xsl:value-of select="$quote"/>
        <xsl:text>Xml_id</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>name</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>place</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$newline"/>
        <xsl:apply-templates select="text/body/listOrg/org[@xml:id]"/>
    </xsl:template>
    <xsl:template match="org[@xml:id]">
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="@xml:id"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>  
        <xsl:value-of select="orgName" separator=", "/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>  
        <xsl:value-of select="$quote"/>
        
        <xsl:for-each select="placeName">
            <xsl:variable name="placeid" select="replace(@corresp,'places.xml#','')"/>
            <xsl:value-of select="$lk-doc/descendant::place[@xml:id=$placeid]/placeName/text()"/>
            <xsl:if test="position() != last()">
                <xsl:text>, </xsl:text>
            </xsl:if>
        </xsl:for-each>
        
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$newline"/>
    </xsl:template>
</xsl:stylesheet>