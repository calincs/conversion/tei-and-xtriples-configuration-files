<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0" version="2.0">
    <xsl:output method="text" encoding="utf-8" />
    
    <xsl:param name="delim" select="';'" />
    <xsl:param name="quote" select="'&quot;'" />
    <xsl:variable name="newline" select="'&#10;'" />
    
    <xsl:param name="lk_places" select="'0_places.xml'"/>
    <xsl:variable name="lk-doc_places" select="doc($lk_places)"/>
    <xsl:param name="lk_org" select="'0_organizations.xml'"/>
    <xsl:variable name="lk-doc_org" select="doc($lk_org)"/>
    
    
    <xsl:template match="TEI">
        <xsl:value-of select="$quote"/>
        <xsl:text>Xml_id</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>title</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>place</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>org</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:text>when</xsl:text>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$newline"/>
        <xsl:apply-templates select="text/body/div[@xml:id]/p"/>
    </xsl:template>
    
    <xsl:template match="div[@xml:id]/p">
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="parent::node()/@xml:id"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="title" separator=", "/>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:for-each select="placeName">
            <xsl:variable name="placeid" select="replace(@corresp,'places.xml#','')"/>
            <xsl:value-of select="$lk-doc_places/descendant::place[@xml:id=$placeid]/placeName/text()"/>
            <xsl:if test="position() != last()">
                <xsl:text>, </xsl:text>
            </xsl:if>
        </xsl:for-each>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$quote"/>
        <xsl:for-each select="orgName">
            <xsl:variable name="orgid" select="replace(@corresp,'organizations.xml#','')"/>
            <xsl:value-of select="$lk-doc_org/descendant::org[@xml:id=$orgid]/orgName/text()"/>
            <xsl:if test="position() != last()">
                <xsl:text>, </xsl:text>
            </xsl:if>
        </xsl:for-each>
        <xsl:value-of select="$quote"/>
        <xsl:value-of select="$delim"/>
        <xsl:value-of select="$newline"/>
    </xsl:template>
</xsl:stylesheet>