<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:map="http://www.w3.org/2005/xpath-functions/map"
    xmlns:tei="http://www.tei-c/ns/1.0"     
    xpath-default-namespace="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes=" xsl   tei xs"
    version="3.0">
    <xsl:strip-space elements="*"/>
    <xsl:output method="xml" indent="yes"/>


    <xsl:include href="5_periodical_map.xsl"/>
    
    <xsl:template match="node()| @*">
        <xsl:copy>
            <xsl:apply-templates select="node() | @*"/>
        </xsl:copy>
    </xsl:template>
    

    <xsl:template match="div[@xml:id]" >
        <xsl:variable name="myId" select="@xml:id"/>
        <xsl:copy>
            
            <xsl:apply-templates select="@*"/>
            <xsl:if test="map:contains($wikidata_perio_ref, $myId)">
                <xsl:element name="idno" >
                    <xsl:attribute name="type">WIKIDATA</xsl:attribute>
                    <xsl:value-of select="$wikidata_perio_ref($myId)"/>
                </xsl:element>
            </xsl:if>
            <xsl:if test="map:contains($isni_perio_ref, $myId)">
                <xsl:element name="idno" >
                    <xsl:attribute name="type">INSI</xsl:attribute>
                    <xsl:value-of select="$isni_perio_ref($myId)"/>
                </xsl:element>
            </xsl:if>
            <xsl:if test="map:contains($viaf_perio_ref, $myId)">
                <xsl:element name="idno" >
                    <xsl:attribute name="type">VIAF</xsl:attribute>
                    <xsl:value-of select="$viaf_perio_ref($myId)"/>
                </xsl:element>
            </xsl:if>
            
            <xsl:apply-templates select="node()"/>
        </xsl:copy>  
    </xsl:template>
    
</xsl:stylesheet>