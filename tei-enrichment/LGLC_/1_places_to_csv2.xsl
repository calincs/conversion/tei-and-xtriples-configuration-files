<?xml version="1.0" encoding="UTF-8"?>
    <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:xs="http://www.w3.org/2001/XMLSchema"
        exclude-result-prefixes="xs"
        xpath-default-namespace="http://www.tei-c.org/ns/1.0" version="3.0">
        <xsl:output method="text" encoding="utf-8" />
        
        <xsl:param name="delim" select="';'" />
        <xsl:param name="quote" select="'&quot;'" />
        <xsl:variable name="newline" select="'&#10;'" />
        
        <xsl:key name="provinces" match="place[@type='province']" use="@xml:id" />
        <xsl:key name="countries" match="place[@type='country']" use="@xml:id" />
        <xsl:key name="cities" match="place[@type='city']" use="@xml:id" />
        
        <xsl:template match="TEI">
            <xsl:value-of select="$quote"/>
            <xsl:text>Xml_id</xsl:text>
            <xsl:value-of select="$quote"/>
            <xsl:value-of select="$delim"/>
            <xsl:value-of select="$quote"/>
            <xsl:text>type</xsl:text>
            <xsl:value-of select="$quote"/>
            <xsl:value-of select="$delim"/>
            <xsl:value-of select="$quote"/>
            <xsl:text>country</xsl:text>
            <xsl:value-of select="$quote"/>
            <xsl:value-of select="$delim"/>
            <xsl:value-of select="$quote"/>
            <xsl:text>province</xsl:text>
            <xsl:value-of select="$quote"/>
            <xsl:value-of select="$delim"/>
            <xsl:value-of select="$quote"/>
            <xsl:text>city</xsl:text>
            <xsl:value-of select="$quote"/>
            <xsl:value-of select="$delim"/>
            <xsl:value-of select="$quote"/>
            <xsl:text>Placename</xsl:text>        
            <xsl:value-of select="$quote"/>
            <xsl:value-of select="$newline"/>
            <xsl:apply-templates select="text/body/listPlace/listPlace[@type='other']/place[@xml:id and @type!='city' and @type!='country' and @type!='province']"/>
        </xsl:template>
        
        
        <xsl:template match="place[@xml:id and @type!='city' and @type!='country' and @type!='province']">
            <xsl:variable name="city_id" select="replace(location[last()]/address/addrLine/@corresp,'#','')"/>
            <xsl:variable name="region_id" select="replace(key('cities', $city_id)/location/region/@corresp,'#','')"></xsl:variable>

            
            <xsl:value-of select="$quote"/>
            <xsl:value-of select="@xml:id"/>
            <xsl:value-of select="$quote"/>
            <xsl:value-of select="$delim"/>
            <xsl:value-of select="$quote"/>  
            <xsl:value-of select="@type"/>
            <xsl:value-of select="$quote"/>
            <xsl:value-of select="$delim"/>
            <xsl:value-of select="$quote"/>
            <xsl:choose>
                <xsl:when test="key('cities', $city_id)/country">
                    
                    <xsl:value-of select="key('cities', $city_id)/country"/>
                </xsl:when>
                <xsl:when test="key('provinces', $region_id)/country">
                    <xsl:value-of select="key('provinces', $region_id)/country"/>
                </xsl:when>
            </xsl:choose>

             <xsl:value-of select="$quote"/>
            <xsl:value-of select="$delim"/>
            <xsl:value-of select="$quote"/>  
            <xsl:choose>
                <xsl:when test="key('cities', $city_id)">
                    <xsl:value-of select="key('provinces', $region_id)/placeName"/>
                </xsl:when>
                
                <xsl:when test="key('provinces', $city_id)">
                    <xsl:value-of select="key('provinces', $city_id)/placeName"/>
                    
                </xsl:when>
            </xsl:choose>
            <xsl:value-of select="$quote"/>
            <xsl:value-of select="$delim"/>
            <xsl:value-of select="$quote"/>  
            <xsl:value-of select="key('cities', $city_id)/placeName"/>
            <xsl:value-of select="$quote"/>
            <xsl:value-of select="$delim"/>
            <xsl:value-of select="$quote"/> 
            <xsl:value-of select="placeName"/>
            <xsl:value-of select="$quote"/>        
            <xsl:value-of select="$newline"/>
        </xsl:template>
</xsl:stylesheet>