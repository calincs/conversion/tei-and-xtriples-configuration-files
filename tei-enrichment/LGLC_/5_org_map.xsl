<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:schema="https://schema.org/"
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:map="http://www.w3.org/2005/xpath-functions/map"
                version="3.0">
   <xsl:variable name="viaf_org_ref" as="map(xs:string, xs:string)">
      <xsl:map>
         <xsl:map-entry key="'DBLD'" select="'https://viaf.org/viaf/294994700/rdf.xml'"/>
         <xsl:map-entry key="'GAL'" select="'https://viaf.org/viaf/151906836/rdf.xml'"/>
         <xsl:map-entry key="'GI'" select="'https://viaf.org/viaf/151156491/rdf.xml'"/>
         <xsl:map-entry key="'HBC'" select="'https://viaf.org/viaf/139169065/rdf.xml'"/>
         <xsl:map-entry key="'HPL'" select="'https://viaf.org/viaf/153551580/rdf.xml'"/>
         <xsl:map-entry key="'JHS'" select="'https://viaf.org/viaf/127364339/rdf.xml'"/>
         <xsl:map-entry key="'KOFC'" select="'https://viaf.org/viaf/154098594/rdf.xml'"/>
         <xsl:map-entry key="'LIB'" select="'https://viaf.org/viaf/150041309/rdf.xml'"/>
         <xsl:map-entry key="'NDP'" select="'https://viaf.org/viaf/135767595/rdf.xml'"/>
         <xsl:map-entry key="'NUS'" select="'https://viaf.org/viaf/143582797/rdf.xml'"/>
         <xsl:map-entry key="'NYPD'" select="'https://viaf.org/viaf/152377154/rdf.xml'"/>
         <xsl:map-entry key="'PQ'" select="'https://viaf.org/viaf/151188225/rdf.xml'"/>
         <xsl:map-entry key="'UCC'" select="'https://viaf.org/viaf/148348068/rdf.xml'"/>
         <xsl:map-entry key="'WAVAW'" select="'https://viaf.org/viaf/266307300/rdf.xml'"/>
         <xsl:map-entry key="'APSAA'" select="'https://viaf.org/viaf/134820550/rdf.xml'"/>
         <xsl:map-entry key="'APA'" select="'https://viaf.org/viaf/150684657/rdf.xml'"/>
         <xsl:map-entry key="'APA2'" select="'https://viaf.org/viaf/153428680/rdf.xml'"/>
         <xsl:map-entry key="'AMA'" select="'https://viaf.org/viaf/139178561/rdf.xml'"/>
         <xsl:map-entry key="'RSM'" select="'https://viaf.org/viaf/156773049/rdf.xml'"/>
         <xsl:map-entry key="'SI'" select="'https://viaf.org/viaf/140174359/rdf.xml'"/>
         <xsl:map-entry key="'ZSL'" select="'https://viaf.org/viaf/126477741/rdf.xml'"/>
         <xsl:map-entry key="'SLP'" select="'https://viaf.org/viaf/130440504/rdf.xml'"/>
         <xsl:map-entry key="'SALVA'" select="'https://viaf.org/viaf/153030282/rdf.xml'"/>
         <xsl:map-entry key="'CANUCKS'" select="'https://viaf.org/viaf/262331821/rdf.xml'"/>
         <xsl:map-entry key="'PEN'" select="'https://viaf.org/viaf/167140998/rdf.xml'"/>
         <xsl:map-entry key="'PENC'" select="'https://viaf.org/viaf/172689641/rdf.xml'"/>
         <xsl:map-entry key="'GPCE'" select="'https://viaf.org/viaf/123227460/rdf.xml'"/>
         <xsl:map-entry key="'CLA'" select="'https://viaf.org/viaf/154275268/rdf.xml'"/>
         <xsl:map-entry key="'SUNM'" select="'https://viaf.org/viaf/316967877/rdf.xml'"/>
         <xsl:map-entry key="'UPCUSA'" select="'https://viaf.org/viaf/129006771/rdf.xml'"/>
         <xsl:map-entry key="'LIONS'" select="'https://viaf.org/viaf/157838476/rdf.xml'"/>
         <xsl:map-entry key="'MACMC'" select="'https://viaf.org/viaf/235131386/rdf.xml'"/>
         <xsl:map-entry key="'UTP'" select="'https://viaf.org/viaf/140153241/rdf.xml'"/>
         <xsl:map-entry key="'FLAM'" select="'https://viaf.org/viaf/154542374/rdf.xml'"/>
         <xsl:map-entry key="'TBW'" select="'https://viaf.org/viaf/139475107/rdf.xml'"/>
      </xsl:map>
   </xsl:variable>
   <xsl:variable name="wikidata_org_ref" as="map(xs:string, xs:string)">
      <xsl:map>
         <xsl:map-entry key="'CITV'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1532871.rdf'"/>
         <xsl:map-entry key="'CPC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2160378.rdf'"/>
         <xsl:map-entry key="'CTV'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q30308101.rdf'"/>
         <xsl:map-entry key="'DBLD'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q485774.rdf'"/>
         <xsl:map-entry key="'DS'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5297803.rdf'"/>
         <xsl:map-entry key="'GAL'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q79296927.rdf'"/>
         <xsl:map-entry key="'GA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q33121932.rdf'"/>
         <xsl:map-entry key="'GBC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q30295427.rdf'"/>
         <xsl:map-entry key="'GI'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q283498.rdf'"/>
         <xsl:map-entry key="'GOC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q422404.rdf'"/>
         <xsl:map-entry key="'GOO'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5589283.rdf'"/>
         <xsl:map-entry key="'HBC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q76039.rdf'"/>
         <xsl:map-entry key="'HPL'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q17850655.rdf'"/>
         <xsl:map-entry key="'JHS'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q6240084.rdf'"/>
         <xsl:map-entry key="'KOFC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1046799.rdf'"/>
         <xsl:map-entry key="'LIB'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q138345.rdf'"/>
         <xsl:map-entry key="'MONC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2994131.rdf'"/>
         <xsl:map-entry key="'NDP'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q129634.rdf'"/>
         <xsl:map-entry key="'NUS'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q6979169.rdf'"/>
         <xsl:map-entry key="'NYPD'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q328473.rdf'"/>
         <xsl:map-entry key="'PQ'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q950356.rdf'"/>
         <xsl:map-entry key="'SFBOS'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3658756.rdf'"/>
         <xsl:map-entry key="'SGOV'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q30295437.rdf'"/>
         <xsl:map-entry key="'SOF'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q170208.rdf'"/>
         <xsl:map-entry key="'UCC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q198745.rdf'"/>
         <xsl:map-entry key="'WAVAW'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q16951450.rdf'"/>
         <xsl:map-entry key="'APSAA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2843014.rdf'"/>
         <xsl:map-entry key="'VCBB'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q7914130.rdf'"/>
         <xsl:map-entry key="'APA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q466524.rdf'"/>
         <xsl:map-entry key="'APA2'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q466587.rdf'"/>
         <xsl:map-entry key="'AMA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q465697.rdf'"/>
         <xsl:map-entry key="'RSM'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3446034.rdf'"/>
         <xsl:map-entry key="'SI'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q159454.rdf'"/>
         <xsl:map-entry key="'ZSL'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q220198.rdf'"/>
         <xsl:map-entry key="'SLP'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3179541.rdf'"/>
         <xsl:map-entry key="'SALVA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q188307.rdf'"/>
         <xsl:map-entry key="'CANUCKS'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q192890.rdf'"/>
         <xsl:map-entry key="'PEN'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q206811.rdf'"/>
         <xsl:map-entry key="'PENC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q7118977.rdf'"/>
         <xsl:map-entry key="'GPCE'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q81307.rdf'"/>
         <xsl:map-entry key="'CLA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5030288.rdf'"/>
         <xsl:map-entry key="'SUNM'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3503752.rdf'"/>
         <xsl:map-entry key="'UPCUSA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q7889068.rdf'"/>
         <xsl:map-entry key="'LIONS'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q270029.rdf'"/>
         <xsl:map-entry key="'MACMC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q6724744.rdf'"/>
         <xsl:map-entry key="'UTP'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q4005866.rdf'"/>
         <xsl:map-entry key="'FLAM'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3117417.rdf'"/>
         <xsl:map-entry key="'PINKT'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q29561313.rdf'"/>
         <xsl:map-entry key="'TBW'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q7715813.rdf'"/>
         <xsl:map-entry key="'NOF'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3342407.rdf'"/>
      </xsl:map>
   </xsl:variable>
   <xsl:variable name="isni_org_ref" as="map(xs:string, xs:string)">
      <xsl:map>
         <xsl:map-entry key="'GAL'" select="'http://www.isni.org/0000000404616259.xml'"/>
         <xsl:map-entry key="'GA'" select="'http://www.isni.org/0000000404595283.xml'"/>
         <xsl:map-entry key="'GBC'" select="'http://www.isni.org/0000000406351100.xml'"/>
         <xsl:map-entry key="'GI'" select="'http://www.isni.org/0000000123535129.xml'"/>
         <xsl:map-entry key="'GOC'" select="'http://www.isni.org/0000000403771994.xml'"/>
         <xsl:map-entry key="'GOO'" select="'http://www.isni.org/0000000404044473.xml'"/>
         <xsl:map-entry key="'HBC'" select="'http://www.isni.org/0000000094550409.xml'"/>
         <xsl:map-entry key="'JHS'" select="'http://www.isni.org/0000000106586047.xml'"/>
         <xsl:map-entry key="'KOFC'" select="'http://www.isni.org/000000011018354X.xml'"/>
         <xsl:map-entry key="'LIB'" select="'http://www.isni.org/0000000115511290.xml'"/>
         <xsl:map-entry key="'NUS'" select="'http://www.isni.org/0000000121742917.xml'"/>
         <xsl:map-entry key="'NYPD'" select="'http://www.isni.org/0000000121871313.xml'"/>
         <xsl:map-entry key="'PQ'" select="'http://www.isni.org/0000000106724876.xml'"/>
         <xsl:map-entry key="'SGOV'" select="'http://www.isni.org/0000000404042339.xml'"/>
         <xsl:map-entry key="'UCC'" select="'http://www.isni.org/0000000406312558.xml'"/>
         <xsl:map-entry key="'APSAA'" select="'http://www.isni.org/0000000120190743.xml'"/>
         <xsl:map-entry key="'APA'" select="'http://www.isni.org/0000000121844411.xml'"/>
         <xsl:map-entry key="'APA2'" select="'http://www.isni.org/0000000121667523.xml'"/>
         <xsl:map-entry key="'AMA'" select="'http://www.isni.org/000000044647675X.xml'"/>
         <xsl:map-entry key="'RSM'" select="'http://www.isni.org/000000040427632X.xml'"/>
         <xsl:map-entry key="'SI'" select="'http://www.isni.org/000000012314999X.xml'"/>
         <xsl:map-entry key="'ZSL'" select="'http://www.isni.org/0000000122427273.xml'"/>
         <xsl:map-entry key="'SALVA'" select="'http://www.isni.org/0000000123695800.xml'"/>
         <xsl:map-entry key="'PEN'" select="'http://www.isni.org/0000000114225452.xml'"/>
         <xsl:map-entry key="'GPCE'" select="'http://www.isni.org/0000000110868215.xml'"/>
         <xsl:map-entry key="'CLA'" select="'http://www.isni.org/0000000404993089.xml'"/>
         <xsl:map-entry key="'UPCUSA'" select="'http://www.isni.org/0000000115037728.xml'"/>
         <xsl:map-entry key="'LIONS'" select="'http://www.isni.org/0000000107908714.xml'"/>
         <xsl:map-entry key="'UTP'" select="'http://www.isni.org/0000000122917264.xml'"/>
      </xsl:map>
   </xsl:variable>
</xsl:stylesheet>
