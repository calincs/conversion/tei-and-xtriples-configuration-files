<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:schema="https://schema.org/"
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:map="http://www.w3.org/2005/xpath-functions/map"
                version="3.0">
   <xsl:variable name="viaf_place2_ref" as="map(xs:string, xs:string)">
      <xsl:map>
         <xsl:map-entry key="'SWI'"
                        select="'https://viaf.org/viaf/35147662949560550901/rdf.xml'"/>
         <xsl:map-entry key="'UD'" select="'https://viaf.org/viaf/132685214/rdf.xml'"/>
         <xsl:map-entry key="'GDB'"
                        select="'https://viaf.org/viaf/2122154801938256310001/rdf.xml'"/>
         <xsl:map-entry key="'GDBB'"
                        select="'https://viaf.org/viaf/2122154801938256310001/rdf.xml'"/>
         <xsl:map-entry key="'GDB_B'"
                        select="'https://viaf.org/viaf/2122154801938256310001/rdf.xml'"/>
         <xsl:map-entry key="'CEAC'" select="'https://viaf.org/viaf/130476278/rdf.xml'"/>
         <xsl:map-entry key="'CHUMC'" select="'https://viaf.org/viaf/144718439/rdf.xml'"/>
         <xsl:map-entry key="'NCCT'" select="'https://viaf.org/viaf/261192332/rdf.xml'"/>
         <xsl:map-entry key="'STLH'" select="'https://viaf.org/viaf/137136174/rdf.xml'"/>
         <xsl:map-entry key="'MCH'" select="'https://viaf.org/viaf/311360001/rdf.xml'"/>
         <xsl:map-entry key="'PH'" select="'https://viaf.org/viaf/123347051/rdf.xml'"/>
         <xsl:map-entry key="'TCH'"
                        select="'https://viaf.org/viaf/169144782736153805440/rdf.xml'"/>
         <xsl:map-entry key="'AGO'" select="'https://viaf.org/viaf/151879312/rdf.xml'"/>
         <xsl:map-entry key="'CAG'" select="'https://viaf.org/viaf/124263197/rdf.xml'"/>
         <xsl:map-entry key="'CMCP'" select="'https://viaf.org/viaf/145029088/rdf.xml'"/>
         <xsl:map-entry key="'MRO'" select="'https://viaf.org/viaf/315530127/rdf.xml'"/>
         <xsl:map-entry key="'PLF'" select="'https://viaf.org/viaf/316998791/rdf.xml'"/>
         <xsl:map-entry key="'LP'"
                        select="'https://viaf.org/viaf/3127152080591507230000/rdf.xml'"/>
         <xsl:map-entry key="'NIT'" select="'https://viaf.org/viaf/265481334/rdf.xml'"/>
         <xsl:map-entry key="'RAT'" select="'https://viaf.org/viaf/145343594/rdf.xml'"/>
         <xsl:map-entry key="'TNM'" select="'https://viaf.org/viaf/134800992/rdf.xml'"/>
         <xsl:map-entry key="'TSD'" select="'https://viaf.org/viaf/317273950/rdf.xml'"/>
         <xsl:map-entry key="'MLG'" select="'https://viaf.org/viaf/123088135/rdf.xml'"/>
         <xsl:map-entry key="'BSUC'" select="'https://viaf.org/viaf/156186620/rdf.xml'"/>
         <xsl:map-entry key="'SFCH'"
                        select="'https://viaf.org/viaf/228144782722594424375/rdf.xml'"/>
         <xsl:map-entry key="'TRSQ'" select="'https://viaf.org/viaf/315530001/rdf.xml'"/>
         <xsl:map-entry key="'PENT'"
                        select="'https://viaf.org/viaf/640144783176054589415/rdf.xml'"/>
         <xsl:map-entry key="'KIA'" select="'https://viaf.org/viaf/158285112/rdf.xml'"/>
         <xsl:map-entry key="'SCO'" select="'https://viaf.org/viaf/151544433/rdf.xml'"/>
         <xsl:map-entry key="'DISNEY'" select="'https://viaf.org/viaf/315526679/rdf.xml'"/>
         <xsl:map-entry key="'CCCA'" select="'https://viaf.org/viaf/128923020/rdf.xml'"/>
      </xsl:map>
   </xsl:variable>
   <xsl:variable name="wikidata_place2_ref" as="map(xs:string, xs:string)">
      <xsl:map>
         <xsl:map-entry key="'CP'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5094649.rdf'"/>
         <xsl:map-entry key="'CTA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5148385.rdf'"/>
         <xsl:map-entry key="'FORG'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q61890231.rdf'"/>
         <xsl:map-entry key="'PJ'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q67812001.rdf'"/>
         <xsl:map-entry key="'SWI'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1261362.rdf'"/>
         <xsl:map-entry key="'UD'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1426524.rdf'"/>
         <xsl:map-entry key="'GDB'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5566193.rdf'"/>
         <xsl:map-entry key="'GDBB'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5566193.rdf'"/>
         <xsl:map-entry key="'GDB_B'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5566193.rdf'"/>
         <xsl:map-entry key="'OWMB'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2915475.rdf'"/>
         <xsl:map-entry key="'TWB'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q14875455.rdf'"/>
         <xsl:map-entry key="'CC519'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q7711946.rdf'"/>
         <xsl:map-entry key="'CEAC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q82997492.rdf'"/>
         <xsl:map-entry key="'CHANC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q4231899.rdf'"/>
         <xsl:map-entry key="'CHUMC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1054216.rdf'"/>
         <xsl:map-entry key="'HFC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q59535442.rdf'"/>
         <xsl:map-entry key="'NCCT'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q61467172.rdf'"/>
         <xsl:map-entry key="'PDA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1326219.rdf'"/>
         <xsl:map-entry key="'ROBSQ'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q9293120.rdf'"/>
         <xsl:map-entry key="'STLH'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q7589484.rdf'"/>
         <xsl:map-entry key="'WCC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q8025739.rdf'"/>
         <xsl:map-entry key="'YEC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q43080776.rdf'"/>
         <xsl:map-entry key="'HRC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1514655.rdf'"/>
         <xsl:map-entry key="'MCCT'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3585486.rdf'"/>
         <xsl:map-entry key="'BCL'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3365509.rdf'"/>
         <xsl:map-entry key="'MCH'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1151208.rdf'"/>
         <xsl:map-entry key="'ML'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q15150769.rdf'"/>
         <xsl:map-entry key="'ABLEG'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q6560903.rdf'"/>
         <xsl:map-entry key="'SLEG'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q6596422.rdf'"/>
         <xsl:map-entry key="'OCH'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q439501.rdf'"/>
         <xsl:map-entry key="'PH'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1589289.rdf'"/>
         <xsl:map-entry key="'TCH'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1093941.rdf'"/>
         <xsl:map-entry key="'VCH'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q820763.rdf'"/>
         <xsl:map-entry key="'AGO'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q670250.rdf'"/>
         <xsl:map-entry key="'CAG'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5164961.rdf'"/>
         <xsl:map-entry key="'CMCP'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2355659.rdf'"/>
         <xsl:map-entry key="'TSUN'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q7826528.rdf'"/>
         <xsl:map-entry key="'CS'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q4365607.rdf'"/>
         <xsl:map-entry key="'DBP'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q65085516.rdf'"/>
         <xsl:map-entry key="'MINO'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q6869292.rdf'"/>
         <xsl:map-entry key="'MJHP'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3363862.rdf'"/>
         <xsl:map-entry key="'MRO'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q205036.rdf'"/>
         <xsl:map-entry key="'MTMLP'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3364081.rdf'"/>
         <xsl:map-entry key="'PLF'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3363848.rdf'"/>
         <xsl:map-entry key="'PNE'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q4179402.rdf'"/>
         <xsl:map-entry key="'RPA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3433506.rdf'"/>
         <xsl:map-entry key="'VSQ'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3494857.rdf'"/>
         <xsl:map-entry key="'SFP'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q7425197.rdf'"/>
         <xsl:map-entry key="'WWI'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q7969483.rdf'"/>
         <xsl:map-entry key="'BBHS'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q4862212.rdf'"/>
         <xsl:map-entry key="'TUPHS'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q7526306.rdf'"/>
         <xsl:map-entry key="'CIN'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q38370955.rdf'"/>
         <xsl:map-entry key="'CLMT'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q38271575.rdf'"/>
         <xsl:map-entry key="'LP'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q38271907.rdf'"/>
         <xsl:map-entry key="'NIT'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q7034089.rdf'"/>
         <xsl:map-entry key="'OY'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q26550610.rdf'"/>
         <xsl:map-entry key="'PCIN'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q7122345.rdf'"/>
         <xsl:map-entry key="'PAT'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q7228755.rdf'"/>
         <xsl:map-entry key="'PRT'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q38282916.rdf'"/>
         <xsl:map-entry key="'RAT'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3527553.rdf'"/>
         <xsl:map-entry key="'RTH'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q7385042.rdf'"/>
         <xsl:map-entry key="'TNM'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1628172.rdf'"/>
         <xsl:map-entry key="'TSD'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1115701.rdf'"/>
         <xsl:map-entry key="'UPTT'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q7899459.rdf'"/>
         <xsl:map-entry key="'MLG'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1814534.rdf'"/>
         <xsl:map-entry key="'BSUC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q4928405.rdf'"/>
         <xsl:map-entry key="'SFCH'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1093944.rdf'"/>
         <xsl:map-entry key="'ASG'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q28456908.rdf'"/>
         <xsl:map-entry key="'CMP'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5061417.rdf'"/>
         <xsl:map-entry key="'CBE'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5092280.rdf'"/>
         <xsl:map-entry key="'EDIN'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5339067.rdf'"/>
         <xsl:map-entry key="'TRSQ'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q129143.rdf'"/>
         <xsl:map-entry key="'OTTCH'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q19844866.rdf'"/>
         <xsl:map-entry key="'PENT'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q11208.rdf'"/>
         <xsl:map-entry key="'SHOS'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3481913.rdf'"/>
         <xsl:map-entry key="'CENP'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5059192.rdf'"/>
         <xsl:map-entry key="'CPSS'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5055242.rdf'"/>
         <xsl:map-entry key="'HPAR'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1617812.rdf'"/>
         <xsl:map-entry key="'PHW'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q7186161.rdf'"/>
         <xsl:map-entry key="'CHALL'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q42891608.rdf'"/>
         <xsl:map-entry key="'KIA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q8685.rdf'"/>
         <xsl:map-entry key="'TC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q38370932.rdf'"/>
         <xsl:map-entry key="'TQ'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2737049.rdf'"/>
         <xsl:map-entry key="'SCO'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1155740.rdf'"/>
         <xsl:map-entry key="'TATRL'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q7785594.rdf'"/>
         <xsl:map-entry key="'SP'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q4345268.rdf'"/>
         <xsl:map-entry key="'DISNEY'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q181185.rdf'"/>
         <xsl:map-entry key="'RAMP'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q7290163.rdf'"/>
         <xsl:map-entry key="'FVM'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5430835.rdf'"/>
         <xsl:map-entry key="'CNE'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q857399.rdf'"/>
         <xsl:map-entry key="'CCCA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1077975.rdf'"/>
         <xsl:map-entry key="'STMC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2942399.rdf'"/>
         <xsl:map-entry key="'CCH'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q16825253.rdf'"/>
         <xsl:map-entry key="'CNT'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q134883.rdf'"/>
         <xsl:map-entry key="'TH3'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q42544565.rdf'"/>
      </xsl:map>
   </xsl:variable>
   <xsl:variable name="isni_place2_ref" as="map(xs:string, xs:string)">
      <xsl:map>
         <xsl:map-entry key="'CHUMC'" select="'http://www.isni.org/0000 0001 0743 2111.xml'"/>
         <xsl:map-entry key="'AGO'" select="'http://www.isni.org/0000 0004 1936 9414.xml'"/>
         <xsl:map-entry key="'CAG'" select="'http://www.isni.org/0000 0001 1016 5675.xml'"/>
         <xsl:map-entry key="'CMCP'" select="'http://www.isni.org/0000 0001 2176 8316.xml'"/>
         <xsl:map-entry key="'RAT'" select="'http://www.isni.org/0000 0000 9809 2097.xml'"/>
         <xsl:map-entry key="'TNM'" select="'http://www.isni.org/0000 0001 2289 738X.xml'"/>
         <xsl:map-entry key="'SCO'" select="'http://www.isni.org/0000 0001 2097 5698.xml'"/>
      </xsl:map>
   </xsl:variable>
   <xsl:variable name="getty_place2_ref" as="map(xs:string, xs:string)">
      <xsl:map>
         <xsl:map-entry key="'AGO'" select="'http://vocab.getty.edu/ulan/500245617.rdf'"/>
      </xsl:map>
   </xsl:variable>
</xsl:stylesheet>
