<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:schema="https://schema.org/"
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:map="http://www.w3.org/2005/xpath-functions/map"
                version="3.0">
   <xsl:variable name="geoname_place_ref" as="map(xs:string, xs:string)">
      <xsl:map>
         <xsl:map-entry key="'AUS'" select="'https://sws.geonames.org/2077456/about.rdf'"/>
         <xsl:map-entry key="'AUST'" select="'https://sws.geonames.org/2782113/about.rdf'"/>
         <xsl:map-entry key="'BEL'" select="'https://sws.geonames.org/2802361/about.rdf'"/>
         <xsl:map-entry key="'BELA'" select="'https://sws.geonames.org/630336/about.rdf'"/>
         <xsl:map-entry key="'BRAZ'" select="'https://sws.geonames.org/3469034/about.rdf'"/>
         <xsl:map-entry key="'CAN'" select="'https://sws.geonames.org/6251999/about.rdf'"/>
         <xsl:map-entry key="'CHN'" select="'https://sws.geonames.org/1814991/about.rdf'"/>
         <xsl:map-entry key="'CUBA'" select="'https://sws.geonames.org/3562981/about.rdf'"/>
         <xsl:map-entry key="'DEN'" select="'https://sws.geonames.org/2623032/about.rdf'"/>
         <xsl:map-entry key="'ENG'" select="'https://sws.geonames.org/6269131/about.rdf'"/>
         <xsl:map-entry key="'EST'" select="'https://sws.geonames.org/453733/about.rdf'"/>
         <xsl:map-entry key="'FIN'" select="'https://sws.geonames.org/660013/about.rdf'"/>
         <xsl:map-entry key="'FRA'" select="'https://sws.geonames.org/3017382/about.rdf'"/>
         <xsl:map-entry key="'GER'" select="'https://sws.geonames.org/2921044/about.rdf'"/>
         <xsl:map-entry key="'GR'" select="'https://sws.geonames.org/390903/about.rdf'"/>
         <xsl:map-entry key="'HTI'" select="'https://sws.geonames.org/3723988/about.rdf'"/>
         <xsl:map-entry key="'HUN'" select="'https://sws.geonames.org/719819/about.rdf'"/>
         <xsl:map-entry key="'INDI'" select="'https://sws.geonames.org/1269750/about.rdf'"/>
         <xsl:map-entry key="'INDO'" select="'https://sws.geonames.org/1643084/about.rdf'"/>
         <xsl:map-entry key="'ITA'" select="'https://sws.geonames.org/3175395/about.rdf'"/>
         <xsl:map-entry key="'IRE'" select="'https://sws.geonames.org/2963597/about.rdf'"/>
         <xsl:map-entry key="'JAM'" select="'https://sws.geonames.org/3489940/about.rdf'"/>
         <xsl:map-entry key="'JAP'" select="'https://sws.geonames.org/1861060/about.rdf'"/>
         <xsl:map-entry key="'MOROC'" select="'https://sws.geonames.org/2542007/about.rdf'"/>
         <xsl:map-entry key="'MYS'" select="'https://sws.geonames.org/1733045/about.rdf'"/>
         <xsl:map-entry key="'NIRE'" select="'https://sws.geonames.org/2641364/about.rdf'"/>
         <xsl:map-entry key="'PALE'" select="'https://sws.geonames.org/6254930/about.rdf'"/>
         <xsl:map-entry key="'POL'" select="'https://sws.geonames.org/798544/about.rdf'"/>
         <xsl:map-entry key="'SCOT'" select="'https://sws.geonames.org/2638360/about.rdf'"/>
         <xsl:map-entry key="'SWE'" select="'https://sws.geonames.org/2661886/about.rdf'"/>
         <xsl:map-entry key="'UK'" select="'https://sws.geonames.org/2635167/about.rdf'"/>
         <xsl:map-entry key="'USA'" select="'https://sws.geonames.org/6252001/about.rdf'"/>
         <xsl:map-entry key="'WALES'" select="'https://sws.geonames.org/2634895/about.rdf'"/>
         <xsl:map-entry key="'AB'" select="'https://sws.geonames.org/5883102/about.rdf'"/>
         <xsl:map-entry key="'BC'" select="'https://sws.geonames.org/5991097/about.rdf'"/>
         <xsl:map-entry key="'MB'" select="'https://sws.geonames.org/6065171/about.rdf'"/>
         <xsl:map-entry key="'NB'" select="'https://sws.geonames.org/6087430/about.rdf'"/>
         <xsl:map-entry key="'NL'" select="'https://sws.geonames.org/6354959/about.rdf'"/>
         <xsl:map-entry key="'NS'" select="'https://sws.geonames.org/6091530/about.rdf'"/>
         <xsl:map-entry key="'NT'" select="'https://sws.geonames.org/6091069/about.rdf'"/>
         <xsl:map-entry key="'NU'" select="'https://sws.geonames.org/6091732/about.rdf'"/>
         <xsl:map-entry key="'ON'" select="'https://sws.geonames.org/6093943/about.rdf'"/>
         <xsl:map-entry key="'PE'" select="'https://sws.geonames.org/6113358/about.rdf'"/>
         <xsl:map-entry key="'SK'" select="'https://sws.geonames.org/6141242/about.rdf'"/>
         <xsl:map-entry key="'YT'" select="'https://sws.geonames.org/6185811/about.rdf'"/>
         <xsl:map-entry key="'AL'" select="'https://sws.geonames.org/5106870/about.rdf'"/>
         <xsl:map-entry key="'AK'" select="'https://sws.geonames.org/5879092/about.rdf'"/>
         <xsl:map-entry key="'AZ'" select="'https://sws.geonames.org/5551752/about.rdf'"/>
         <xsl:map-entry key="'AR'" select="'https://sws.geonames.org/4099753/about.rdf'"/>
         <xsl:map-entry key="'CA'" select="'https://sws.geonames.org/5332921/about.rdf'"/>
         <xsl:map-entry key="'CT'" select="'https://sws.geonames.org/4831725/about.rdf'"/>
         <xsl:map-entry key="'DE'" select="'https://sws.geonames.org/4142224/about.rdf'"/>
         <xsl:map-entry key="'FL'" select="'https://sws.geonames.org/4155751/about.rdf'"/>
         <xsl:map-entry key="'GA'" select="'https://sws.geonames.org/4197000/about.rdf'"/>
         <xsl:map-entry key="'HI'" select="'https://sws.geonames.org/5855797/about.rdf'"/>
         <xsl:map-entry key="'ID'" select="'https://sws.geonames.org/5596512/about.rdf'"/>
         <xsl:map-entry key="'IL'" select="'https://sws.geonames.org/4896861/about.rdf'"/>
         <xsl:map-entry key="'IN'" select="'https://sws.geonames.org/4921868/about.rdf'"/>
         <xsl:map-entry key="'IA'" select="'https://sws.geonames.org/4328676/about.rdf'"/>
         <xsl:map-entry key="'KS'" select="'https://sws.geonames.org/4273857/about.rdf'"/>
         <xsl:map-entry key="'KY'" select="'https://sws.geonames.org/2161599/about.rdf'"/>
         <xsl:map-entry key="'LA'" select="'https://sws.geonames.org/4331987/about.rdf'"/>
         <xsl:map-entry key="'ME'" select="'https://sws.geonames.org/4971068/about.rdf'"/>
         <xsl:map-entry key="'MD'" select="'https://sws.geonames.org/4361885/about.rdf'"/>
         <xsl:map-entry key="'MA'" select="'https://sws.geonames.org/6254926/about.rdf'"/>
         <xsl:map-entry key="'MI'" select="'https://sws.geonames.org/5060420/about.rdf'"/>
         <xsl:map-entry key="'MN'" select="'https://sws.geonames.org/5037779/about.rdf'"/>
         <xsl:map-entry key="'MS'" select="'https://sws.geonames.org/4436296/about.rdf'"/>
         <xsl:map-entry key="'MO'" select="'https://sws.geonames.org/6698515/about.rdf'"/>
         <xsl:map-entry key="'MT'" select="'https://sws.geonames.org/5667009/about.rdf'"/>
         <xsl:map-entry key="'NE'" select="'https://sws.geonames.org/5073708/about.rdf'"/>
         <xsl:map-entry key="'NV'" select="'https://sws.geonames.org/5509151/about.rdf'"/>
         <xsl:map-entry key="'NH'" select="'https://sws.geonames.org/5164292/about.rdf'"/>
         <xsl:map-entry key="'NJ'" select="'https://sws.geonames.org/5101760/about.rdf'"/>
         <xsl:map-entry key="'NM'" select="'https://sws.geonames.org/5481136/about.rdf'"/>
         <xsl:map-entry key="'NY'" select="'https://sws.geonames.org/5128638/about.rdf'"/>
         <xsl:map-entry key="'NC'" select="'https://sws.geonames.org/4482348/about.rdf'"/>
         <xsl:map-entry key="'ND'" select="'https://sws.geonames.org/5690763/about.rdf'"/>
         <xsl:map-entry key="'OH'" select="'https://sws.geonames.org/5129633/about.rdf'"/>
         <xsl:map-entry key="'OK'" select="'https://sws.geonames.org/5204366/about.rdf'"/>
         <xsl:map-entry key="'OR'" select="'https://sws.geonames.org/5744337/about.rdf'"/>
         <xsl:map-entry key="'PA'" select="'https://sws.geonames.org/6254927/about.rdf'"/>
         <xsl:map-entry key="'RI'" select="'https://sws.geonames.org/5224323/about.rdf'"/>
         <xsl:map-entry key="'SC'" select="'https://sws.geonames.org/4597040/about.rdf'"/>
         <xsl:map-entry key="'TN'" select="'https://sws.geonames.org/4662168/about.rdf'"/>
         <xsl:map-entry key="'TX'" select="'https://sws.geonames.org/4736286/about.rdf'"/>
         <xsl:map-entry key="'UT'" select="'https://sws.geonames.org/5549030/about.rdf'"/>
         <xsl:map-entry key="'VT'" select="'https://sws.geonames.org/5277125/about.rdf'"/>
         <xsl:map-entry key="'VA'" select="'https://sws.geonames.org/6254928/about.rdf'"/>
         <xsl:map-entry key="'WA'" select="'https://sws.geonames.org/5815135/about.rdf'"/>
         <xsl:map-entry key="'WV'" select="'https://sws.geonames.org/4826850/about.rdf'"/>
         <xsl:map-entry key="'WI'" select="'https://sws.geonames.org/5279468/about.rdf'"/>
         <xsl:map-entry key="'WY'" select="'https://sws.geonames.org/5843591/about.rdf'"/>
         <xsl:map-entry key="'DC'" select="'https://sws.geonames.org/4138106/about.rdf'"/>
         <xsl:map-entry key="'ABB'" select="'https://sws.geonames.org/5881791/about.rdf'"/>
         <xsl:map-entry key="'AJAX'" select="'https://sws.geonames.org/5882873/about.rdf'"/>
         <xsl:map-entry key="'ALLAN'" select="'https://sws.geonames.org/5883752/about.rdf'"/>
         <xsl:map-entry key="'AMH'" select="'https://sws.geonames.org/5884467/about.rdf'"/>
         <xsl:map-entry key="'AMOS'" select="'https://sws.geonames.org/5884588/about.rdf'"/>
         <xsl:map-entry key="'ANNA'" select="'https://sws.geonames.org/5885421/about.rdf'"/>
         <xsl:map-entry key="'ANTIG'" select="'https://sws.geonames.org/5886182/about.rdf'"/>
         <xsl:map-entry key="'ARG'" select="'https://sws.geonames.org/5886823/about.rdf'"/>
         <xsl:map-entry key="'AURO'" select="'https://sws.geonames.org/5888377/about.rdf'"/>
         <xsl:map-entry key="'BAIEC'" select="'https://sws.geonames.org/5889745/about.rdf'"/>
         <xsl:map-entry key="'BARR'" select="'https://sws.geonames.org/5894171/about.rdf'"/>
         <xsl:map-entry key="'BATHU'" select="'https://sws.geonames.org/6696259/about.rdf'"/>
         <xsl:map-entry key="'BOL'" select="'https://sws.geonames.org/5905308/about.rdf'"/>
         <xsl:map-entry key="'BOUV'" select="'https://sws.geonames.org/5906267/about.rdf'"/>
         <xsl:map-entry key="'BOWM'" select="'https://sws.geonames.org/5906831/about.rdf'"/>
         <xsl:map-entry key="'BR'" select="'https://sws.geonames.org/5907364/about.rdf'"/>
         <xsl:map-entry key="'BRA'" select="'https://sws.geonames.org/5907990/about.rdf'"/>
         <xsl:map-entry key="'BRAM'" select="'https://sws.geonames.org/5907896/about.rdf'"/>
         <xsl:map-entry key="'BROV'" select="'https://sws.geonames.org/5909294/about.rdf'"/>
         <xsl:map-entry key="'BUR'" select="'https://sws.geonames.org/5911606/about.rdf'"/>
         <xsl:map-entry key="'BURL'" select="'https://sws.geonames.org/5911592/about.rdf'"/>
         <xsl:map-entry key="'CAL'" select="'https://sws.geonames.org/5913490/about.rdf'"/>
         <xsl:map-entry key="'CAMPB'" select="'https://sws.geonames.org/6696258/about.rdf'"/>
         <xsl:map-entry key="'CAMR'" select="'https://sws.geonames.org/5914653/about.rdf'"/>
         <xsl:map-entry key="'CARD'" select="'https://sws.geonames.org/5916821/about.rdf'"/>
         <xsl:map-entry key="'CB'" select="'https://sws.geonames.org/5927969/about.rdf'"/>
         <xsl:map-entry key="'CF'" select="'https://sws.geonames.org/5914033/about.rdf'"/>
         <xsl:map-entry key="'CHA'" select="'https://sws.geonames.org/5920433/about.rdf'"/>
         <xsl:map-entry key="'CHAR'" select="'https://sws.geonames.org/5920288/about.rdf'"/>
         <xsl:map-entry key="'CHAT'" select="'https://sws.geonames.org/5920447/about.rdf'"/>
         <xsl:map-entry key="'CHES'" select="'https://sws.geonames.org/5921028/about.rdf'"/>
         <xsl:map-entry key="'CHIC'" select="'https://sws.geonames.org/5921225/about.rdf'"/>
         <xsl:map-entry key="'CK'" select="'https://sws.geonames.org/5920450/about.rdf'"/>
         <xsl:map-entry key="'COB'" select="'https://sws.geonames.org/5924579/about.rdf'"/>
         <xsl:map-entry key="'COLL'" select="'https://sws.geonames.org/5925975/about.rdf'"/>
         <xsl:map-entry key="'COOKL'" select="'https://sws.geonames.org/5927141/about.rdf'"/>
         <xsl:map-entry key="'COW'" select="'https://sws.geonames.org/5931074/about.rdf'"/>
         <xsl:map-entry key="'CRAN'" select="'https://sws.geonames.org/5931800/about.rdf'"/>
         <xsl:map-entry key="'DANV'" select="'https://sws.geonames.org/5935042/about.rdf'"/>
         <xsl:map-entry key="'DART'" select="'https://sws.geonames.org/5935277/about.rdf'"/>
         <xsl:map-entry key="'DELT'" select="'https://sws.geonames.org/5937616/about.rdf'"/>
         <xsl:map-entry key="'DESCH'" select="'https://sws.geonames.org/8672718/about.rdf'"/>
         <xsl:map-entry key="'DM'" select="'https://sws.geonames.org/5941602/about.rdf'"/>
         <xsl:map-entry key="'DUND'" select="'https://sws.geonames.org/6948482/about.rdf'"/>
         <xsl:map-entry key="'EDM'" select="'https://sws.geonames.org/5946768/about.rdf'"/>
         <xsl:map-entry key="'EGAN'" select="'https://sws.geonames.org/5947035/about.rdf'"/>
         <xsl:map-entry key="'ERI'" select="'https://sws.geonames.org/5949247/about.rdf'"/>
         <xsl:map-entry key="'ETOB'" select="'https://sws.geonames.org/5950267/about.rdf'"/>
         <xsl:map-entry key="'FLE'" select="'https://sws.geonames.org/5954617/about.rdf'"/>
         <xsl:map-entry key="'FLOR'" select="'https://sws.geonames.org/8581594/about.rdf'"/>
         <xsl:map-entry key="'FRE'" select="'https://sws.geonames.org/5957776/about.rdf'"/>
         <xsl:map-entry key="'FE'" select="'https://sws.geonames.org/5955815/about.rdf'"/>
         <xsl:map-entry key="'FQU'" select="'https://sws.geonames.org/5955922/about.rdf'"/>
         <xsl:map-entry key="'GAL'" select="'https://sws.geonames.org/5959084/about.rdf'"/>
         <xsl:map-entry key="'GAT'" select="'https://sws.geonames.org/5959974/about.rdf'"/>
         <xsl:map-entry key="'GEO'" select="'https://sws.geonames.org/5960533/about.rdf'"/>
         <xsl:map-entry key="'GLACE'" select="'https://sws.geonames.org/5961564/about.rdf'"/>
         <xsl:map-entry key="'GMERE'" select="'https://sws.geonames.org/5964615/about.rdf'"/>
         <xsl:map-entry key="'GP'" select="'https://sws.geonames.org/5964347/about.rdf'"/>
         <xsl:map-entry key="'GRA'" select="'https://sws.geonames.org/6111855/about.rdf'"/>
         <xsl:map-entry key="'GRAN'" select="'https://sws.geonames.org/5964215/about.rdf'"/>
         <xsl:map-entry key="'GT'" select="'https://sws.geonames.org/5960533/about.rdf'"/>
         <xsl:map-entry key="'GUE'" select="'https://sws.geonames.org/5967629/about.rdf'"/>
         <xsl:map-entry key="'HAL'" select="'https://sws.geonames.org/5969423/about.rdf'"/>
         <xsl:map-entry key="'HAM'" select="'https://sws.geonames.org/5969782/about.rdf'"/>
         <xsl:map-entry key="'HAND'" select="'https://sws.geonames.org/5970088/about.rdf'"/>
         <xsl:map-entry key="'HAR'" select="'https://sws.geonames.org/5971462/about.rdf'"/>
         <xsl:map-entry key="'HAWK'" select="'https://sws.geonames.org/5972360/about.rdf'"/>
         <xsl:map-entry key="'HUDS'" select="'https://sws.geonames.org/5978126/about.rdf'"/>
         <xsl:map-entry key="'HUMB'" select="'https://sws.geonames.org/5978404/about.rdf'"/>
         <xsl:map-entry key="'INSI'" select="'https://sws.geonames.org/5983519/about.rdf'"/>
         <xsl:map-entry key="'JASP'" select="'https://sws.geonames.org/6354954/about.rdf'"/>
         <xsl:map-entry key="'KAM'" select="'https://sws.geonames.org/5989045/about.rdf'"/>
         <xsl:map-entry key="'KEL'" select="'https://sws.geonames.org/5990579/about.rdf'"/>
         <xsl:map-entry key="'KEN'" select="'https://sws.geonames.org/5991055/about.rdf'"/>
         <xsl:map-entry key="'KENTV'" select="'https://sws.geonames.org/5991148/about.rdf'"/>
         <xsl:map-entry key="'KIN'" select="'https://sws.geonames.org/5992500/about.rdf'"/>
         <xsl:map-entry key="'KINC'" select="'https://sws.geonames.org/5992144/about.rdf'"/>
         <xsl:map-entry key="'LAC'" select="'https://sws.geonames.org/6545041/about.rdf'"/>
         <xsl:map-entry key="'LAM'" select="'https://sws.geonames.org/6945986/about.rdf'"/>
         <xsl:map-entry key="'LAS'" select="'https://sws.geonames.org/6050177/about.rdf'"/>
         <xsl:map-entry key="'LAV'" select="'https://sws.geonames.org/6050610/about.rdf'"/>
         <xsl:map-entry key="'LENX'" select="'https://sws.geonames.org/6052325/about.rdf'"/>
         <xsl:map-entry key="'LETH'" select="'https://sws.geonames.org/6053154/about.rdf'"/>
         <xsl:map-entry key="'LIND'" select="'https://sws.geonames.org/6054350/about.rdf'"/>
         <xsl:map-entry key="'LON'" select="'https://sws.geonames.org/6058560/about.rdf'"/>
         <xsl:map-entry key="'LONG'" select="'https://sws.geonames.org/6059891/about.rdf'"/>
         <xsl:map-entry key="'MANI'" select="'https://sws.geonames.org/6065262/about.rdf'"/>
         <xsl:map-entry key="'MAR'" select="'https://sws.geonames.org/6066513/about.rdf'"/>
         <xsl:map-entry key="'MAYF'" select="'https://sws.geonames.org/6068320/about.rdf'"/>
         <xsl:map-entry key="'MEA'" select="'https://sws.geonames.org/6071421/about.rdf'"/>
         <xsl:map-entry key="'MEDH'" select="'https://sws.geonames.org/6071618/about.rdf'"/>
         <xsl:map-entry key="'MIL'" select="'https://sws.geonames.org/6074377/about.rdf'"/>
         <xsl:map-entry key="'MJAW'" select="'https://sws.geonames.org/6078112/about.rdf'"/>
         <xsl:map-entry key="'MIS'" select="'https://sws.geonames.org/6075357/about.rdf'"/>
         <xsl:map-entry key="'MONC'" select="'https://sws.geonames.org/6076211/about.rdf'"/>
         <xsl:map-entry key="'MONTJ'" select="'https://sws.geonames.org/6944113/about.rdf'"/>
         <xsl:map-entry key="'MONTL'" select="'https://sws.geonames.org/6077128/about.rdf'"/>
         <xsl:map-entry key="'MON'" select="'https://sws.geonames.org/6077243/about.rdf'"/>
         <xsl:map-entry key="'MONN'" select="'https://sws.geonames.org/6077264/about.rdf'"/>
         <xsl:map-entry key="'MULM'" select="'https://sws.geonames.org/6084118/about.rdf'"/>
         <xsl:map-entry key="'NANA'" select="'https://sws.geonames.org/6085772/about.rdf'"/>
         <xsl:map-entry key="'NBAT'" select="'https://sws.geonames.org/6089404/about.rdf'"/>
         <xsl:map-entry key="'NCAS'" select="'https://sws.geonames.org/6087456/about.rdf'"/>
         <xsl:map-entry key="'NEWM'" select="'https://sws.geonames.org/6087701/about.rdf'"/>
         <xsl:map-entry key="'NEWR'" select="'https://sws.geonames.org/6943826/about.rdf'"/>
         <xsl:map-entry key="'NF'" select="'https://sws.geonames.org/6087892/about.rdf'"/>
         <xsl:map-entry key="'NIP'" select="'https://sws.geonames.org/6088469/about.rdf'"/>
         <xsl:map-entry key="'NPN'" select="'https://sws.geonames.org/6087029/about.rdf'"/>
         <xsl:map-entry key="'NWM'" select="'https://sws.geonames.org/6087844/about.rdf'"/>
         <xsl:map-entry key="'NORTHY'" select="'https://sws.geonames.org/6091104/about.rdf'"/>
         <xsl:map-entry key="'NORV'" select="'https://sws.geonames.org/6091132/about.rdf'"/>
         <xsl:map-entry key="'OAK'" select="'https://sws.geonames.org/6092122/about.rdf'"/>
         <xsl:map-entry key="'ODES'" select="'https://sws.geonames.org/6092581/about.rdf'"/>
         <xsl:map-entry key="'ORI'" select="'https://sws.geonames.org/6094325/about.rdf'"/>
         <xsl:map-entry key="'OSHA'" select="'https://sws.geonames.org/6094578/about.rdf'"/>
         <xsl:map-entry key="'OTT'" select="'https://sws.geonames.org/6094817/about.rdf'"/>
         <xsl:map-entry key="'PALI'" select="'https://sws.geonames.org/6111640/about.rdf'"/>
         <xsl:map-entry key="'PASQ'" select="'https://sws.geonames.org/6098985/about.rdf'"/>
         <xsl:map-entry key="'PRA'" select="'https://sws.geonames.org/6113335/about.rdf'"/>
         <xsl:map-entry key="'PET'" select="'https://sws.geonames.org/6101645/about.rdf'"/>
         <xsl:map-entry key="'PICK'" select="'https://sws.geonames.org/6104111/about.rdf'"/>
         <xsl:map-entry key="'PICTC'" select="'https://sws.geonames.org/6104222/about.rdf'"/>
         <xsl:map-entry key="'PG'" select="'https://sws.geonames.org/6113365/about.rdf'"/>
         <xsl:map-entry key="'POHO'" select="'https://sws.geonames.org/6111882/about.rdf'"/>
         <xsl:map-entry key="'POI'" select="'https://sws.geonames.org/6107171/about.rdf'"/>
         <xsl:map-entry key="'PP'" select="'https://sws.geonames.org/6105173/about.rdf'"/>
         <xsl:map-entry key="'QUE'" select="'https://sws.geonames.org/6325494/about.rdf'"/>
         <xsl:map-entry key="'RD'" select="'https://sws.geonames.org/6118158/about.rdf'"/>
         <xsl:map-entry key="'REG'" select="'https://sws.geonames.org/6119109/about.rdf'"/>
         <xsl:map-entry key="'RIC'" select="'https://sws.geonames.org/6122091/about.rdf'"/>
         <xsl:map-entry key="'RICHM'" select="'https://sws.geonames.org/6122085/about.rdf'"/>
         <xsl:map-entry key="'RIMO'" select="'https://sws.geonames.org/6354895/about.rdf'"/>
         <xsl:map-entry key="'ROSET'" select="'https://sws.geonames.org/6127749/about.rdf'"/>
         <xsl:map-entry key="'ROU'" select="'https://sws.geonames.org/6128089/about.rdf'"/>
         <xsl:map-entry key="'RNOR'" select="'https://sws.geonames.org/6128577/about.rdf'"/>
         <xsl:map-entry key="'SACK'" select="'https://sws.geonames.org/6137063/about.rdf'"/>
         <xsl:map-entry key="'SAI'" select="'https://sws.geonames.org/6138610/about.rdf'"/>
         <xsl:map-entry key="'SAS'" select="'https://sws.geonames.org/6141256/about.rdf'"/>
         <xsl:map-entry key="'SCA'" select="'https://sws.geonames.org/6948711/about.rdf'"/>
         <xsl:map-entry key="'SECH'" select="'https://sws.geonames.org/6143367/about.rdf'"/>
         <xsl:map-entry key="'SGDB'" select="'https://sws.geonames.org/6138267/about.rdf'"/>
         <xsl:map-entry key="'SHEF'" select="'https://sws.geonames.org/6145844/about.rdf'"/>
         <xsl:map-entry key="'SHER'" select="'https://sws.geonames.org/6146143/about.rdf'"/>
         <xsl:map-entry key="'STEU'" select="'https://sws.geonames.org/6138175/about.rdf'"/>
         <xsl:map-entry key="'SHAW'" select="'https://sws.geonames.org/6145489/about.rdf'"/>
         <xsl:map-entry key="'SJ'" select="'https://sws.geonames.org/8672647/about.rdf'"/>
         <xsl:map-entry key="'SOO'" select="'https://sws.geonames.org/6151264/about.rdf'"/>
         <xsl:map-entry key="'SMEA'" select="'https://sws.geonames.org/6149823/about.rdf'"/>
         <xsl:map-entry key="'SSM'" select="'https://sws.geonames.org/6141439/about.rdf'"/>
         <xsl:map-entry key="'STC'" select="'https://sws.geonames.org/6155721/about.rdf'"/>
         <xsl:map-entry key="'STCAS'" select="'https://sws.geonames.org/6137560/about.rdf'"/>
         <xsl:map-entry key="'STCAM'" select="'https://sws.geonames.org/6137550/about.rdf'"/>
         <xsl:map-entry key="'STE'" select="'https://sws.geonames.org/6156102/about.rdf'"/>
         <xsl:map-entry key="'STI'" select="'https://sws.geonames.org/6156855/about.rdf'"/>
         <xsl:map-entry key="'STJ'" select="'https://sws.geonames.org/6324733/about.rdf'"/>
         <xsl:map-entry key="'STJE'" select="'https://sws.geonames.org/6138501/about.rdf'"/>
         <xsl:map-entry key="'STJO'" select="'https://sws.geonames.org/6138517/about.rdf'"/>
         <xsl:map-entry key="'STR'" select="'https://sws.geonames.org/6157977/about.rdf'"/>
         <xsl:map-entry key="'STTHO'" select="'https://sws.geonames.org/6158357/about.rdf'"/>
         <xsl:map-entry key="'SUD'" select="'https://sws.geonames.org/5964700/about.rdf'"/>
         <xsl:map-entry key="'SUMS'" select="'https://sws.geonames.org/6159244/about.rdf'"/>
         <xsl:map-entry key="'SYDN'" select="'https://sws.geonames.org/6354908/about.rdf'"/>
         <xsl:map-entry key="'THU'" select="'https://sws.geonames.org/6166142/about.rdf'"/>
         <xsl:map-entry key="'TIM'" select="'https://sws.geonames.org/6166739/about.rdf'"/>
         <xsl:map-entry key="'TOR'" select="'https://sws.geonames.org/6167865/about.rdf'"/>
         <xsl:map-entry key="'TR'" select="'https://sws.geonames.org/6169141/about.rdf'"/>
         <xsl:map-entry key="'TREH'" select="'https://sws.geonames.org/6168694/about.rdf'"/>
         <xsl:map-entry key="'TRUR'" select="'https://sws.geonames.org/6169587/about.rdf'"/>
         <xsl:map-entry key="'UPT'" select="'https://sws.geonames.org/6172751/about.rdf'"/>
         <xsl:map-entry key="'VAL'" select="'https://sws.geonames.org/6173017/about.rdf'"/>
         <xsl:map-entry key="'VAN'" select="'https://sws.geonames.org/6173331/about.rdf'"/>
         <xsl:map-entry key="'VD'" select="'https://sws.geonames.org/6173767/about.rdf'"/>
         <xsl:map-entry key="'VDO'" select="'https://sws.geonames.org/6173017/about.rdf'"/>
         <xsl:map-entry key="'VER'" select="'https://sws.geonames.org/6173864/about.rdf'"/>
         <xsl:map-entry key="'VIC'" select="'https://sws.geonames.org/6174041/about.rdf'"/>
         <xsl:map-entry key="'WELL'" select="'https://sws.geonames.org/6177869/about.rdf'"/>
         <xsl:map-entry key="'WEYB'" select="'https://sws.geonames.org/6179652/about.rdf'"/>
         <xsl:map-entry key="'WIN'" select="'https://sws.geonames.org/6183235/about.rdf'"/>
         <xsl:map-entry key="'WIND'" select="'https://sws.geonames.org/6182962/about.rdf'"/>
         <xsl:map-entry key="'WINK'" select="'https://sws.geonames.org/6183204/about.rdf'"/>
         <xsl:map-entry key="'WISH'" select="'https://sws.geonames.org/6183429/about.rdf'"/>
         <xsl:map-entry key="'WLOC'" select="'https://sws.geonames.org/6179138/about.rdf'"/>
         <xsl:map-entry key="'WOOD'" select="'https://sws.geonames.org/6184365/about.rdf'"/>
         <xsl:map-entry key="'WYN'" select="'https://sws.geonames.org/6185021/about.rdf'"/>
         <xsl:map-entry key="'ABER'" select="'https://sws.geonames.org/2657835/about.rdf'"/>
         <xsl:map-entry key="'ACNC'" select="'https://sws.geonames.org/4453028/about.rdf'"/>
         <xsl:map-entry key="'AMES'" select="'https://sws.geonames.org/4846834/about.rdf'"/>
         <xsl:map-entry key="'LEEP'" select="'https://sws.geonames.org/5071839/about.rdf'"/>
         <xsl:map-entry key="'NEWB'" select="'https://sws.geonames.org/5101717/about.rdf'"/>
         <xsl:map-entry key="'ASHEV'" select="'https://sws.geonames.org/4453066/about.rdf'"/>
         <xsl:map-entry key="'ALL'" select="'https://sws.geonames.org/5178040/about.rdf'"/>
         <xsl:map-entry key="'BARNS'" select="'https://sws.geonames.org/4529973/about.rdf'"/>
         <xsl:map-entry key="'BERK'" select="'https://sws.geonames.org/5327684/about.rdf'"/>
         <xsl:map-entry key="'BERL'" select="'https://sws.geonames.org/2950159/about.rdf'"/>
         <xsl:map-entry key="'BIN'" select="'https://sws.geonames.org/5109177/about.rdf'"/>
         <xsl:map-entry key="'BIRK'" select="'https://sws.geonames.org/2655613/about.rdf'"/>
         <xsl:map-entry key="'BLA'" select="'https://sws.geonames.org/7506763/about.rdf'"/>
         <xsl:map-entry key="'BLO'" select="'https://sws.geonames.org/4254679/about.rdf'"/>
         <xsl:map-entry key="'BLYN'" select="'https://sws.geonames.org/5110302/about.rdf'"/>
         <xsl:map-entry key="'BMHAM'" select="'https://sws.geonames.org/2655603/about.rdf'"/>
         <xsl:map-entry key="'BOS'" select="'https://sws.geonames.org/4930956/about.rdf'"/>
         <xsl:map-entry key="'BOU'" select="'https://sws.geonames.org/2655095/about.rdf'"/>
         <xsl:map-entry key="'BRI'" select="'https://sws.geonames.org/2654675/about.rdf'"/>
         <xsl:map-entry key="'BRIS'" select="'https://sws.geonames.org/2174003/about.rdf'"/>
         <xsl:map-entry key="'BRO'" select="'https://sws.geonames.org/4931429/about.rdf'"/>
         <xsl:map-entry key="'BROWN'" select="'https://sws.geonames.org/4676740/about.rdf'"/>
         <xsl:map-entry key="'BRX'" select="'https://sws.geonames.org/5110266/about.rdf'"/>
         <xsl:map-entry key="'BSE'" select="'https://sws.geonames.org/2654186/about.rdf'"/>
         <xsl:map-entry key="'BUFF'" select="'https://sws.geonames.org/5110629/about.rdf'"/>
         <xsl:map-entry key="'CAP'" select="'https://sws.geonames.org/3728474/about.rdf'"/>
         <xsl:map-entry key="'CAR'" select="'https://sws.geonames.org/2653822/about.rdf'"/>
         <xsl:map-entry key="'CARI'" select="'https://sws.geonames.org/4960140/about.rdf'"/>
         <xsl:map-entry key="'CASA'" select="'https://sws.geonames.org/2553604/about.rdf'"/>
         <xsl:map-entry key="'CHE'" select="'https://sws.geonames.org/4351264/about.rdf'"/>
         <xsl:map-entry key="'CHI'" select="'https://sws.geonames.org/4887398/about.rdf'"/>
         <xsl:map-entry key="'CHICK'" select="'https://sws.geonames.org/4533029/about.rdf'"/>
         <xsl:map-entry key="'CLE'" select="'https://sws.geonames.org/5150529/about.rdf'"/>
         <xsl:map-entry key="'CLEV'" select="'https://sws.geonames.org/2652861/about.rdf'"/>
         <xsl:map-entry key="'CLYD'" select="'https://sws.geonames.org/2652730/about.rdf'"/>
         <xsl:map-entry key="'COLB'" select="'https://sws.geonames.org/4753654/about.rdf'"/>
         <xsl:map-entry key="'COLU'" select="'https://sws.geonames.org/4509177/about.rdf'"/>
         <xsl:map-entry key="'COV'" select="'https://sws.geonames.org/2652221/about.rdf'"/>
         <xsl:map-entry key="'DALL'" select="'https://sws.geonames.org/4684888/about.rdf'"/>
         <xsl:map-entry key="'DONI'" select="'https://sws.geonames.org/2651119/about.rdf'"/>
         <xsl:map-entry key="'DORC'" select="'https://sws.geonames.org/2651101/about.rdf'"/>
         <xsl:map-entry key="'DOUNE'" select="'https://sws.geonames.org/2651060/about.rdf'"/>
         <xsl:map-entry key="'EC'" select="'https://sws.geonames.org/5097677/about.rdf'"/>
         <xsl:map-entry key="'EDI'" select="'https://sws.geonames.org/2650225/about.rdf'"/>
         <xsl:map-entry key="'ELM'" select="'https://sws.geonames.org/5116495/about.rdf'"/>
         <xsl:map-entry key="'EUG'" select="'https://sws.geonames.org/5725846/about.rdf'"/>
         <xsl:map-entry key="'EXET'" select="'https://sws.geonames.org/2649808/about.rdf'"/>
         <xsl:map-entry key="'FALK'" select="'https://sws.geonames.org/2649723/about.rdf'"/>
         <xsl:map-entry key="'FUKUO'" select="'https://sws.geonames.org/1863955/about.rdf'"/>
         <xsl:map-entry key="'GALES'" select="'https://sws.geonames.org/4893392/about.rdf'"/>
         <xsl:map-entry key="'GC'" select="'https://sws.geonames.org/6331364/about.rdf'"/>
         <xsl:map-entry key="'GLAS'" select="'https://sws.geonames.org/2648579/about.rdf'"/>
         <xsl:map-entry key="'GUIL'" select="'https://sws.geonames.org/2647793/about.rdf'"/>
         <xsl:map-entry key="'HAC'" select="'https://sws.geonames.org/5098706/about.rdf'"/>
         <xsl:map-entry key="'HAMP'" select="'https://sws.geonames.org/2647553/about.rdf'"/>
         <xsl:map-entry key="'HARN'" select="'https://sws.geonames.org/4469999/about.rdf'"/>
         <xsl:map-entry key="'HESP'" select="'https://sws.geonames.org/4995865/about.rdf'"/>
         <xsl:map-entry key="'HOU'" select="'https://sws.geonames.org/4699066/about.rdf'"/>
         <xsl:map-entry key="'HYDE'" select="'https://sws.geonames.org/1269843/about.rdf'"/>
         <xsl:map-entry key="'IND'" select="'https://sws.geonames.org/4259418/about.rdf'"/>
         <xsl:map-entry key="'INDE'" select="'https://sws.geonames.org/4391812/about.rdf'"/>
         <xsl:map-entry key="'JACK'" select="'https://sws.geonames.org/4160021/about.rdf'"/>
         <xsl:map-entry key="'KENN'" select="'https://sws.geonames.org/4558942/about.rdf'"/>
         <xsl:map-entry key="'KING'" select="'https://sws.geonames.org/3489854/about.rdf'"/>
         <xsl:map-entry key="'LASV'" select="'https://sws.geonames.org/5506956/about.rdf'"/>
         <xsl:map-entry key="'LODZ'" select="'https://sws.geonames.org/3093133/about.rdf'"/>
         <xsl:map-entry key="'LOLL'" select="'https://sws.geonames.org/6557780/about.rdf'"/>
         <xsl:map-entry key="'LONE'" select="'https://sws.geonames.org/2643743/about.rdf'"/>
         <xsl:map-entry key="'LOS'" select="'https://sws.geonames.org/5368361/about.rdf'"/>
         <xsl:map-entry key="'MALA'" select="'https://sws.geonames.org/1734759/about.rdf'"/>
         <xsl:map-entry key="'MANC'" select="'https://sws.geonames.org/2643123/about.rdf'"/>
         <xsl:map-entry key="'MANH'" select="'https://sws.geonames.org/5125766/about.rdf'"/>
         <xsl:map-entry key="'MET'" select="'https://sws.geonames.org/5101125/about.rdf'"/>
         <xsl:map-entry key="'MIA'" select="'https://sws.geonames.org/4164138/about.rdf'"/>
         <xsl:map-entry key="'MILW'" select="'https://sws.geonames.org/5263045/about.rdf'"/>
         <xsl:map-entry key="'MIN'" select="'https://sws.geonames.org/5037649/about.rdf'"/>
         <xsl:map-entry key="'MONR'" select="'https://sws.geonames.org/4333669/about.rdf'"/>
         <xsl:map-entry key="'NDEL'" select="'https://sws.geonames.org/1261481/about.rdf'"/>
         <xsl:map-entry key="'NEW'" select="'https://sws.geonames.org/5128581/about.rdf'"/>
         <xsl:map-entry key="'NEWA'" select="'https://sws.geonames.org/5101798/about.rdf'"/>
         <xsl:map-entry key="'PRINC'" select="'https://sws.geonames.org/5102922/about.rdf'"/>
         <xsl:map-entry key="'NEWC'" select="'https://sws.geonames.org/4262072/about.rdf'"/>
         <xsl:map-entry key="'NOORD'" select="'https://sws.geonames.org/2749810/about.rdf'"/>
         <xsl:map-entry key="'NORF'" select="'https://sws.geonames.org/4776222/about.rdf'"/>
         <xsl:map-entry key="'NP'" select="'https://sws.geonames.org/5697939/about.rdf'"/>
         <xsl:map-entry key="'OAKL'" select="'https://sws.geonames.org/5378538/about.rdf'"/>
         <xsl:map-entry key="'OKC'" select="'https://sws.geonames.org/4544349/about.rdf'"/>
         <xsl:map-entry key="'OWS'" select="'https://sws.geonames.org/6095645/about.rdf'"/>
         <xsl:map-entry key="'PALM'" select="'https://sws.geonames.org/5380668/about.rdf'"/>
         <xsl:map-entry key="'PAR'" select="'https://sws.geonames.org/2968815/about.rdf'"/>
         <xsl:map-entry key="'PARM'" select="'https://sws.geonames.org/6540120/about.rdf'"/>
         <xsl:map-entry key="'PASA'" select="'https://sws.geonames.org/5381396/about.rdf'"/>
         <xsl:map-entry key="'PATC'" select="'https://sws.geonames.org/5130672/about.rdf'"/>
         <xsl:map-entry key="'PIML'" select="'https://sws.geonames.org/6545249/about.rdf'"/>
         <xsl:map-entry key="'PIT'" select="'https://sws.geonames.org/4947459/about.rdf'"/>
         <xsl:map-entry key="'PITTS'" select="'https://sws.geonames.org/4277241/about.rdf'"/>
         <xsl:map-entry key="'PLA'" select="'https://sws.geonames.org/5102720/about.rdf'"/>
         <xsl:map-entry key="'POR'" select="'https://sws.geonames.org/5746545/about.rdf'"/>
         <xsl:map-entry key="'PORH'" select="'https://sws.geonames.org/5006233/about.rdf'"/>
         <xsl:map-entry key="'RICH'" select="'https://sws.geonames.org/2639388/about.rdf'"/>
         <xsl:map-entry key="'RIVS'" select="'https://sws.geonames.org/5387877/about.rdf'"/>
         <xsl:map-entry key="'ROCH'" select="'https://sws.geonames.org/5134086/about.rdf'"/>
         <xsl:map-entry key="'SA'" select="'https://sws.geonames.org/4726206/about.rdf'"/>
         <xsl:map-entry key="'SACR'" select="'https://sws.geonames.org/5389489/about.rdf'"/>
         <xsl:map-entry key="'SAOL'" select="'https://sws.geonames.org/6318534/about.rdf'"/>
         <xsl:map-entry key="'SAVA'" select="'https://sws.geonames.org/4221552/about.rdf'"/>
         <xsl:map-entry key="'SCHEN'" select="'https://sws.geonames.org/5136454/about.rdf'"/>
         <xsl:map-entry key="'SDI'" select="'https://sws.geonames.org/5391811/about.rdf'"/>
         <xsl:map-entry key="'SEA'" select="'https://sws.geonames.org/5809844/about.rdf'"/>
         <xsl:map-entry key="'SF'" select="'https://sws.geonames.org/5391959/about.rdf'"/>
         <xsl:map-entry key="'SMON'" select="'https://sws.geonames.org/5393212/about.rdf'"/>
         <xsl:map-entry key="'SS'" select="'https://sws.geonames.org/4369596/about.rdf'"/>
         <xsl:map-entry key="'SOLE'" select="'https://sws.geonames.org/5397018/about.rdf'"/>
         <xsl:map-entry key="'SWIN'" select="'https://sws.geonames.org/2636389/about.rdf'"/>
         <xsl:map-entry key="'SYD'" select="'https://sws.geonames.org/2147714/about.rdf'"/>
         <xsl:map-entry key="'TALL'" select="'https://sws.geonames.org/588409/about.rdf'"/>
         <xsl:map-entry key="'THOR'" select="'https://sws.geonames.org/6165719/about.rdf'"/>
         <xsl:map-entry key="'TOUL'" select="'https://sws.geonames.org/2972315/about.rdf'"/>
         <xsl:map-entry key="'TRENT'" select="'https://sws.geonames.org/4251413/about.rdf'"/>
         <xsl:map-entry key="'TRU'" select="'https://sws.geonames.org/5141508/about.rdf'"/>
         <xsl:map-entry key="'TULS'" select="'https://sws.geonames.org/4553433/about.rdf'"/>
         <xsl:map-entry key="'VERN'" select="'https://sws.geonames.org/6453910/about.rdf'"/>
         <xsl:map-entry key="'VICB'" select="'https://sws.geonames.org/4449620/about.rdf'"/>
         <xsl:map-entry key="'VIEN'" select="'https://sws.geonames.org/2761369/about.rdf'"/>
         <xsl:map-entry key="'WARD'" select="'https://sws.geonames.org/5611441/about.rdf'"/>
         <xsl:map-entry key="'WAS'" select="'https://sws.geonames.org/4140963/about.rdf'"/>
         <xsl:map-entry key="'WHOL'" select="'https://sws.geonames.org/5408076/about.rdf'"/>
         <xsl:map-entry key="'WOND'" select="'https://sws.geonames.org/2783456/about.rdf'"/>
         <xsl:map-entry key="'YOG'" select="'https://sws.geonames.org/1621177/about.rdf'"/>
         <xsl:map-entry key="'YOR'" select="'https://sws.geonames.org/5410902/about.rdf'"/>
         <xsl:map-entry key="'YORK'" select="'https://sws.geonames.org/4983661/about.rdf'"/>
         <xsl:map-entry key="'AUSTI'" select="'https://sws.geonames.org/4671654/about.rdf'"/>
         <xsl:map-entry key="'FTOW'" select="'https://sws.geonames.org/5954157/about.rdf'"/>
         <xsl:map-entry key="'FMCM'" select="'https://sws.geonames.org/5955895/about.rdf'"/>
         <xsl:map-entry key="'RVIR'" select="'https://sws.geonames.org/4781708/about.rdf'"/>
         <xsl:map-entry key="'FFRA'" select="'https://sws.geonames.org/5955826/about.rdf'"/>
         <xsl:map-entry key="'VM'" select="'https://sws.geonames.org/6174335/about.rdf'"/>
         <xsl:map-entry key="'WM'" select="'https://sws.geonames.org/6179226/about.rdf'"/>
         <xsl:map-entry key="'ATL'" select="'https://sws.geonames.org/4180439/about.rdf'"/>
         <xsl:map-entry key="'ALMA'" select="'https://sws.geonames.org/5884083/about.rdf'"/>
         <xsl:map-entry key="'NAR'" select="'https://sws.geonames.org/8449722/about.rdf'"/>
         <xsl:map-entry key="'HOUL'" select="'https://sws.geonames.org/4967563/about.rdf'"/>
         <xsl:map-entry key="'TA'" select="'https://sws.geonames.org/4174715/about.rdf'"/>
         <xsl:map-entry key="'SADB'" select="'https://sws.geonames.org/6137733/about.rdf'"/>
         <xsl:map-entry key="'TECUM'" select="'https://sws.geonames.org/6162416/about.rdf'"/>
         <xsl:map-entry key="'SIM'" select="'https://sws.geonames.org/6147962/about.rdf'"/>
         <xsl:map-entry key="'NFNY'" select="'https://sws.geonames.org/5128723/about.rdf'"/>
         <xsl:map-entry key="'MINSP'" select="'https://sws.geonames.org/6955117/about.rdf'"/>
         <xsl:map-entry key="'CHARLE'" select="'https://sws.geonames.org/4988575/about.rdf'"/>
      </xsl:map>
   </xsl:variable>
   <xsl:variable name="wikidata_place_ref" as="map(xs:string, xs:string)">
      <xsl:map>
         <xsl:map-entry key="'AUS'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q408.rdf'"/>
         <xsl:map-entry key="'AUST'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q40.rdf'"/>
         <xsl:map-entry key="'BEL'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q31.rdf'"/>
         <xsl:map-entry key="'BELA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q184.rdf'"/>
         <xsl:map-entry key="'BRAZ'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q155.rdf'"/>
         <xsl:map-entry key="'CAN'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q16.rdf'"/>
         <xsl:map-entry key="'CHN'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q148.rdf'"/>
         <xsl:map-entry key="'CUBA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q241.rdf'"/>
         <xsl:map-entry key="'DEN'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q35.rdf'"/>
         <xsl:map-entry key="'ENG'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q21.rdf'"/>
         <xsl:map-entry key="'EST'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q191.rdf'"/>
         <xsl:map-entry key="'FIN'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q33.rdf'"/>
         <xsl:map-entry key="'FRA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q142.rdf'"/>
         <xsl:map-entry key="'GER'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q183.rdf'"/>
         <xsl:map-entry key="'GR'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q41.rdf'"/>
         <xsl:map-entry key="'HTI'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q790.rdf'"/>
         <xsl:map-entry key="'HUN'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q28.rdf'"/>
         <xsl:map-entry key="'INDI'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q668.rdf'"/>
         <xsl:map-entry key="'INDO'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q252.rdf'"/>
         <xsl:map-entry key="'ITA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q38.rdf'"/>
         <xsl:map-entry key="'IRE'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q27.rdf'"/>
         <xsl:map-entry key="'JAM'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q766.rdf'"/>
         <xsl:map-entry key="'JAP'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q17.rdf'"/>
         <xsl:map-entry key="'MOROC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1028.rdf'"/>
         <xsl:map-entry key="'MYS'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q833.rdf'"/>
         <xsl:map-entry key="'NETH'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q55.rdf'"/>
         <xsl:map-entry key="'NIRE'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q26.rdf'"/>
         <xsl:map-entry key="'PALE'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q219060.rdf'"/>
         <xsl:map-entry key="'POL'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q36.rdf'"/>
         <xsl:map-entry key="'RUS'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2184.rdf'"/>
         <xsl:map-entry key="'SCOT'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q22.rdf'"/>
         <xsl:map-entry key="'SWE'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q34.rdf'"/>
         <xsl:map-entry key="'UK'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q145.rdf'"/>
         <xsl:map-entry key="'USA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q30.rdf'"/>
         <xsl:map-entry key="'WALES'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q25.rdf'"/>
         <xsl:map-entry key="'AB'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1951.rdf'"/>
         <xsl:map-entry key="'BC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3195268.rdf'"/>
         <xsl:map-entry key="'MB'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1948.rdf'"/>
         <xsl:map-entry key="'NB'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1965.rdf'"/>
         <xsl:map-entry key="'NL'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2003.rdf'"/>
         <xsl:map-entry key="'NS'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1952.rdf'"/>
         <xsl:map-entry key="'NT'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2007.rdf'"/>
         <xsl:map-entry key="'NU'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2023.rdf'"/>
         <xsl:map-entry key="'ON'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1904.rdf'"/>
         <xsl:map-entry key="'PE'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1979.rdf'"/>
         <xsl:map-entry key="'SK'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1989.rdf'"/>
         <xsl:map-entry key="'YT'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2009.rdf'"/>
         <xsl:map-entry key="'AL'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3709658.rdf'"/>
         <xsl:map-entry key="'AK'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q797.rdf'"/>
         <xsl:map-entry key="'AZ'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q816.rdf'"/>
         <xsl:map-entry key="'AR'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1612.rdf'"/>
         <xsl:map-entry key="'CA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q99.rdf'"/>
         <xsl:map-entry key="'CO'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5148680.rdf'"/>
         <xsl:map-entry key="'CT'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q779.rdf'"/>
         <xsl:map-entry key="'DE'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1393.rdf'"/>
         <xsl:map-entry key="'FL'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q812.rdf'"/>
         <xsl:map-entry key="'GA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1428.rdf'"/>
         <xsl:map-entry key="'HI'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q782.rdf'"/>
         <xsl:map-entry key="'ID'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1221.rdf'"/>
         <xsl:map-entry key="'IL'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1204.rdf'"/>
         <xsl:map-entry key="'IN'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1415.rdf'"/>
         <xsl:map-entry key="'IA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2238109.rdf'"/>
         <xsl:map-entry key="'KS'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1558.rdf'"/>
         <xsl:map-entry key="'KY'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q6392158.rdf'"/>
         <xsl:map-entry key="'LA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1588.rdf'"/>
         <xsl:map-entry key="'ME'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q724.rdf'"/>
         <xsl:map-entry key="'MD'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1391.rdf'"/>
         <xsl:map-entry key="'MA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q771.rdf'"/>
         <xsl:map-entry key="'MI'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q49357026.rdf'"/>
         <xsl:map-entry key="'MN'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1527.rdf'"/>
         <xsl:map-entry key="'MS'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1494.rdf'"/>
         <xsl:map-entry key="'MO'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q272560.rdf'"/>
         <xsl:map-entry key="'MT'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1212.rdf'"/>
         <xsl:map-entry key="'NE'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1553.rdf'"/>
         <xsl:map-entry key="'NV'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1227.rdf'"/>
         <xsl:map-entry key="'NH'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3472179.rdf'"/>
         <xsl:map-entry key="'NJ'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1408.rdf'"/>
         <xsl:map-entry key="'NM'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1522.rdf'"/>
         <xsl:map-entry key="'NY'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1384.rdf'"/>
         <xsl:map-entry key="'NC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1454.rdf'"/>
         <xsl:map-entry key="'ND'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1207.rdf'"/>
         <xsl:map-entry key="'OH'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q927274.rdf'"/>
         <xsl:map-entry key="'OK'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q7082094.rdf'"/>
         <xsl:map-entry key="'OR'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q824.rdf'"/>
         <xsl:map-entry key="'PA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1400.rdf'"/>
         <xsl:map-entry key="'RI'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1387.rdf'"/>
         <xsl:map-entry key="'SC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1456.rdf'"/>
         <xsl:map-entry key="'TN'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1509.rdf'"/>
         <xsl:map-entry key="'TX'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1439.rdf'"/>
         <xsl:map-entry key="'UT'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q829.rdf'"/>
         <xsl:map-entry key="'VT'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2224910.rdf'"/>
         <xsl:map-entry key="'VA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1370.rdf'"/>
         <xsl:map-entry key="'WA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1223.rdf'"/>
         <xsl:map-entry key="'WV'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1371.rdf'"/>
         <xsl:map-entry key="'WI'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1537.rdf'"/>
         <xsl:map-entry key="'WY'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1214.rdf'"/>
         <xsl:map-entry key="'DC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3551781.rdf'"/>
         <xsl:map-entry key="'ABB'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q271730.rdf'"/>
         <xsl:map-entry key="'AJAX'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q386567.rdf'"/>
         <xsl:map-entry key="'ALLAN'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q704795.rdf'"/>
         <xsl:map-entry key="'AMH'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q470594.rdf'"/>
         <xsl:map-entry key="'AMOS'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q141705.rdf'"/>
         <xsl:map-entry key="'ANNA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q564536.rdf'"/>
         <xsl:map-entry key="'ANTIG'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q575483.rdf'"/>
         <xsl:map-entry key="'ARG'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2860953.rdf'"/>
         <xsl:map-entry key="'AURO'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q266438.rdf'"/>
         <xsl:map-entry key="'BAIEC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q139654.rdf'"/>
         <xsl:map-entry key="'BARR'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q34183.rdf'"/>
         <xsl:map-entry key="'BATHU'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q810807.rdf'"/>
         <xsl:map-entry key="'BOL'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q891636.rdf'"/>
         <xsl:map-entry key="'BOUV'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q141924.rdf'"/>
         <xsl:map-entry key="'BOWM'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2922719.rdf'"/>
         <xsl:map-entry key="'BR'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q44198.rdf'"/>
         <xsl:map-entry key="'BRA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q34180.rdf'"/>
         <xsl:map-entry key="'BRAM'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q61432.rdf'"/>
         <xsl:map-entry key="'BROV'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q34047.rdf'"/>
         <xsl:map-entry key="'BUR'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q244025.rdf'"/>
         <xsl:map-entry key="'BURL'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q34218.rdf'"/>
         <xsl:map-entry key="'CAL'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q36312.rdf'"/>
         <xsl:map-entry key="'CAMPB'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1030568.rdf'"/>
         <xsl:map-entry key="'CAMR'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q775610.rdf'"/>
         <xsl:map-entry key="'CARD'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1743545.rdf'"/>
         <xsl:map-entry key="'CARL'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5041335.rdf'"/>
         <xsl:map-entry key="'CB'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q991363.rdf'"/>
         <xsl:map-entry key="'CF'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5028194.rdf'"/>
         <xsl:map-entry key="'CHA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q141522.rdf'"/>
         <xsl:map-entry key="'CHAR'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2127.rdf'"/>
         <xsl:map-entry key="'CHAT'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1068188.rdf'"/>
         <xsl:map-entry key="'CHES'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1070656.rdf'"/>
         <xsl:map-entry key="'CHIC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q952269.rdf'"/>
         <xsl:map-entry key="'CK'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q113464.rdf'"/>
         <xsl:map-entry key="'COB'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1007418.rdf'"/>
         <xsl:map-entry key="'COLL'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3501912.rdf'"/>
         <xsl:map-entry key="'COOKL'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5167136.rdf'"/>
         <xsl:map-entry key="'COW'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q142107.rdf'"/>
         <xsl:map-entry key="'CRAN'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1138837.rdf'"/>
         <xsl:map-entry key="'CREIG'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3858430.rdf'"/>
         <xsl:map-entry key="'DANV'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q142481.rdf'"/>
         <xsl:map-entry key="'DART'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q497373.rdf'"/>
         <xsl:map-entry key="'DELT'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q851838.rdf'"/>
         <xsl:map-entry key="'DESCH'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q985593.rdf'"/>
         <xsl:map-entry key="'DM'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q14875138.rdf'"/>
         <xsl:map-entry key="'DOW'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5303305.rdf'"/>
         <xsl:map-entry key="'DUND'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q620726.rdf'"/>
         <xsl:map-entry key="'EDM'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2096.rdf'"/>
         <xsl:map-entry key="'EGAN'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q4529741.rdf'"/>
         <xsl:map-entry key="'ERI'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3492596.rdf'"/>
         <xsl:map-entry key="'ETOB'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1020159.rdf'"/>
         <xsl:map-entry key="'FLE'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5458720.rdf'"/>
         <xsl:map-entry key="'FLOR'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3074056.rdf'"/>
         <xsl:map-entry key="'FRE'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2138.rdf'"/>
         <xsl:map-entry key="'FV'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5061111.rdf'"/>
         <xsl:map-entry key="'FE'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q268103.rdf'"/>
         <xsl:map-entry key="'FQU'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1866018.rdf'"/>
         <xsl:map-entry key="'GAL'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1492058.rdf'"/>
         <xsl:map-entry key="'GAT'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q141844.rdf'"/>
         <xsl:map-entry key="'GEO'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3103735.rdf'"/>
         <xsl:map-entry key="'GLACE'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3108251.rdf'"/>
         <xsl:map-entry key="'GMERE'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q291278.rdf'"/>
         <xsl:map-entry key="'GP'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q642900.rdf'"/>
         <xsl:map-entry key="'GRA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q7230651.rdf'"/>
         <xsl:map-entry key="'GRAN'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q141881.rdf'"/>
         <xsl:map-entry key="'GT'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3103735.rdf'"/>
         <xsl:map-entry key="'GUE'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q504114.rdf'"/>
         <xsl:map-entry key="'HAL'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2141.rdf'"/>
         <xsl:map-entry key="'HAM'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q133116.rdf'"/>
         <xsl:map-entry key="'HAND'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q14874948.rdf'"/>
         <xsl:map-entry key="'HAR'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5666481.rdf'"/>
         <xsl:map-entry key="'HAWK'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2573770.rdf'"/>
         <xsl:map-entry key="'HUDS'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q141445.rdf'"/>
         <xsl:map-entry key="'HUL'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2995979.rdf'"/>
         <xsl:map-entry key="'HUMB'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1904018.rdf'"/>
         <xsl:map-entry key="'INDH'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1911671.rdf'"/>
         <xsl:map-entry key="'INSI'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q6038156.rdf'"/>
         <xsl:map-entry key="'JASP'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q999429.rdf'"/>
         <xsl:map-entry key="'JONQ'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3183626.rdf'"/>
         <xsl:map-entry key="'KAM'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q473209.rdf'"/>
         <xsl:map-entry key="'KEL'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q232226.rdf'"/>
         <xsl:map-entry key="'KEN'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q993714.rdf'"/>
         <xsl:map-entry key="'KENTV'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q992432.rdf'"/>
         <xsl:map-entry key="'KIN'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q202973.rdf'"/>
         <xsl:map-entry key="'KINC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3492582.rdf'"/>
         <xsl:map-entry key="'KIT'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q16801136.rdf'"/>
         <xsl:map-entry key="'LAC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1474128.rdf'"/>
         <xsl:map-entry key="'LAM'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q142113.rdf'"/>
         <xsl:map-entry key="'LAS'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1435042.rdf'"/>
         <xsl:map-entry key="'LAV'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q141810.rdf'"/>
         <xsl:map-entry key="'LENX'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q672089.rdf'"/>
         <xsl:map-entry key="'LETH'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q270887.rdf'"/>
         <xsl:map-entry key="'LIND'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3241437.rdf'"/>
         <xsl:map-entry key="'LON'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q92561.rdf'"/>
         <xsl:map-entry key="'LONG'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q139398.rdf'"/>
         <xsl:map-entry key="'MANI'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q141777.rdf'"/>
         <xsl:map-entry key="'MAR'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q44016.rdf'"/>
         <xsl:map-entry key="'MAXV'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q50347614.rdf'"/>
         <xsl:map-entry key="'MAYF'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q6797110.rdf'"/>
         <xsl:map-entry key="'MEA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1904178.rdf'"/>
         <xsl:map-entry key="'MEDH'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q368241.rdf'"/>
         <xsl:map-entry key="'MIL'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q425057.rdf'"/>
         <xsl:map-entry key="'MJAW'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1019496.rdf'"/>
         <xsl:map-entry key="'MIS'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q50816.rdf'"/>
         <xsl:map-entry key="'MONC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q457334.rdf'"/>
         <xsl:map-entry key="'MONTJ'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q141717.rdf'"/>
         <xsl:map-entry key="'MONTL'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q142058.rdf'"/>
         <xsl:map-entry key="'MON'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q340.rdf'"/>
         <xsl:map-entry key="'MONN'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1714436.rdf'"/>
         <xsl:map-entry key="'MOMT'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1795689.rdf'"/>
         <xsl:map-entry key="'MULM'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q7848795.rdf'"/>
         <xsl:map-entry key="'NANA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q16461.rdf'"/>
         <xsl:map-entry key="'NBAT'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q999654.rdf'"/>
         <xsl:map-entry key="'NCAS'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3339099.rdf'"/>
         <xsl:map-entry key="'NEWM'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q52999.rdf'"/>
         <xsl:map-entry key="'NEWR'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q142295.rdf'"/>
         <xsl:map-entry key="'NF'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q274120.rdf'"/>
         <xsl:map-entry key="'NIP'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1915543.rdf'"/>
         <xsl:map-entry key="'NPN'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1639083.rdf'"/>
         <xsl:map-entry key="'NWM'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q876122.rdf'"/>
         <xsl:map-entry key="'NORTHY'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1586510.rdf'"/>
         <xsl:map-entry key="'NORV'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q7060735.rdf'"/>
         <xsl:map-entry key="'NVAN'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1001626.rdf'"/>
         <xsl:map-entry key="'OAK'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q424984.rdf'"/>
         <xsl:map-entry key="'ODES'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2201332.rdf'"/>
         <xsl:map-entry key="'ORI'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2373358.rdf'"/>
         <xsl:map-entry key="'OSHA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q211867.rdf'"/>
         <xsl:map-entry key="'OTT'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1930.rdf'"/>
         <xsl:map-entry key="'PALI'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2957506.rdf'"/>
         <xsl:map-entry key="'PASQ'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q7142311.rdf'"/>
         <xsl:map-entry key="'PRA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q671431.rdf'"/>
         <xsl:map-entry key="'PET'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q776930.rdf'"/>
         <xsl:map-entry key="'PICK'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q386626.rdf'"/>
         <xsl:map-entry key="'PICTC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2092485.rdf'"/>
         <xsl:map-entry key="'PG'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q590205.rdf'"/>
         <xsl:map-entry key="'POHO'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q613870.rdf'"/>
         <xsl:map-entry key="'POI'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3393516.rdf'"/>
         <xsl:map-entry key="'PP'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3388664.rdf'"/>
         <xsl:map-entry key="'QUE'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2145.rdf'"/>
         <xsl:map-entry key="'RD'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q23025.rdf'"/>
         <xsl:map-entry key="'REG'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2123.rdf'"/>
         <xsl:map-entry key="'RIC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q52998.rdf'"/>
         <xsl:map-entry key="'RICHM'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q236837.rdf'"/>
         <xsl:map-entry key="'RIMO'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1336.rdf'"/>
         <xsl:map-entry key="'ROBV'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3437016.rdf'"/>
         <xsl:map-entry key="'ROSET'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1916875.rdf'"/>
         <xsl:map-entry key="'ROU'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3443238.rdf'"/>
         <xsl:map-entry key="'RNOR'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q141804.rdf'"/>
         <xsl:map-entry key="'SACK'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2028824.rdf'"/>
         <xsl:map-entry key="'SAI'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2117999.rdf'"/>
         <xsl:map-entry key="'SAS'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q10566.rdf'"/>
         <xsl:map-entry key="'SCA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1025401.rdf'"/>
         <xsl:map-entry key="'SECH'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1259274.rdf'"/>
         <xsl:map-entry key="'SGDB'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3462211.rdf'"/>
         <xsl:map-entry key="'SHEF'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3481727.rdf'"/>
         <xsl:map-entry key="'SHER'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q139473.rdf'"/>
         <xsl:map-entry key="'STEU'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q141505.rdf'"/>
         <xsl:map-entry key="'ASCOT'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2866421.rdf'"/>
         <xsl:map-entry key="'SHAW'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q141980.rdf'"/>
         <xsl:map-entry key="'SJ'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3462595.rdf'"/>
         <xsl:map-entry key="'SOO'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q993727.rdf'"/>
         <xsl:map-entry key="'SMEA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2204552.rdf'"/>
         <xsl:map-entry key="'SSM'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q463165.rdf'"/>
         <xsl:map-entry key="'STC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q126805.rdf'"/>
         <xsl:map-entry key="'STCAS'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3461942.rdf'"/>
         <xsl:map-entry key="'STCAM'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q338345.rdf'"/>
         <xsl:map-entry key="'STE'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1026567.rdf'"/>
         <xsl:map-entry key="'STI'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q4442642.rdf'"/>
         <xsl:map-entry key="'STJ'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2082.rdf'"/>
         <xsl:map-entry key="'STJE'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q141724.rdf'"/>
         <xsl:map-entry key="'STJO'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q203403.rdf'"/>
         <xsl:map-entry key="'STR'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q740756.rdf'"/>
         <xsl:map-entry key="'STTHO'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2078072.rdf'"/>
         <xsl:map-entry key="'SUD'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q383434.rdf'"/>
         <xsl:map-entry key="'SUMS'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q863058.rdf'"/>
         <xsl:map-entry key="'SYDN'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q932261.rdf'"/>
         <xsl:map-entry key="'THU'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q34116.rdf'"/>
         <xsl:map-entry key="'TIM'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q991310.rdf'"/>
         <xsl:map-entry key="'TOR'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q172.rdf'"/>
         <xsl:map-entry key="'TR'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q44012.rdf'"/>
         <xsl:map-entry key="'TREH'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q281304.rdf'"/>
         <xsl:map-entry key="'TRUR'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q283064.rdf'"/>
         <xsl:map-entry key="'UPT'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1001007.rdf'"/>
         <xsl:map-entry key="'VAL'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q141937.rdf'"/>
         <xsl:map-entry key="'VAN'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q24639.rdf'"/>
         <xsl:map-entry key="'VD'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q518076.rdf'"/>
         <xsl:map-entry key="'VDO'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q141937.rdf'"/>
         <xsl:map-entry key="'VER'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q985671.rdf'"/>
         <xsl:map-entry key="'VIC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2132.rdf'"/>
         <xsl:map-entry key="'WELL'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q424773.rdf'"/>
         <xsl:map-entry key="'WEYB'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1904554.rdf'"/>
         <xsl:map-entry key="'WIN'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2135.rdf'"/>
         <xsl:map-entry key="'WIND'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q182625.rdf'"/>
         <xsl:map-entry key="'WINK'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1019402.rdf'"/>
         <xsl:map-entry key="'WISH'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q8027570.rdf'"/>
         <xsl:map-entry key="'WLOC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1004113.rdf'"/>
         <xsl:map-entry key="'WOOD'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1642364.rdf'"/>
         <xsl:map-entry key="'WYN'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1917692.rdf'"/>
         <xsl:map-entry key="'ABER'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q319369.rdf'"/>
         <xsl:map-entry key="'ACNC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q491626.rdf'"/>
         <xsl:map-entry key="'AMES'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q470273.rdf'"/>
         <xsl:map-entry key="'LEEP'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q49337801.rdf'"/>
         <xsl:map-entry key="'NEWB'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q138338.rdf'"/>
         <xsl:map-entry key="'ASHEV'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q648501.rdf'"/>
         <xsl:map-entry key="'ALL'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1752800.rdf'"/>
         <xsl:map-entry key="'BARNS'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2461969.rdf'"/>
         <xsl:map-entry key="'BERK'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q484678.rdf'"/>
         <xsl:map-entry key="'BERL'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q64.rdf'"/>
         <xsl:map-entry key="'BIN'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q213814.rdf'"/>
         <xsl:map-entry key="'BIRK'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q746718.rdf'"/>
         <xsl:map-entry key="'BLA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q881140.rdf'"/>
         <xsl:map-entry key="'BLO'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q490385.rdf'"/>
         <xsl:map-entry key="'BLYN'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q18419.rdf'"/>
         <xsl:map-entry key="'BMHAM'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2256.rdf'"/>
         <xsl:map-entry key="'BOS'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q100.rdf'"/>
         <xsl:map-entry key="'BOU'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q170478.rdf'"/>
         <xsl:map-entry key="'BRI'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q23154.rdf'"/>
         <xsl:map-entry key="'BRIS'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q34932.rdf'"/>
         <xsl:map-entry key="'BRO'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q49193.rdf'"/>
         <xsl:map-entry key="'BROWN'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q51693.rdf'"/>
         <xsl:map-entry key="'BRX'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q18426.rdf'"/>
         <xsl:map-entry key="'BSE'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q764754.rdf'"/>
         <xsl:map-entry key="'BUFF'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q40435.rdf'"/>
         <xsl:map-entry key="'CAP'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q216835.rdf'"/>
         <xsl:map-entry key="'CAR'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q10690.rdf'"/>
         <xsl:map-entry key="'CARI'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1010699.rdf'"/>
         <xsl:map-entry key="'CASA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q7903.rdf'"/>
         <xsl:map-entry key="'CHE'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q755851.rdf'"/>
         <xsl:map-entry key="'CHI'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1297.rdf'"/>
         <xsl:map-entry key="'CHICK'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2794790.rdf'"/>
         <xsl:map-entry key="'CLE'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q37320.rdf'"/>
         <xsl:map-entry key="'CLEV'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q670079.rdf'"/>
         <xsl:map-entry key="'CLYD'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q996502.rdf'"/>
         <xsl:map-entry key="'COLB'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1110876.rdf'"/>
         <xsl:map-entry key="'COLU'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q16567.rdf'"/>
         <xsl:map-entry key="'COV'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q6225.rdf'"/>
         <xsl:map-entry key="'DALL'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q16557.rdf'"/>
         <xsl:map-entry key="'DONI'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1241544.rdf'"/>
         <xsl:map-entry key="'DORC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q503331.rdf'"/>
         <xsl:map-entry key="'DOUNE'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1014524.rdf'"/>
         <xsl:map-entry key="'EC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q281839.rdf'"/>
         <xsl:map-entry key="'EDI'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q23436.rdf'"/>
         <xsl:map-entry key="'ELM'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2180242.rdf'"/>
         <xsl:map-entry key="'EUG'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q171224.rdf'"/>
         <xsl:map-entry key="'EXET'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q134672.rdf'"/>
         <xsl:map-entry key="'FALK'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q623687.rdf'"/>
         <xsl:map-entry key="'FUKUO'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q26600.rdf'"/>
         <xsl:map-entry key="'GALES'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q782574.rdf'"/>
         <xsl:map-entry key="'GC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q739452.rdf'"/>
         <xsl:map-entry key="'GLAS'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q4093.rdf'"/>
         <xsl:map-entry key="'GUIL'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q213465.rdf'"/>
         <xsl:map-entry key="'HAC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q138458.rdf'"/>
         <xsl:map-entry key="'HAMP'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q25610.rdf'"/>
         <xsl:map-entry key="'HARN'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5659749.rdf'"/>
         <xsl:map-entry key="'HESP'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3319689.rdf'"/>
         <xsl:map-entry key="'HOU'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q16555.rdf'"/>
         <xsl:map-entry key="'HYDE'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1361.rdf'"/>
         <xsl:map-entry key="'IND'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q6346.rdf'"/>
         <xsl:map-entry key="'INDE'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q24603.rdf'"/>
         <xsl:map-entry key="'JACK'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q16568.rdf'"/>
         <xsl:map-entry key="'KENN'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1181684.rdf'"/>
         <xsl:map-entry key="'KING'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q34692.rdf'"/>
         <xsl:map-entry key="'LASV'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q23768.rdf'"/>
         <xsl:map-entry key="'LODZ'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q580.rdf'"/>
         <xsl:map-entry key="'LOLL'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q625061.rdf'"/>
         <xsl:map-entry key="'LONE'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q84.rdf'"/>
         <xsl:map-entry key="'LOS'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q65.rdf'"/>
         <xsl:map-entry key="'MALA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q61089.rdf'"/>
         <xsl:map-entry key="'MANC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q18125.rdf'"/>
         <xsl:map-entry key="'MANH'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q286148.rdf'"/>
         <xsl:map-entry key="'MET'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1021527.rdf'"/>
         <xsl:map-entry key="'MIA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q8652.rdf'"/>
         <xsl:map-entry key="'MILW'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q37836.rdf'"/>
         <xsl:map-entry key="'MIN'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q36091.rdf'"/>
         <xsl:map-entry key="'MONR'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q947115.rdf'"/>
         <xsl:map-entry key="'NDEL'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q987.rdf'"/>
         <xsl:map-entry key="'NEW'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q60.rdf'"/>
         <xsl:map-entry key="'NEWA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q25395.rdf'"/>
         <xsl:map-entry key="'PRINC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q138518.rdf'"/>
         <xsl:map-entry key="'NEWC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q985283.rdf'"/>
         <xsl:map-entry key="'NOORD'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q848988.rdf'"/>
         <xsl:map-entry key="'NORF'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q49231.rdf'"/>
         <xsl:map-entry key="'NP'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q989960.rdf'"/>
         <xsl:map-entry key="'OAKL'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q17042.rdf'"/>
         <xsl:map-entry key="'OKC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q34863.rdf'"/>
         <xsl:map-entry key="'OWS'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1017735.rdf'"/>
         <xsl:map-entry key="'PALM'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q488004.rdf'"/>
         <xsl:map-entry key="'PAR'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q90.rdf'"/>
         <xsl:map-entry key="'PARM'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2683.rdf'"/>
         <xsl:map-entry key="'PASA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q485176.rdf'"/>
         <xsl:map-entry key="'PATC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q580140.rdf'"/>
         <xsl:map-entry key="'PIML'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q123306.rdf'"/>
         <xsl:map-entry key="'PIT'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q49184.rdf'"/>
         <xsl:map-entry key="'PITTS'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1913953.rdf'"/>
         <xsl:map-entry key="'PLA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q544890.rdf'"/>
         <xsl:map-entry key="'POR'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q6106.rdf'"/>
         <xsl:map-entry key="'PORH'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1000222.rdf'"/>
         <xsl:map-entry key="'RICH'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1009324.rdf'"/>
         <xsl:map-entry key="'RIVS'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q49243.rdf'"/>
         <xsl:map-entry key="'ROCH'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q49218.rdf'"/>
         <xsl:map-entry key="'SA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q975.rdf'"/>
         <xsl:map-entry key="'SACR'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q18013.rdf'"/>
         <xsl:map-entry key="'SAOL'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1751337.rdf'"/>
         <xsl:map-entry key="'SAVA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q83813.rdf'"/>
         <xsl:map-entry key="'SCHEN'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q331380.rdf'"/>
         <xsl:map-entry key="'SDI'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q16552.rdf'"/>
         <xsl:map-entry key="'SEA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5083.rdf'"/>
         <xsl:map-entry key="'SF'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q62.rdf'"/>
         <xsl:map-entry key="'SMON'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q47164.rdf'"/>
         <xsl:map-entry key="'SS'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q755741.rdf'"/>
         <xsl:map-entry key="'SOLE'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q985256.rdf'"/>
         <xsl:map-entry key="'SWIN'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q200942.rdf'"/>
         <xsl:map-entry key="'SYD'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3130.rdf'"/>
         <xsl:map-entry key="'TALL'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1770.rdf'"/>
         <xsl:map-entry key="'THOR'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q448120.rdf'"/>
         <xsl:map-entry key="'TOUL'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q7880.rdf'"/>
         <xsl:map-entry key="'TRENT'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q577120.rdf'"/>
         <xsl:map-entry key="'TRU'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3452563.rdf'"/>
         <xsl:map-entry key="'TULS'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q44989.rdf'"/>
         <xsl:map-entry key="'VERN'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q236736.rdf'"/>
         <xsl:map-entry key="'VICB'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q493787.rdf'"/>
         <xsl:map-entry key="'VIEN'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1741.rdf'"/>
         <xsl:map-entry key="'WARD'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1511397.rdf'"/>
         <xsl:map-entry key="'WAS'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q61.rdf'"/>
         <xsl:map-entry key="'WHOL'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q846421.rdf'"/>
         <xsl:map-entry key="'WOND'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2332648.rdf'"/>
         <xsl:map-entry key="'YOG'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q7568.rdf'"/>
         <xsl:map-entry key="'YOR'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q493518.rdf'"/>
         <xsl:map-entry key="'YORK'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q2323126.rdf'"/>
         <xsl:map-entry key="'AUSTI'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q16559.rdf'"/>
         <xsl:map-entry key="'HBT'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q14875189.rdf'"/>
         <xsl:map-entry key="'HRC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1514655.rdf'"/>
         <xsl:map-entry key="'MCCT'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3585486.rdf'"/>
         <xsl:map-entry key="'NDS'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3344686.rdf'"/>
         <xsl:map-entry key="'SAM'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q7586728.rdf'"/>
         <xsl:map-entry key="'BSUC'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q4928405.rdf'"/>
         <xsl:map-entry key="'FTOW'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5457112.rdf'"/>
         <xsl:map-entry key="'FMCM'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q126624.rdf'"/>
         <xsl:map-entry key="'RVIR'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q43421.rdf'"/>
         <xsl:map-entry key="'FFRA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1438546.rdf'"/>
         <xsl:map-entry key="'VM'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q141645.rdf'"/>
         <xsl:map-entry key="'WM'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q139497.rdf'"/>
         <xsl:map-entry key="'ATL'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q23556.rdf'"/>
         <xsl:map-entry key="'ALMA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q141973.rdf'"/>
         <xsl:map-entry key="'NAR'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q6965309.rdf'"/>
         <xsl:map-entry key="'HOUL'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q913153.rdf'"/>
         <xsl:map-entry key="'AGIN'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q4692573.rdf'"/>
         <xsl:map-entry key="'TA'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q37043.rdf'"/>
         <xsl:map-entry key="'SADB'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q141500.rdf'"/>
         <xsl:map-entry key="'TECUM'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3089334.rdf'"/>
         <xsl:map-entry key="'SIM'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3054408.rdf'"/>
         <xsl:map-entry key="'ROM'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q220.rdf'"/>
         <xsl:map-entry key="'NFNY'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q128133.rdf'"/>
         <xsl:map-entry key="'MINSP'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1480013.rdf'"/>
         <xsl:map-entry key="'CHARLE'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q1066741.rdf'"/>
      </xsl:map>
   </xsl:variable>
</xsl:stylesheet>
