<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:schema="https://schema.org/"
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:map="http://www.w3.org/2005/xpath-functions/map"
                version="3.0">
   <xsl:variable name="viaf_perio_ref" as="map(xs:string, xs:string)">
      <xsl:map/>
   </xsl:variable>
   <xsl:variable name="wikidata_perio_ref" as="map(xs:string, xs:string)">
      <xsl:map>
         <xsl:map-entry key="'p9'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q27718209.rdf'"/>
         <xsl:map-entry key="'p10'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q3395962.rdf'"/>
         <xsl:map-entry key="'p31'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q5528540.rdf'"/>
         <xsl:map-entry key="'p52'"
                        select="'http://www.wikidata.org/wiki/Special:EntityData/Q6481094.rdf'"/>
      </xsl:map>
   </xsl:variable>
   <xsl:variable name="isni_perio_ref" as="map(xs:string, xs:string)">
      <xsl:map/>
   </xsl:variable>
</xsl:stylesheet>
