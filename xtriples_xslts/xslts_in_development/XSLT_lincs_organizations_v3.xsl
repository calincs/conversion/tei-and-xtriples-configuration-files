<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:schema="https://schema.org/"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    xmlns:frbroo="http://iflastandards.info/ns/fr/frbr/frbroo/" 
    xmlns:tei="http://www.tei-c/ns/1.0"
    xmlns:owl="http://www.w3.org/TR/2004/REC-owl-guide-20040210/"
    xmlns:fa="http://www.w3.org/2005/xpath-functions"
    xmlns:crm="http://www.cidoc-crm.org/cidoc-crm/"
    xmlns:map="http://www.w3.org/2005/xpath-functions/map/"
    exclude-result-prefixes="xsl schema rdf tei fa owl xs"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0" version="2.0"
       xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#">
    <xsl:output method="xml" encoding="UTF-16" indent="yes"/> <!--declare frbroo-->
    
    <xsl:template match="/">
        <rdf:RDF xml:lang="en"   
            xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
            xmlns:crm="http://www.cidoc-crm.org/cidoc-crm/"
            xmlns:skos="http://www.w3.org/2004/02/skos/core#"
            xmlns:temp_lincs_temp="http://temp.lincsproject.ca/"
            xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#">
            <xsl:apply-templates select="descendant::listOrg"/>
        </rdf:RDF>
    </xsl:template>
      
    <xsl:template match="listOrg">
        <xsl:apply-templates select="org"/>
    </xsl:template>
    
    <xsl:template match="org">
          
        <xsl:apply-templates select="orgName[@type='standard']"/>
            
            <crm:P2_has_type>
                <crm:E55_Type ="http://xmlns.com/foaf/0.1/#term_Organization"/>
            </crm:P2_has_type>
            
            <xsl:comment>Placeholder for the establishment/termination of an Organisation:
                            ?crm:P49_was_created_by
                                ?crm:E63_Beginning_of_Existence/crm:E64_End_of_Existence
                                    ?crm:P92_was_brought_into_existence_by
                                        ?crm:E77_Persistent_Item</xsl:comment>
            
    </xsl:template>
    
    <xsl:template match="orgName[@type='standard']">
        <xsl:apply-templates select="descendant::orgName"/>
    </xsl:template>
    
    <xsl:template match="orgName">
        <xsl:element name="{concat('crm:', 'E74_Group')}">
            <xsl:attribute name="" select="@ref"></xsl:attribute><!-- 00 insert <http://temp.lincsproject.ca/> when available -->
            
            <rdfs:label>
                <xsl:value-of select="normalize-space(.)"/>
            </rdfs:label>
           
        </xsl:element>
    </xsl:template>
    
    
   <!--    
    <E74_Group ="uri_org">
     crm for event establishment
    </E74_Group>
-->

    
        
    
 
</xsl:stylesheet>
