<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:schema="https://schema.org/"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c/ns/1.0"
    xmlns:frbroo="D" 
    xmlns:owl="http://www.w3.org/TR/2004/REC-owl-guide-20040210/"
    xmlns:fa="http://www.w3.org/2005/xpath-functions"
    xmlns:crm="http://www.cidoc-crm.org/cidoc-crm/"
    xmlns:cwrc="http://sparql.cwrc.ca/ontologies/cwrc.html"
    xmlns:map="http://www.w3.org/2005/xpath-functions/map"
    exclude-result-prefixes="cwrc xsl schema rdf tei fa owl xs"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0" version="2.0"
       xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#">
    <xsl:output method="xml" encoding="UTF-16" indent="yes"/> <!--declare frbroo-->
    
    <xsl:template match="/">
        <rdf:RDF xml:lang="en"   
            xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
            xmlns:crm="http://www.cidoc-crm.org/cidoc-crm/"
            xmlns:skos="http://www.w3.org/2004/02/skos/core#"
            xmlns:temp_lincs_temp="http://temp.lincsproject.ca/"
            xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#">
            <xsl:apply-templates select="descendant::listPerson"/>
        </rdf:RDF>
    </xsl:template>
    
    <!-- TODO Add cwrc URIs + process <note> with the word doc inside LINCS Drive -->
    
    <xsl:template match="listPerson">
        <xsl:apply-templates select="person"/>
    </xsl:template>
    
    <xsl:template match="person">
        <crm:E39_Actor rdf:about="uri:Name_of_Person">
            <rdfs:label>
                <xsl:value-of select="normalize-space(persName[@type='standard'])"/>
            </rdfs:label>  
                      
            <xsl:apply-templates select="child::persName[@type='preferred']"/>
            <xsl:apply-templates select="persName[@type='variant']"/>
            <xsl:apply-templates select="birth"/>
            <xsl:apply-templates select="death"/>
            <xsl:apply-templates select="nationality"/>
            <xsl:apply-templates select="occupation"/>
            <xsl:apply-templates select="note[@type='general']"/>
            <!--About <floruit>: CC & AD: There isn’t a application profile for this so we should leave it for now to be safe-->
            <!--About <trait>: CC & AD: There isn’t a application profile for this so we should leave it for now to be safe-->
            <!--About <respons>: CC & AD: There isn’t a application profile for this so we should leave it for now to be safe-->
            <xsl:apply-templates select="sex"/>
            
        </crm:E39_Actor>       
    </xsl:template>

    
    <xsl:template match="persName[@type='preferred']">
            <crm:P1_is_identified_by>
                <crm:E33_E41_Linguistic_Appellation rdf:about="uri:Name_of_Person">
                    <rdfs:label>Name of <xsl:value-of select="normalize-space(.)"/>
                    </rdfs:label>
                    <crm:P190_has_symbolic_content xml:lang="en">
                        <xsl:value-of select="normalize-space(.)"/>
                    </crm:P190_has_symbolic_content>
                    <crm:P2_has_type>
                        <crm:E55_Type rdf:about="https://sparql.cwrc.ca/ontologies/cwrc.html#PersonalName"/>
                    </crm:P2_has_type>
                    
                   <xsl:apply-templates select="child::name[@type='forename']"/> 
                    <xsl:apply-templates select="child::name[@type='middlename']"/>
                    <xsl:apply-templates select="child::name[@type='surname']"/>
                    <xsl:apply-templates select="child::genName"/>
                    <!--<xsl:apply-templates select="child::roleName"/>
                    CC & AD: There isn’t a application profile for this so we should leave it for now to be safe-->                                      
                    
                </crm:E33_E41_Linguistic_Appellation>
            </crm:P1_is_identified_by>
    </xsl:template>
    
    <xsl:template match="persName[@type='variant']">
        <crm:P1_is_identified_by>
            <crm:E33_E41_Linguistic_Appellation rdf:about="uri:Name_of_Person">
                <rdfs:label>Name of <xsl:value-of select="normalize-space(.)"/>
                </rdfs:label>
                <crm:P190_has_symbolic_content xml:lang="en">
                    <xsl:value-of select="normalize-space(.)"/>
                </crm:P190_has_symbolic_content>
                <crm:P2_has_type>
                    <crm:E55_Type rdf:about="http://sparql.cwrc.ca/ontologies/cwrc#PersonalName"/>
                </crm:P2_has_type>
                
                <xsl:apply-templates select="child::name[@type='forename']"/> 
                <xsl:apply-templates select="child::name[@type='middlename']"/>
                <xsl:apply-templates select="child::name[@type='surname']"/>
                <xsl:apply-templates select="child::genName"/>
                                                                
            </crm:E33_E41_Linguistic_Appellation>
        </crm:P1_is_identified_by>

        <crm:P67i_is_referred_to_by>
           <xsl:apply-templates select="child::note"/>
        </crm:P67i_is_referred_to_by>
    </xsl:template>
 
    <xsl:template match="name[@type='forename']">
        <crm:P106_is_composed_of xml:lang="en">
        <crm:E33_E41_Linguistic_Appellation rdf:about="uri:First_Name_of_Person">
            <rdfs:label>Forename of <xsl:value-of select="ancestor::persName[@type='preferred']"/>
            </rdfs:label>
            <crm:P190_has_symbolic_content xml:lang="en">
                <xsl:value-of select="normalize-space(.)"/>
            </crm:P190_has_symbolic_content>
            <crm:P2_has_type>
                <crm:E55_Type rdf:about="https://sparql.cwrc.ca/ontologies/cwrc.html#Forename"/>
            </crm:P2_has_type>
        </crm:E33_E41_Linguistic_Appellation>
        </crm:P106_is_composed_of>
    </xsl:template>
        
       
    <xsl:template match="name[@type='middlename']">        
        <crm:P106_is_composed_of xml:lang="en">
            <crm:E33_E41_Linguistic_Appellation rdf:about="uri:Middlename_of_Person">
                <rdfs:label>Middlename of <xsl:value-of select="ancestor::persName[@type='preferred']"/>
                </rdfs:label>
                <crm:P190_has_symbolic_content xml:lang="en">
                    <xsl:value-of select="normalize-space(.)"/>
                </crm:P190_has_symbolic_content>
                <crm:P2_has_type>
                    <crm:E55_Type rdf:about="uri:cwrc.html#Middlename"/>
                </crm:P2_has_type>
            </crm:E33_E41_Linguistic_Appellation>
        </crm:P106_is_composed_of>    
    </xsl:template>
    
    <xsl:template match="name[@type='surname']">
        <crm:P106_is_composed_of xml:lang="en">
            <crm:E33_E41_Linguistic_Appellation rdf:about="uri:Surname_of_Person">
                <rdfs:label>Surname of <xsl:value-of select="ancestor::persName[@type='preferred']"/>
                </rdfs:label>
                <crm:P190_has_symbolic_content xml:lang="en">
                    <xsl:value-of select="normalize-space(.)"/>
                </crm:P190_has_symbolic_content>
                <crm:P2_has_type>
                    <crm:E55_Type rdf:about="http://sparql.cwrc.ca/ontologies/cwrc#Surname"/>
                </crm:P2_has_type>
            </crm:E33_E41_Linguistic_Appellation>
        </crm:P106_is_composed_of>    
    </xsl:template>
    
    <xsl:template match="genName">
        <crm:P106_is_composed_of xml:lang="en">
            <crm:E33_E41_Linguistic_Appellation rdf:about="uri:genName_of_Person">
                <rdfs:label>Generational name of <xsl:value-of select="ancestor::persName[@type='preferred']"/>
                </rdfs:label>
                <crm:P190_has_symbolic_content xml:lang="en">
                    <xsl:value-of select="normalize-space(.)"/>
                </crm:P190_has_symbolic_content>
                <crm:P2_has_type>
                    <crm:E55_Type rdf:about="https://sparql.cwrc.ca/ontologies/cwrc.html#GenerationalName"/>
                </crm:P2_has_type>
            </crm:E33_E41_Linguistic_Appellation>
        </crm:P106_is_composed_of>    
    </xsl:template>
    
    <xsl:template match="roleName">
        <crm:P106_is_composed_of xml:lang="en">
            <crm:E33_E41_Linguistic_Appellation rdf:about="uri:roleName_of_Person">
                <rdfs:label>Role name of <xsl:value-of select="ancestor::persName[@type='preferred']"/>
                </rdfs:label>
                <crm:P190_has_symbolic_content xml:lang="en">
                    <xsl:value-of select="normalize-space(.)"/>
                </crm:P190_has_symbolic_content>
                <crm:P2_has_type>
                    <crm:E55_Type rdf:about="http://sparql.cwrc.ca/ontologies/cwrc#RoleName"/>
                </crm:P2_has_type>
            </crm:E33_E41_Linguistic_Appellation>
        </crm:P106_is_composed_of>    
    </xsl:template>
    
    <xsl:template match="note">
        <crm:E33_Linguistic_Object rdf:about="uri:Name_of_Person"> 
            <rdfs:label>Name of label : Note about the person's variant name</rdfs:label>
            <crm:P190_has_symbolic_content xml:lang="en">
                <xsl:value-of select="normalize-space(.)"/>
            </crm:P190_has_symbolic_content>
            <crm:P2_has_type>
                <crm:E55_Type rdf:about="http://sparql.cwrc.ca/ontologies/cwrc#Note"/>
            </crm:P2_has_type>            
        </crm:E33_Linguistic_Object>        
    </xsl:template>
        
    <xsl:template match="birth">
        <crm:P98i_was_born>
            <crm:E67_Birth rdf:about="uri:Name_of_Person">
                <rdfs:label>Birth of <xsl:value-of select="normalize-space(preceding-sibling::persName[@type='preferred'])"/></rdfs:label>
               
               <xsl:apply-templates select="./date"/>
               <!--<xsl:apply-templates select="./placeName"/>
               CC: we need to be able to associate the place with the fact that the person was born there (just as we do with a timespan “this person had a birth time and this is the birthtime and its properties”). Leave it in comments, not converting, waiting for an official “How to connect placenames to birth and death events” from LINCS
               CC: They’ll have URIs for all the places coming from LEAF Writer-->
               
            </crm:E67_Birth>
        </crm:P98i_was_born>
    </xsl:template>
    
    <xsl:template match="death">
        <crm:P100i_died_in>
            <crm:E69_Death rdf:about="uri:Name_of_Person">
                <rdfs:label>Death of <xsl:value-of select="normalize-space(preceding-sibling::persName[@type='preferred'])"/></rdfs:label>
                
                <xsl:apply-templates select="./date"/>
                <!--<xsl:apply-templates select="./placeName"/>
                 CC: we need to be able to associate the place with the fact that the person was born there (just as we do with a timespan “this person had a birth time and this is the birthtime and its properties”). Leave it in comments, not converting, waiting for an official “How to connect placenames to birth and death events” from LINCS
                CC: They’ll have URIs for all the places coming from LEAF Writer-->
                <!--Information pointing to the location of Birth will go in here when we have the CIDOC pattern for Birth locations-->
                
            </crm:E69_Death>
        </crm:P100i_died_in>
    </xsl:template>
    
    <xsl:template match="birth/date">
        <crm:P4_has_time-span>
            <crm:E52_Time-Span rdf:about="uri:Time_Span">
                <rdfs:label>Datetime of birth of <xsl:value-of select="normalize-space(preceding::persName[@type='preferred'])"/></rdfs:label>
                <crm:P82_at_some_time_within>"<xsl:value-of select="@when"/>"^^xsd:dateTime</crm:P82_at_some_time_within>
                <crm:P82b_end_of_the_end>"<xsl:value-of select="@when"/>"^^xsd:dateTime</crm:P82b_end_of_the_end>
                <crm:P82a_begin_of_the_begin>"<xsl:value-of select="@when"/>"^^xsd:dateTime</crm:P82a_begin_of_the_begin> 
            </crm:E52_Time-Span>
        </crm:P4_has_time-span>
    </xsl:template>
    
    <!--AD: Not transforming yet, because we don't know how to connect the E53 Place to the birth and death event, missing a P_property?
        <xsl:template match="birth/placeName">        
        <crm:E53_Place rdf:about="uri:Name_of_Place?">
            <rdfs:label>
                <xsl:value-of select="normalize-space(.)"/>
            </rdfs:label>            
            <crm:P1_is_identified_by>
                <crm:E33_E41_Linguistic_Appellation rdf:about="https://sparql.cwrc.ca/ontologies/cwrc.html#birthPlace">
                    <crm:P190_has_symbolic_content>
                        <xsl:value-of select="normalize-space(.)"/>
                    </crm:P190_has_symbolic_content>
                </crm:E33_E41_Linguistic_Appellation>
            </crm:P1_is_identified_by>
        </crm:E53_Place>        
    </xsl:template>-->
    
    <xsl:template match="death/date">
        <crm:P4_has_time-span>
            <crm:E52_Time-Span rdf:about="uri:Time_Span">
                <rdfs:label>Datetime of death of <xsl:value-of select="normalize-space(preceding::persName[@type='preferred'])"/></rdfs:label>
                <crm:P82_at_some_time_within>"<xsl:value-of select="@when"/>"^^xsd:dateTime</crm:P82_at_some_time_within>
                <crm:P82b_end_of_the_end>"<xsl:value-of select="@when"/>"^^xsd:dateTime</crm:P82b_end_of_the_end>
                <crm:P82a_begin_of_the_begin>"<xsl:value-of select="@when"/>"^^xsd:dateTime</crm:P82a_begin_of_the_begin> 
            </crm:E52_Time-Span>
        </crm:P4_has_time-span>
    </xsl:template>
    
    <!-- AD: Not transforming yet, because we don't know how to connect the E53 Place to the birth and death event, missing a P_property?
        <xsl:template match="death/placeName">        
        <crm:E53_Place rdf:about="uri:Name_of_Place?">
            <rdfs:label><xsl:value-of select="normalize-space(.)"/></rdfs:label>            
            <crm:P1_is_identified_by>
                <crm:E33_E41_Linguistic_Appellation rdf:about="https://sparql.cwrc.ca/ontologies/cwrc.html#deathPlace">
                    <crm:P190_has_symbolic_content>
                        <xsl:value-of select="normalize-space(.)"/>
                    </crm:P190_has_symbolic_content>
                </crm:E33_E41_Linguistic_Appellation>
            </crm:P1_is_identified_by>
        </crm:E53_Place>        
    </xsl:template>-->
      
    <!-- Mapping the NATIONALITY
            1 - Define a variable to store the beginning of our URI. -->
    <xsl:variable name="nationalityPrefix" 
        select="'http://sparql.cwrc.ca/ontologies/cwrc#'"/>
    <!-- 2 - Define a variable to store the map then create the map.-->
    <xsl:variable name="nationality" as="map(xs:string, xs:string)">
        <xsl:map>
            <xsl:map-entry key="'africanNationalIdentity'" select="'africanNationalIdentity'"/>
            <xsl:map-entry key="'anglo-IndianNationalIdentity'" select="'anglo-IndianNationalIdentity'"/>
            <xsl:map-entry key="'anglo-IrishNationalIdentity'" select="'anglo-IrishNationalIdentity'"/>
            <xsl:map-entry key="'britishNationalIdentity'" select="'britishNationalIdentity'"/>
            <xsl:map-entry key="'europeanNationalIdentity'" select="'europeanNationalIdentity'"/>
            <xsl:map-entry key="'florentineNationalIdentity'" select="'florentineNationalIdentity'"/>
            <xsl:map-entry key="'hanoverianNationalIdentity'" select="'hanoverianNationalIdentity'"/>
            <xsl:map-entry key="'jewishNationalIdentity'" select="'jewishNationalIdentity'"/>
            <xsl:map-entry key="'mohawkNationalIdentity'" select="'mohawkNationalIdentity'"/>
            <xsl:map-entry key="'saxonNationalIdentity'" select="'saxonNationalIdentity'"/>
            <xsl:map-entry key="'sephardicJewishNationalIdentity'" select="'sephardicJewishNationalIdentity'"/>
            <xsl:map-entry key="'statelessNationalIdentity'" select="'statelessNationalIdentity'"/>            
        </xsl:map>
    </xsl:variable>
    
   <xsl:template match="nationality">
        <crm:P74_has_current_or_former_residence>
            <crm:E53_Place rdf:about="uri:Name_of_Place">
                <crm:P1_is_identified_by>
                    <crm:E33_E41_Linguistic_Appellation rdf:about="uri:Name_of_Place">
                        <crm:P190_has_symbolic_content xml:lang="en">
                            <xsl:value-of select="normalize-space(.)"/> : nationality of the person e.g. the place where they live
                        </crm:P190_has_symbolic_content>
                        <crm:P2_has_type>
                            <xsl:choose>
                                <xsl:when test="map:contains($nationality, current())">
                                    <crm:E55_Type rdf:about="{concat($nationalityPrefix, $nationality(current()), '')}"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:comment>ERROR : No URI was found for nationality : "<xsl:value-of select="current()"/>". Please check the content of the @value or rely on our documentation to input correct format.</xsl:comment>
                                </xsl:otherwise>
                            </xsl:choose>         
                        </crm:P2_has_type>                        
                    </crm:E33_E41_Linguistic_Appellation>
                </crm:P1_is_identified_by>
            </crm:E53_Place>
        </crm:P74_has_current_or_former_residence>
    </xsl:template>
    
    <!-- Mapping the OCCUPATION
            1 - Define a variable to store the beginning of our URI. -->
    <xsl:variable name="occupationPrefix" 
        select="'http://id.lincsproject.ca/occupation/'"/>
    <!-- 2 - Define a variable to store the map then create the map.-->
    <xsl:variable name="occupation" as="map(xs:string, xs:string)">
        <xsl:map>
            <xsl:map-entry key="'abolitionist'" select="'abolitionist'"/>
            <xsl:map-entry key="'academic'" select="'academic'"/>
            <xsl:map-entry key="'accountant'" select="'accountant'"/>
            <xsl:map-entry key="'activist'" select="'activist'"/>
            <xsl:map-entry key="'actorManager'" select="'actorManager'"/>
            <xsl:map-entry key="'adjudicator'" select="'adjudicator'"/>
            <xsl:map-entry key="'administrator'" select="'administrator'"/>
            <xsl:map-entry key="'advertising'" select="'advertising'"/>
            <xsl:map-entry key="'advocate'" select="'advocate'"/>
            <xsl:map-entry key="'agent'" select="'agent'"/>
            <xsl:map-entry key="'airForce'" select="'airForce'"/>
            <xsl:map-entry key="'airRaidWarden'" select="'airRaidWarden'"/>
            <xsl:map-entry key="'aircraftIndustry'" select="'aircraftIndustry'"/>
            <xsl:map-entry key="'airman'" select="'airman'"/>
            <xsl:map-entry key="'alternativeMedicine'" select="'alternativeMedicine'"/>
            <xsl:map-entry key="'ambulanceWorker'" select="'ambulanceWorker'"/>
            <xsl:map-entry key="'anti-Suffrage'" select="'anti-Suffrage'"/>
            <xsl:map-entry key="'appraiser'" select="'appraiser'"/>
            <xsl:map-entry key="'archaeologist'" select="'archaeologist'"/>
            <xsl:map-entry key="'architecture'" select="'architecture'"/>
            <xsl:map-entry key="'archivist'" select="'archivist'"/>
            <xsl:map-entry key="'aristocrat'" select="'aristocrat'"/>
            <xsl:map-entry key="'army'" select="'army'"/>
            <xsl:map-entry key="'artsAdministrator'" select="'artsAdministrator'"/>
            <xsl:map-entry key="'artsAdvocate'" select="'artsAdvocate'"/>
            <xsl:map-entry key="'assistant'" select="'assistant'"/>
            <xsl:map-entry key="'associationInvolvement'" select="'associationInvolvement'"/>
            <xsl:map-entry key="'cwrc#astronomer'" select="'cwrc#astronomer'"/>
            <xsl:map-entry key="'attorneyGeneral'" select="'attorneyGeneral'"/>
            <xsl:map-entry key="'auctioneer'" select="'auctioneer'"/>
            <xsl:map-entry key="'automobileIndustry'" select="'automobileIndustry'"/>
            <xsl:map-entry key="'baker'" select="'baker'"/>
            <xsl:map-entry key="'bankrupt'" select="'bankrupt'"/>
            <xsl:map-entry key="'beautician'" select="'beautician'"/>
            <xsl:map-entry key="'beauty'" select="'beauty'"/>
            <xsl:map-entry key="'beggar'" select="'beggar'"/>
            <xsl:map-entry key="'bibliophile'" select="'bibliophile'"/>
            <xsl:map-entry key="'billCollector'" select="'billCollector'"/>
            <xsl:map-entry key="'biologist'" select="'biologist'"/>
            <xsl:map-entry key="'blacksmith'" select="'blacksmith'"/>
            <xsl:map-entry key="'boardMember'" select="'boardMember'"/>
            <xsl:map-entry key="'boardingHouseKeeper'" select="'boardingHouseKeeper'"/>
            <xsl:map-entry key="'bookProduction'" select="'bookProduction'"/>
            <xsl:map-entry key="'bookbinder'" select="'bookbinder'"/>
            <xsl:map-entry key="'bookclubParticipatant'" select="'bookclubParticipatant'"/>
            <xsl:map-entry key="'botanist'" select="'botanist'"/>
            <xsl:map-entry key="'brewer'" select="'brewer'"/>
            <xsl:map-entry key="'broadcasting'" select="'broadcasting'"/>
            <xsl:map-entry key="'bullionCarrier'" select="'bullionCarrier'"/>
            <xsl:map-entry key="'bursar'" select="'bursar'"/>
            <xsl:map-entry key="'business'" select="'business'"/>
            <xsl:map-entry key="'businessOwner'" select="'businessOwner'"/>
            <xsl:map-entry key="'butcher'" select="'butcher'"/>
            <xsl:map-entry key="'calligraphy'" select="'calligraphy'"/>
            <xsl:map-entry key="'captain'" select="'captain'"/>
            <xsl:map-entry key="'careGiver'" select="'careGiver'"/>
            <xsl:map-entry key="'cartoonist'" select="'cartoonist'"/>
            <xsl:map-entry key="'catering'" select="'catering'"/>
            <xsl:map-entry key="'censor'" select="'censor'"/>
            <xsl:map-entry key="'chairperson'" select="'chairperson'"/>
            <xsl:map-entry key="'chaperone'" select="'chaperone'"/>
            <xsl:map-entry key="'charityWorker'" select="'charityWorker'"/>
            <xsl:map-entry key="'chemist'" select="'chemist'"/>
            <xsl:map-entry key="'chess'" select="'chess'"/>
            <xsl:map-entry key="'childcare'" select="'childcare'"/>
            <xsl:map-entry key="'childcareWork'" select="'childcareWork'"/>
            <xsl:map-entry key="'classicist'" select="'classicist'"/>
            <xsl:map-entry key="'cleaner'" select="'cleaner'"/>
            <xsl:map-entry key="'clericalWork'" select="'clericalWork'"/>
            <xsl:map-entry key="'clothingProduction'" select="'clothingProduction'"/>
            <xsl:map-entry key="'clothworker'" select="'clothworker'"/>
            <xsl:map-entry key="'coach'" select="'coach'"/>
            <xsl:map-entry key="'coastGuard'" select="'coastGuard'"/>
            <xsl:map-entry key="'cobbler'" select="'cobbler'"/>
            <xsl:map-entry key="'collector'" select="'collector'"/>
            <xsl:map-entry key="'colonialOfficial'" select="'colonialOfficial'"/>
            <xsl:map-entry key="'commissioner'" select="'commissioner'"/>
            <xsl:map-entry key="'committeeMember'" select="'committeeMember'"/>
            <xsl:map-entry key="'communeOrganizer'" select="'communeOrganizer'"/>
            <xsl:map-entry key="'communism'" select="'communism'"/>
            <xsl:map-entry key="'companion'" select="'companion'"/>
            <xsl:map-entry key="'composer'" select="'composer'"/>
            <xsl:map-entry key="'computerScientist'" select="'computerScientist'"/>
            <xsl:map-entry key="'computerWork'" select="'computerWork'"/>
            <xsl:map-entry key="'confectioner'" select="'confectioner'"/>
            <xsl:map-entry key="'connoisseur'" select="'connoisseur'"/>
            <xsl:map-entry key="'conservationist'" select="'conservationist'"/>
            <xsl:map-entry key="'conservatism'" select="'conservatism'"/>
            <xsl:map-entry key="'construction'" select="'construction'"/>
            <xsl:map-entry key="'consultant'" select="'consultant'"/>
            <xsl:map-entry key="'contractor'" select="'contractor'"/>
            <xsl:map-entry key="'cooking'" select="'cooking'"/>
            <xsl:map-entry key="'coopOrganizer'" select="'coopOrganizer'"/>
            <xsl:map-entry key="'cooper'" select="'cooper'"/>
            <xsl:map-entry key="'copyist'" select="'copyist'"/>
            <xsl:map-entry key="'corporateDirector'" select="'corporateDirector'"/>
            <xsl:map-entry key="'counsellor'" select="'counsellor'"/>
            <xsl:map-entry key="'courtier'" select="'courtier'"/>
            <xsl:map-entry key="'craftsperson'" select="'craftsperson'"/>
            <xsl:map-entry key="'cricket'" select="'cricket'"/>
            <xsl:map-entry key="'criminal'" select="'criminal'"/>
            <xsl:map-entry key="'critic'" select="'critic'"/>
            <xsl:map-entry key="'curator'" select="'curator'"/>
            <xsl:map-entry key="'cyclist'" select="'cyclist'"/>
            <xsl:map-entry key="'dairyWork'" select="'dairyWork'"/>
            <xsl:map-entry key="'dance'" select="'dance'"/>
            <xsl:map-entry key="'dataProcessing'" select="'dataProcessing'"/>
            <xsl:map-entry key="'decoder'" select="'decoder'"/>
            <xsl:map-entry key="'deliveryPerson'" select="'deliveryPerson'"/>
            <xsl:map-entry key="'dentist'" select="'dentist'"/>
            <xsl:map-entry key="'designer'" select="'designer'"/>
            <xsl:map-entry key="'diplomat'" select="'diplomat'"/>
            <xsl:map-entry key="'director'" select="'director'"/>
            <xsl:map-entry key="'diver'" select="'diver'"/>
            <xsl:map-entry key="'dockyardWork'" select="'dockyardWork'"/>
            <xsl:map-entry key="'domesticServant'" select="'domesticServant'"/>
            <xsl:map-entry key="'domesticWork'" select="'domesticWork'"/>
            <xsl:map-entry key="'draper'" select="'draper'"/>
            <xsl:map-entry key="'dressmaking'" select="'dressmaking'"/>
            <xsl:map-entry key="'drinking'" select="'drinking'"/>
            <xsl:map-entry key="'drugUse'" select="'drugUse'"/>
            <xsl:map-entry key="'editing'" select="'editing'"/>
            <xsl:map-entry key="'education'" select="'education'"/>
            <xsl:map-entry key="'educationalAdministration'" select="'educationalAdministration'"/>
            <xsl:map-entry key="'elevatorOperator'" select="'elevatorOperator'"/>
            <xsl:map-entry key="'elocutionist'" select="'elocutionist'"/>
            <xsl:map-entry key="'employer'" select="'employer'"/>
            <xsl:map-entry key="'employmentAgent'" select="'employmentAgent'"/>
            <xsl:map-entry key="'engineering'" select="'engineering'"/>
            <xsl:map-entry key="'engraver'" select="'engraver'"/>
            <xsl:map-entry key="'equineActivity'" select="'equineActivity'"/>
            <xsl:map-entry key="'eventOrganizer'" select="'eventOrganizer'"/>
            <xsl:map-entry key="'exhibitions'" select="'exhibitions'"/>
            <xsl:map-entry key="'explorer'" select="'explorer'"/>
            <xsl:map-entry key="'factoryWorker'" select="'factoryWorker'"/>
            <xsl:map-entry key="'farming'" select="'farming'"/>
            <xsl:map-entry key="'fashion'" select="'fashion'"/>
            <xsl:map-entry key="'fellow'" select="'fellow'"/>
            <xsl:map-entry key="'feminism'" select="'feminism'"/>
            <xsl:map-entry key="'filmIndustry'" select="'filmIndustry'"/>
            <xsl:map-entry key="'finance'" select="'finance'"/>
            <xsl:map-entry key="'firefighter'" select="'firefighter'"/>
            <xsl:map-entry key="'#firstAid'" select="'#firstAid'"/>
            <xsl:map-entry key="'fisherman'" select="'fisherman'"/>
            <xsl:map-entry key="'fishmonger'" select="'fishmonger'"/>
            <xsl:map-entry key="'foodDistribution'" select="'foodDistribution'"/>
            <xsl:map-entry key="'fosterParent'" select="'fosterParent'"/>
            <xsl:map-entry key="'founder'" select="'founder'"/>
            <xsl:map-entry key="'fundraiser'" select="'fundraiser'"/>
            <xsl:map-entry key="'gambling'" select="'gambling'"/>
            <xsl:map-entry key="'gardener'" select="'gardener'"/>
            <xsl:map-entry key="'gastronome'" select="'gastronome'"/>
            <xsl:map-entry key="'genealogist'" select="'genealogist'"/>
            <xsl:map-entry key="'gentleman'" select="'gentleman'"/>
            <xsl:map-entry key="'geographer'" select="'geographer'"/>
            <xsl:map-entry key="'geologist'" select="'geologist'"/>
            <xsl:map-entry key="'glassWorker'" select="'glassWorker'"/>
            <xsl:map-entry key="'glover'" select="'glover'"/>
            <xsl:map-entry key="'goldsmith'" select="'goldsmith'"/>
            <xsl:map-entry key="'governess'" select="'governess'"/>
            <xsl:map-entry key="'government'" select="'government'"/>
            <xsl:map-entry key="'governmentOfficial'" select="'governmentOfficial'"/>
            <xsl:map-entry key="'governorGeneral'" select="'governorGeneral'"/>
            <xsl:map-entry key="'grocer'" select="'grocer'"/>
            <xsl:map-entry key="'guardian'" select="'guardian'"/>
            <xsl:map-entry key="'haberdasher'" select="'haberdasher'"/>
            <xsl:map-entry key="'handwritingExpert'" select="'handwritingExpert'"/>
            <xsl:map-entry key="'headOfState'" select="'headOfState'"/>
            <xsl:map-entry key="'healthCareProvider'" select="'healthCareProvider'"/>
            <xsl:map-entry key="'higherGovernment'" select="'higherGovernment'"/>
            <xsl:map-entry key="'historian'" select="'historian'"/>
            <xsl:map-entry key="'hospitality'" select="'hospitality'"/>
            <xsl:map-entry key="'hostess'" select="'hostess'"/>
            <xsl:map-entry key="'housePainter'" select="'housePainter'"/>
            <xsl:map-entry key="'houseSitter'" select="'houseSitter'"/>
            <xsl:map-entry key="'housekeeper'" select="'housekeeper'"/>
            <xsl:map-entry key="'housework'" select="'housework'"/>
            <xsl:map-entry key="'humanitarianWork'" select="'humanitarianWork'"/>
            <xsl:map-entry key="'hunter'" select="'hunter'"/>
            <xsl:map-entry key="'hypnotist'" select="'hypnotist'"/>
            <xsl:map-entry key="'illustrator'" select="'illustrator'"/>
            <xsl:map-entry key="'immunologist'" select="'immunologist'"/>
            <xsl:map-entry key="'insurance'" select="'insurance'"/>
            <xsl:map-entry key="'intellectual'" select="'intellectual'"/>
            <xsl:map-entry key="'intelligenceWork'" select="'intelligenceWork'"/>
            <xsl:map-entry key="'interviewer'" select="'interviewer'"/>
            <xsl:map-entry key="'inventor'" select="'inventor'"/>
            <xsl:map-entry key="'investor'" select="'investor'"/>
            <xsl:map-entry key="'ironmaster'" select="'ironmaster'"/>
            <xsl:map-entry key="'ironmonger'" select="'ironmonger'"/>
            <xsl:map-entry key="'italianNationalism'" select="'italianNationalism'"/>
            <xsl:map-entry key="'jacobitism'" select="'jacobitism'"/>
            <xsl:map-entry key="'jeweller'" select="'jeweller'"/>
            <xsl:map-entry key="'journalist'" select="'journalist'"/>
            <xsl:map-entry key="'judge'" select="'judge'"/>
            <xsl:map-entry key="'knight'" select="'knight'"/>
            <xsl:map-entry key="'labourUnion'" select="'labourUnion'"/>
            <xsl:map-entry key="'laundryWorker'" select="'laundryWorker'"/>
            <xsl:map-entry key="'lawEnforcement'" select="'lawEnforcement'"/>
            <xsl:map-entry key="'lawyer'" select="'lawyer'"/>
            <xsl:map-entry key="'leatherWorker'" select="'leatherWorker'"/>
            <xsl:map-entry key="'legalWork'" select="'legalWork'"/>            
            <xsl:map-entry key="'liaisonOfficer'" select="'liaisonOfficer'"/>
            <xsl:map-entry key="'librarian'" select="'librarian'"/>
            <xsl:map-entry key="'lifeScientist'" select="'lifeScientist'"/>
            <xsl:map-entry key="'linguist'" select="'linguist'"/>
            <xsl:map-entry key="'literaryAgent'" select="'literaryAgent'"/>
            <xsl:map-entry key="'literaryAssistant'" select="'literaryAssistant'"/>
            <xsl:map-entry key="'literaryConservationist'" select="'literaryConservationist'"/>
            <xsl:map-entry key="'literaryExecutor'" select="'literaryExecutor'"/>
            <xsl:map-entry key="'literaryForger'" select="'literaryForger'"/>
            <xsl:map-entry key="'literaryGroupParticipant'" select="'literaryGroupParticipant'"/>
            <xsl:map-entry key="'literaryPrizeAdjudicator'" select="'literaryPrizeAdjudicator'"/>
            <xsl:map-entry key="'literaryScholar'" select="'literaryScholar'"/>
            <xsl:map-entry key="'lithographer'" select="'lithographer'"/>
            <xsl:map-entry key="'litigant'" select="'litigant'"/>
            <xsl:map-entry key="'lobbyist'" select="'lobbyist'"/>
            <xsl:map-entry key="'localGovernment'" select="'localGovernment'"/>
            <xsl:map-entry key="'logger'" select="'logger'"/>
            <xsl:map-entry key="'lordChamberlain'" select="'lordChamberlain'"/>
            <xsl:map-entry key="'lordChancellor'" select="'lordChancellor'"/>
            <xsl:map-entry key="'machineWork'" select="'machineWork'"/>
            <xsl:map-entry key="'magistrate'" select="'magistrate'"/>
            <xsl:map-entry key="'maidOfHonour'" select="'maidOfHonour'"/>
            <xsl:map-entry key="'manager'" select="'manager'"/>
            <xsl:map-entry key="'manservant'" select="'manservant'"/>
            <xsl:map-entry key="'manualLabourer'" select="'manualLabourer'"/>
            <xsl:map-entry key="'manufacturer'" select="'manufacturer'"/>
            <xsl:map-entry key="'mapping'" select="'mapping'"/>
            <xsl:map-entry key="'martyr'" select="'martyr'"/>
            <xsl:map-entry key="'mason'" select="'mason'"/>
            <xsl:map-entry key="'masterOfTheRevels'" select="'masterOfTheRevels'"/>
            <xsl:map-entry key="'mathematics'" select="'mathematics'"/>
            <xsl:map-entry key="'matron'" select="'matron'"/>
            <xsl:map-entry key="'mayor'" select="'mayor'"/>
            <xsl:map-entry key="'medicalDoctor'" select="'medicalDoctor'"/>
            <xsl:map-entry key="'memberOfParliament'" select="'memberOfParliament'"/>
            <xsl:map-entry key="'mentalHealthProfessional'" select="'mentalHealthProfessional'"/>
            <xsl:map-entry key="'mentor'" select="'mentor'"/>
            <xsl:map-entry key="'mercer'" select="'mercer'"/>
            <xsl:map-entry key="'merchant'" select="'merchant'"/>
            <xsl:map-entry key="'merchantTaylor'" select="'merchantTaylor'"/>
            <xsl:map-entry key="'metalWork'" select="'metalWork'"/>
            <xsl:map-entry key="'metallurgy'" select="'metallurgy'"/>
            <xsl:map-entry key="'meteorologist'" select="'meteorologist'"/>
            <xsl:map-entry key="'midwife'" select="'midwife'"/>
            <xsl:map-entry key="'military'" select="'military'"/>
            <xsl:map-entry key="'miller'" select="'miller'"/>
            <xsl:map-entry key="'milliner'" select="'milliner'"/>
            <xsl:map-entry key="'miner'" select="'miner'"/>
            <xsl:map-entry key="'mining'" select="'mining'"/>
            <xsl:map-entry key="'miser'" select="'miser'"/>
            <xsl:map-entry key="'missionaryWork'" select="'missionaryWork'"/>
            <xsl:map-entry key="'mistress'" select="'mistress'"/>
            <xsl:map-entry key="'model'" select="'model'"/>
            <xsl:map-entry key="'monarch'" select="'monarch'"/>
            <xsl:map-entry key="'monarchism'" select="'monarchism'"/>
            <xsl:map-entry key="'mountaineering'" select="'mountaineering'"/>
            <xsl:map-entry key="'munitionsWorker'" select="'munitionsWorker'"/>
            <xsl:map-entry key="'music'" select="'music'"/>
            <xsl:map-entry key="'musicologist'" select="'musicologist'"/>
            <xsl:map-entry key="'naturalist'" select="'naturalist'"/>
            <xsl:map-entry key="'navy'" select="'navy'"/>
            <xsl:map-entry key="'needlework'" select="'needlework'"/>
            <xsl:map-entry key="'numismatist'" select="'numismatist'"/>
            <xsl:map-entry key="'nun'" select="'nun'"/>
            <xsl:map-entry key="'nursing'" select="'nursing'"/>
            <xsl:map-entry key="'occultism'" select="'occultism'"/>
            <xsl:map-entry key="'optician'" select="'optician'"/>
            <xsl:map-entry key="'orderly'" select="'orderly'"/>
            <xsl:map-entry key="'orphanageWork'" select="'orphanageWork'"/>
            <xsl:map-entry key="'pacifist'" select="'pacifist'"/>
            <xsl:map-entry key="'parishWork'" select="'parishWork'"/>
            <xsl:map-entry key="'patriot'" select="'patriot'"/>
            <xsl:map-entry key="'patron'" select="'patron'"/>
            <xsl:map-entry key="'pawnbroker'" select="'pawnbroker'"/>
            <xsl:map-entry key="'performer'" select="'performer'"/>
            <xsl:map-entry key="'pharmacist'" select="'pharmacist'"/>
            <xsl:map-entry key="'philanthropicVisitor'" select="'philanthropicVisitor'"/>
            <xsl:map-entry key="'philanthropist'" select="'philanthropist'"/>
            <xsl:map-entry key="'philosopher'" select="'philosopher'"/>
            <xsl:map-entry key="'photography'" select="'photography'"/>
            <xsl:map-entry key="'physiognomist'" select="'physiognomist'"/>
            <xsl:map-entry key="'pilot'" select="'pilot'"/>
            <xsl:map-entry key="'plumber'" select="'plumber'"/>
            <xsl:map-entry key="'poetLaureate'" select="'poetLaureate'"/>
            <xsl:map-entry key="'politicalHost'" select="'politicalHost'"/>
            <xsl:map-entry key="'politicalScience'" select="'politicalScience'"/>
            <xsl:map-entry key="'politicalSpeaker'" select="'politicalSpeaker'"/>
            <xsl:map-entry key="'politics'" select="'politics'"/>
            <xsl:map-entry key="'pollster'" select="'pollster'"/>
            <xsl:map-entry key="'pornographer'" select="'pornographer'"/>
            <xsl:map-entry key="'postalWorker'" select="'postalWorker'"/>
            <xsl:map-entry key="'pottery'" select="'pottery'"/>
            <xsl:map-entry key="'printing'" select="'printing'"/>
            <xsl:map-entry key="'prisoner'" select="'prisoner'"/>
            <xsl:map-entry key="'producer'" select="'producer'"/>
            <xsl:map-entry key="'professor'" select="'professor'"/>
            <xsl:map-entry key="'propagandist'" select="'propagandist'"/>
            <xsl:map-entry key="'propertyAgent'" select="'propertyAgent'"/>
            <xsl:map-entry key="'propertyOwner'" select="'propertyOwner'"/>
            <xsl:map-entry key="'prophet'" select="'prophet'"/>
            <xsl:map-entry key="'prospector'" select="'prospector'"/>
            <xsl:map-entry key="'publicReader'" select="'publicReader'"/>
            <xsl:map-entry key="'publicSpeaker'" select="'publicSpeaker'"/>
            <xsl:map-entry key="'publishing'" select="'publishing'"/>
            <xsl:map-entry key="'radicalism'" select="'radicalism'"/>
            <xsl:map-entry key="'radioIndustry'" select="'radioIndustry'"/>
            <xsl:map-entry key="'radioOperator'" select="'radioOperator'"/>
            <xsl:map-entry key="'railwayWork'" select="'railwayWork'"/>
            <xsl:map-entry key="'reading'" select="'reading'"/>
            <xsl:map-entry key="'rebel'" select="'rebel'"/>
            <xsl:map-entry key="'redCross'" select="'redCross'"/>
            <xsl:map-entry key="'reformer'" select="'reformer'"/>
            <xsl:map-entry key="'refugee'" select="'refugee'"/>
            <xsl:map-entry key="'refugeeWork'" select="'refugeeWork'"/>
            <xsl:map-entry key="'regionalGovernment'" select="'regionalGovernment'"/>
            <xsl:map-entry key="'reliefWork'" select="'reliefWork'"/>
            <xsl:map-entry key="'religious'" select="'religious'"/>
            <xsl:map-entry key="'religiousOfficial'" select="'religiousOfficial'"/>
            <xsl:map-entry key="'religiousStudies'" select="'religiousStudies'"/>
            <xsl:map-entry key="'rescueWork'" select="'rescueWork'"/>
            <xsl:map-entry key="'researcher'" select="'researcher'"/>
            <xsl:map-entry key="'retail'" select="'retail'"/>
            <xsl:map-entry key="'reviewer'" select="'reviewer'"/>
            <xsl:map-entry key="'riding'" select="'riding'"/>
            <xsl:map-entry key="'sailor'" select="'sailor'"/>
            <xsl:map-entry key="'saint'" select="'saint'"/>
            <xsl:map-entry key="'salesperson'" select="'salesperson'"/>
            <xsl:map-entry key="'salter'" select="'salter'"/>
            <xsl:map-entry key="'scientist'" select="'scientist'"/>
            <xsl:map-entry key="'seaCaptain'" select="'seaCaptain'"/>
            <xsl:map-entry key="'servant'" select="'servant'"/>
            <xsl:map-entry key="'server'" select="'server'"/>
            <xsl:map-entry key="'settlementWork'" select="'settlementWork'"/>
            <xsl:map-entry key="'settler'" select="'settler'"/>
            <xsl:map-entry key="'sexWorker'" select="'sexWorker'"/>
            <xsl:map-entry key="'shipping'" select="'shipping'"/>
            <xsl:map-entry key="'singer'" select="'singer'"/>
            <xsl:map-entry key="'skilledTrade'" select="'skilledTrade'"/>
            <xsl:map-entry key="'skinner'" select="'skinner'"/>
            <xsl:map-entry key="'slave'" select="'slave'"/>
            <xsl:map-entry key="'slaveOwner'" select="'slaveOwner'"/>
            <xsl:map-entry key="'slaver'" select="'slaver'"/>
            <xsl:map-entry key="'smuggler'" select="'smuggler'"/>
            <xsl:map-entry key="'socialScientist'" select="'socialScientist'"/>
            <xsl:map-entry key="'socialWork'" select="'socialWork'"/>
            <xsl:map-entry key="'socialism'" select="'socialism'"/>
            <xsl:map-entry key="'socializer'" select="'socializer'"/>
            <xsl:map-entry key="'spinner'" select="'spinner'"/>
            <xsl:map-entry key="'sports'" select="'sports'"/>
            <xsl:map-entry key="'statistician'" select="'statistician'"/>
            <xsl:map-entry key="'storyteller'" select="'storyteller'"/>
            <xsl:map-entry key="'student'" select="'student'"/>
            <xsl:map-entry key="'surveyor'" select="'surveyor'"/>
            <xsl:map-entry key="'tailor'" select="'tailor'"/>
            <xsl:map-entry key="'taxOfficial'" select="'taxOfficial'"/>
            <xsl:map-entry key="'teacher'" select="'teacher'"/>
            <xsl:map-entry key="'technician'" select="'technician'"/>
            <xsl:map-entry key="'telephoneOperator'" select="'telephoneOperator'"/>
            <xsl:map-entry key="'televisionIndustry'" select="'televisionIndustry'"/>
            <xsl:map-entry key="'temperanceMovement'" select="'temperanceMovement'"/>
            <xsl:map-entry key="'textiles'" select="'textiles'"/>
            <xsl:map-entry key="'theatreWork'" select="'theatreWork'"/>
            <xsl:map-entry key="'timberTrade'" select="'timberTrade'"/>
            <xsl:map-entry key="'tinker'" select="'tinker'"/>
            <xsl:map-entry key="'tourGuide'" select="'tourGuide'"/>
            <xsl:map-entry key="'trader'" select="'trader'"/>
            <xsl:map-entry key="'transcriber'" select="'transcriber'"/>
            <xsl:map-entry key="'translator'" select="'translator'"/>
            <xsl:map-entry key="'transportationWork'" select="'transportationWork'"/>
            <xsl:map-entry key="'travelling'" select="'travelling'"/>
            <xsl:map-entry key="'treasurer'" select="'treasurer'"/>
            <xsl:map-entry key="'trustee'" select="'trustee'"/>
            <xsl:map-entry key="'typeSetter'" select="'typeSetter'"/>
            <xsl:map-entry key="'unitarianism'" select="'unitarianism'"/>
            <xsl:map-entry key="'unskilledLabourer'" select="'unskilledLabourer'"/>
            <xsl:map-entry key="'upholsterer'" select="'upholsterer'"/>
            <xsl:map-entry key="'urbanPlanning'" select="'urbanPlanning'"/>
            <xsl:map-entry key="'veterinaryWork'" select="'veterinaryWork'"/>
            <xsl:map-entry key="'vintner'" select="'vintner'"/>
            <xsl:map-entry key="'visualArtist'" select="'visualArtist'"/>
            <xsl:map-entry key="'volunteer'" select="'volunteer'"/>
            <xsl:map-entry key="'walking'" select="'walking'"/>
            <xsl:map-entry key="'warWork'" select="'warWork'"/>
            <xsl:map-entry key="'warehouseWork'" select="'warehouseWork'"/>
            <xsl:map-entry key="'watchmaker'" select="'watchmaker'"/>
            <xsl:map-entry key="'weaver'" select="'weaver'"/>
            <xsl:map-entry key="'welder'" select="'welder'"/>
            <xsl:map-entry key="'wetNurse'" select="'wetNurse'"/>
            <xsl:map-entry key="'whaler'" select="'whaler'"/>
            <xsl:map-entry key="'wife'" select="'wife'"/>
            <xsl:map-entry key="'witness'" select="'witness'"/>
            <xsl:map-entry key="'writer'" select="'writer'"/>
            <xsl:map-entry key="'writerInResidence'" select="'writerInResidence'"/>
            <xsl:map-entry key="'writingGroupParticipant'" select="'writingGroupParticipant'"/>
            <xsl:map-entry key="'yeoman-farmer'" select="'yeoman-farmer'"/>
        </xsl:map>
    </xsl:variable>
    
     <xsl:template match="occupation">
        <crm:P14i_performed>
            <frbroo:F51_Pursuit rdf:about="uri:Pursuit_of_Name_of_Person"> 
                <!-- URI = Name of Person ? or of concept for "Pursuit"?-->
                <crm:P2_has_type>                                     
                    <xsl:choose>
                        <xsl:when test="map:contains($occupation, current())">
                            <crm:E55_Type rdf:about="{concat($occupationPrefix, $occupation(current()), '')}"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:comment>ERROR : No URI was found for occupation : "<xsl:value-of select="current()"/>". Please check the content of the @value or rely on our documentation to input correct format.</xsl:comment>
                        </xsl:otherwise>
                    </xsl:choose>                    
                </crm:P2_has_type>
            </frbroo:F51_Pursuit>
        </crm:P14i_performed>
     </xsl:template>
    
    <!-- Mapping the GENDER
            1 - Define a variable to store the beginning of our URI. -->
    <xsl:variable name="genderPrefix" 
        select="'http://sparql.cwrc.ca/ontologies/cwrc#'"/>
    <!-- 2 - Define a variable to store the map then create the map.-->
    <xsl:variable name="gender" as="map(xs:string, xs:string)">
        <xsl:map>
            <xsl:map-entry key="'Androgynous'" select="'Androgynous'"/>
            <xsl:map-entry key="'GenderQueer'" select="'GenderQueer'"/>
            <xsl:map-entry key="'blackWomanRaceColour'" select="'blackWomanRaceColour'"/>
            <xsl:map-entry key="'cisGender'" select="'cisGender'"/>
            <xsl:map-entry key="'cisMan'" select="'cisMan'"/>
            <xsl:map-entry key="'cisWoman'" select="'cisWoman'"/>
            <xsl:map-entry key="'man'" select="'man'"/>
            <xsl:map-entry key="'transGender'" select="'transGender'"/>
            <xsl:map-entry key="'transMan'" select="'transMan'"/>
            <xsl:map-entry key="'transWoman'" select="'transWoman'"/>
            <xsl:map-entry key="'undefinedGender'" select="'undefinedGender'"/>
            <xsl:map-entry key="'woman'" select="'woman'"/>           
        </xsl:map>
    </xsl:variable>
    
    <xsl:template match="sex">
        <xsl:apply-templates select="@value"/>     
    </xsl:template>
    
    <xsl:template match="@value">
        <crm:P140i_was_attributed_by>
            <crm:E13_Attribute_Assignment>
                <rdfs:label>Gender of <xsl:value-of select="normalize-space(preceding::persName[@type='preferred'])"/>
                </rdfs:label>
                <crm:P2_has_type>
                    <crm:E55_Type rdf:about="https://sparql.cwrc.ca/ontologies/cwrc.html#GenderContext"/>            
                </crm:P2_has_type>
                <crm:P141_assigned>
                    <crm:E7_Activity>
                        <crm:P2_has_type>
                            <crm:E55_Type rdf:about="https://sparql.cwrc.ca/ontologies/cwrc.html#GenderEvent"/>            
                        </crm:P2_has_type>
                        <!-- Calling the map : 
                When the map contains, in $gender, anything that is the current context <sex>
                Then concatenate our $prefix with the current selected $gender value you picked/are in.-->
                        <xsl:choose>
                            <xsl:when test="map:contains($gender, current())">
                                <crm:P16_used_specific_object rdf:resource="{concat($genderPrefix, $gender(current()), '')}"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:comment>ERROR : No URI was found for gender : "<xsl:value-of select="current()"/>". Please check the content of the @value or rely on our documentation to input correct format.</xsl:comment>
                            </xsl:otherwise>
                        </xsl:choose>
                    </crm:E7_Activity>                    
                </crm:P141_assigned>
            </crm:E13_Attribute_Assignment>
        </crm:P140i_was_attributed_by>   
    </xsl:template>
</xsl:stylesheet>
