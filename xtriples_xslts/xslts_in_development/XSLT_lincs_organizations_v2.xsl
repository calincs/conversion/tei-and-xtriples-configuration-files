<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:schema="https://schema.org/"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c/ns/1.0"
    xmlns:frbroo="D" 
    xmlns:owl="http://www.w3.org/TR/2004/REC-owl-guide-20040210/"
    xmlns:fa="http://www.w3.org/2005/xpath-functions"
    xmlns:crm="http://www.cidoc-crm.org/cidoc-crm/"
    xmlns:cwrc="http://sparql.cwrc.ca/ontologies/cwrc.html"
    xmlns:map="http://www.w3.org/2005/xpath-functions/map"
    exclude-result-prefixes="cwrc xsl schema rdf tei fa owl xs"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0" version="2.0"
       xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#">
    <xsl:output method="xml" encoding="UTF-16" indent="yes"/> <!--declare frbroo-->
    
    <xsl:template match="/">
        <rdf:RDF xml:lang="en"   
            xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
            xmlns:crm="http://www.cidoc-crm.org/cidoc-crm/"
            xmlns:skos="http://www.w3.org/2004/02/skos/core#"
            xmlns:temp_lincs_temp="http://temp.lincsproject.ca/"
            xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#">
            <xsl:apply-templates select="descendant::listOrg"/>
        </rdf:RDF>
    </xsl:template>
      
    <xsl:template match="listOrg">
        <xsl:apply-templates select="org"/>
    </xsl:template>
    
    <xsl:template match="org">
        <xsl:element name="{concat('crm:', 'E74_Group')}">
            <xsl:attribute name="rdf:about" select="child::orgName[@type='standard']/@ref"></xsl:attribute>
            
            <rdfs:label>
                <xsl:value-of select="normalize-space(orgName[@type='standard'])"/>
            </rdfs:label>  
                      
            <xsl:apply-templates select="orgName[@type='prefered']"/>
            <xsl:apply-templates select="orgName[@type='variant']"/>
            <!--About <trait>: CC & AD: There isn’t a application profile for this so we should leave it for now to be safe-->
           
        </xsl:element>       
    </xsl:template>

    
    <xsl:template match="orgName[@type='prefered']">
            <crm:P1_is_identified_by>
                <crm:E33_E41_Linguistic_Appellation rdf:about="uri:Name_of_Org">
                    <rdfs:label>Name of <xsl:value-of select="normalize-space(.)"/>
                    </rdfs:label>
                    <crm:P190_has_symbolic_content xml:lang="en">
                        <xsl:value-of select="normalize-space(.)"/>
                    </crm:P190_has_symbolic_content>
                    <crm:P2_has_type>
                        <crm:E55_Type rdf:about="uri:Type_of_name"/>
                        <!--What are the URIs for org names types?-->
                    </crm:P2_has_type>
                    
                   <xsl:apply-templates select="child::name"/> 
                   
                </crm:E33_E41_Linguistic_Appellation>
            </crm:P1_is_identified_by>
    </xsl:template>
    
    <xsl:template match="orgName[@type='variant']">
        <crm:P1_is_identified_by>
            <crm:E33_E41_Linguistic_Appellation rdf:about="uri:Name_of_Org">
                <rdfs:label>Name of <xsl:value-of select="normalize-space(.)"/>
                </rdfs:label>
                <crm:P190_has_symbolic_content xml:lang="en">
                    <xsl:value-of select="normalize-space(.)"/>
                </crm:P190_has_symbolic_content>
                <crm:P2_has_type>
                    <crm:E55_Type rdf:about="uri:Type_of_name"/>
                    <!--What are the URIs for org names types?-->
                </crm:P2_has_type>
                
                <xsl:apply-templates select="child::name"/> 
                                                                
            </crm:E33_E41_Linguistic_Appellation>
        </crm:P1_is_identified_by>
    </xsl:template>
 
    <xsl:template match="name">
        <crm:P106_is_composed_of xml:lang="en">
        <crm:E33_E41_Linguistic_Appellation rdf:about="uri:First_Name_of_Org">
            <rdfs:label>Name of <xsl:value-of select="normalize-space(.)"/>
            </rdfs:label>
            <crm:P190_has_symbolic_content xml:lang="en">
                <xsl:value-of select="normalize-space(.)"/>
            </crm:P190_has_symbolic_content>
            <crm:P2_has_type>
                <crm:E55_Type rdf:about="uri:Type_of_name"/>
                <!--What are the URIs for org names types?-->
            </crm:P2_has_type>
        </crm:E33_E41_Linguistic_Appellation>
        </crm:P106_is_composed_of>
    </xsl:template>
        
       
    
 
</xsl:stylesheet>
