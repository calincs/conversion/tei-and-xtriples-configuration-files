<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:schema="https://schema.org/"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c/ns/1.0"
    xmlns:frbroo="D" 
    xmlns:owl="http://www.w3.org/TR/2004/REC-owl-guide-20040210/"
    xmlns:fa="http://www.w3.org/2005/xpath-functions"
    xmlns:crm="http://www.cidoc-crm.org/cidoc-crm/"
    xmlns:cwrc="http://sparql.cwrc.ca/ontologies/cwrc.html"
    xmlns:map="http://www.w3.org/2005/xpath-functions/map"
    xmlns:dc="https://www.dublincore.org/specifications/dublin-core/dcmi-namespace/"
    exclude-result-prefixes="cwrc xsl schema rdf tei fa owl xs"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0" version="2.0"
       xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#">
    <xsl:output method="xml" encoding="UTF-16" indent="yes"/> <!--declare frbroo-->
    
    <xsl:template match="/">
        <rdf:RDF xml:lang="en"   
            xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
            xmlns:crm="http://www.cidoc-crm.org/cidoc-crm/"
            xmlns:skos="http://www.w3.org/2004/02/skos/core#"
            xmlns:temp_lincs_temp="http://temp.lincsproject.ca/"
            xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#">
            
            <xsl:apply-templates select="descendant::div[@type='poem']"/>
           
            
        </rdf:RDF>
    </xsl:template>
    
    <xsl:template match="div[@type='poem']">
        <xsl:element name="{concat('crm:', 'E73_Information_Object')}">
            <xsl:attribute name="rdf:about" select="idno/@n"/> 
            <xsl:comment>00 means insert idno when available, idno is the poem's URI</xsl:comment>
            <rdfs:label>Name of poem: <xsl:value-of select="normalize-space(descendant::title[@level='a'])"/></rdfs:label>
            
           <xsl:apply-templates select="author"/>
           <xsl:apply-templates select="refers_to_persName"/>
           <xsl:apply-templates select="refers_to_placeName"/>
               
        </xsl:element>
    </xsl:template>
    
    
    
    
    
   
   
    <crm:E73_Information_Object rdf:about=""><!--00 means insert idno when available, idno is the poem's URI-->
        <rdfs:label>Name of Glimpse</rdfs:label>
        <crm:P94i_was_created_by>
            <crm:E65_Creation>
                <rdfs:label>Creation of Glimpse</rdfs:label>
                <crm:P14_carried_out_by>
                    <crm:E21_Person rdf:about="http://viaf.org/viaf/78272387"/>
                    <rdfs:label>Name of Floris Clark McLaren</rdfs:label>
                </crm:P14_carried_out_by>
            </crm:E65_Creation>
        </crm:P94i_was_created_by>
        <crm:P67_refers_to>
            <xsl:element name="{concat('crm:', 'E21_Person')}">
                <xsl:attribute name="rdf:about" select="@ref"/>
                <rdfs:label>Name of <xsl:value-of select="normalize-space(.)"/></rdfs:label>
            </xsl:element>           
        </crm:P67_refers_to>
    </crm:E73_Information_Object>
        
        
    
   
  
    
    
</xsl:stylesheet>
