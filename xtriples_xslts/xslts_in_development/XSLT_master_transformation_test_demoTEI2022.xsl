<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:schema="https://schema.org/"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c/ns/1.0"
    xmlns:frbroo="D" 
    xmlns:owl="http://www.w3.org/TR/2004/REC-owl-guide-20040210/"
    xmlns:fa="http://www.w3.org/2005/xpath-functions"
    xmlns:crm="http://www.cidoc-crm.org/cidoc-crm/"
    xmlns:cwrc="http://sparql.cwrc.ca/ontologies/cwrc.html"
    xmlns:map="http://www.w3.org/2005/xpath-functions/map"
    exclude-result-prefixes="cwrc xsl schema rdf tei fa owl xs"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0" version="2.0"
       xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#">
    <xsl:output method="xml" encoding="UTF-16" indent="yes"/> <!--declare frbroo-->
    
    <xsl:template match="/">
        <rdf:RDF xml:lang="en"   
            xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
            xmlns:crm="http://www.cidoc-crm.org/cidoc-crm/"
            xmlns:skos="http://www.w3.org/2004/02/skos/core#"
            xmlns:temp_lincs_temp="http://temp.lincsproject.ca/"
            xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#">
            <xsl:apply-templates select="descendant::div[@type='HistoricalPeople']/listPerson"/>
        </rdf:RDF>
    </xsl:template>
    
    <xsl:template match="listPerson">
        <xsl:apply-templates select="person"/>
    </xsl:template>
    
    <xsl:template match="person">
        <crm:E21_Person rdf:about="uri:Name_of_Person">
            <rdfs:label>
                <xsl:value-of select="normalize-space(child::persName)"/>
            </rdfs:label>  
                      
                <xsl:apply-templates select="persName"/>
                <xsl:apply-templates select="birth"/>
                <xsl:apply-templates select="nationality"/>
                <xsl:apply-templates select="occupation"/>
            
        </crm:E21_Person>
    </xsl:template>
    
    <xsl:template match="persName">
            <crm:P1_is_identified_by>
                <crm:E33_E41_Linguistic_Appellation rdf:about="uri:Name_of_Person">
                    <rdfs:label>
                        <xsl:value-of select="normalize-space(.)"/>
                    </rdfs:label>
                    <crm:P190_has_symbolic_content xml:lang="en">
                        <xsl:value-of select="normalize-space(.)"/>
                    </crm:P190_has_symbolic_content>
                    <crm:P2_has_type>
                        <crm:E55_Type rdf:about="uri:cwrc.html#PersonalName"/>
                    </crm:P2_has_type>
                    
                    <xsl:apply-templates select="descendant::forename"/> 
                    <xsl:apply-templates select="descendant::surname"/>
                                        
                </crm:E33_E41_Linguistic_Appellation>
            </crm:P1_is_identified_by>
    </xsl:template>
    
 
    <xsl:template match="//descendant::forename">
        <crm:P106_is_composed_of xml:lang="en">
        <crm:E33_E41_Linguistic_Appellation rdf:about="uri:First_Name_of_Person">
            <rdfs:label>
                <xsl:value-of select="normalize-space(.)"/>
            </rdfs:label>
            <crm:P190_has_symbolic_content xml:lang="en">
                <xsl:value-of select="normalize-space(.)"/>
            </crm:P190_has_symbolic_content>
            <crm:P2_has_type>
                <crm:E55_Type rdf:about="uri:cwrc.html#Forename"/>
            </crm:P2_has_type>
        </crm:E33_E41_Linguistic_Appellation>
        </crm:P106_is_composed_of>
    </xsl:template>
        
       
    <xsl:template match="//descendant::surname">        
        <crm:P106_is_composed_of xml:lang="en">
            <crm:E33_E41_Linguistic_Appellation rdf:about="uri:Surname_of_Person">
                <rdfs:label>
                    <xsl:value-of select="normalize-space(.)"/>
                </rdfs:label>
                <crm:P190_has_symbolic_content xml:lang="en">
                    <xsl:value-of select="normalize-space(.)"/>
                </crm:P190_has_symbolic_content>
                <crm:P2_has_type>
                    <crm:E55_Type rdf:about="uri:cwrc.html#Surname"/>
                </crm:P2_has_type>
            </crm:E33_E41_Linguistic_Appellation>
        </crm:P106_is_composed_of>    
    </xsl:template>
    
    <xsl:template match="nationality">
        <crm:P74_has_current_or_former_residence>
            <crm:E53_Place rdf:about="uri:Name_of_Place">
                <crm:P1_is_identified_by>
                    <crm:E33_E41_Linguistic_Appellation rdf:about="uri:Name_of_Place">
                        <crm:P190_has_symbolic_content xml:lang="en">
                            <xsl:value-of select="normalize-space(.)"/>
                        </crm:P190_has_symbolic_content>
                        <crm:P2_has_type>
                            <crm:E55_Type rdf:about="http://sparql.cwrc.ca/ontologies/cwrc#nationality"/>
                        </crm:P2_has_type>                        
                    </crm:E33_E41_Linguistic_Appellation>
                </crm:P1_is_identified_by>
            </crm:E53_Place>
        </crm:P74_has_current_or_former_residence>
    </xsl:template>
    
    <xsl:template match="birth">
        <crm:P98i_was_born>
            <crm:E67_Birth rdf:about="uri:Name_of_Person"> 
                <!--CC: Should the person's URI be in here ? 
                    AD:Yes!
                    CC: or does the URI for the birth go here the birth URI will have to be minted by LINCS
                    EC : Yes!-->
                <rdfs:label> Birth of <xsl:value-of select="normalize-space(preceding-sibling::persName)"/></rdfs:label>
                <crm:P4_has_time-span>
                    <crm:E52_Time-Span rdf:about="uri:Time_Span"> 
                        <!-- CC : What uri goes in here ? 
                            AD: The time span -->
                        <rdfs:label>Datetime of birth of <xsl:value-of select="normalize-space(preceding-sibling::persName)"/></rdfs:label>
                        <!--This could have a type, but the previous declaration makes it clear this is a birth event
                            Does it?
                            <crm:P2_has_type>
                            <crm:E55_Type rdf:about="http://sparql.cwrc.ca/ontologies/cwrc#birth"/>
                        </crm:P2_has_type>-->
                        <crm:P82_at_some_time_within>Date_Test_1826</crm:P82_at_some_time_within>
                        <crm:P82b_end_of_the_end type="xsd:dateTime">Date_Test_1826-01-01T00:00:00</crm:P82b_end_of_the_end>
                        <!-- EC : this can be automated
                             AC: Will do 
                             *Connie writes this into the minutes*
                        -->
                        <crm:P82a_begin_of_the_begin type="xsd:dateTime">Date_Test_1826-12-31T23:59:59</crm:P82a_begin_of_the_begin> 
                        <!--CC: is the time now correct/ if it is, we can just ad the same  h/m/s information to the start and end--> 
                    </crm:E52_Time-Span>
                </crm:P4_has_time-span>
            </crm:E67_Birth>
        </crm:P98i_was_born>
    </xsl:template>
    
        <xsl:template match="occupation">
        <crm:P14i_performed> <!-- CC: if this isn't supposed to be f51_Pursuit, let us live edit it so that we have an example of what it should be4 -->
            <frbroo:F51_Pursuit rdf:about="uri:For_Pursuit"> <!-- CC: should this be the URI for writer? Where does that come from? Do we use the uris from the CWRC ontology ? y -->
                <rdfs:label> <xsl:value-of select="normalize-space(preceding-sibling::persName)"/> doing <xsl:value-of select="normalize-space(.)"/></rdfs:label>
                <crm:P2_has_type>
                    <crm:E55 rdf:about="uri:cwrc.html#writer"/>
                    <rdfs:label><xsl:value-of select="normalize-space(.)"/></rdfs:label>
                </crm:P2_has_type>                
            </frbroo:F51_Pursuit>
        </crm:P14i_performed>
        </xsl:template>
    
</xsl:stylesheet>
