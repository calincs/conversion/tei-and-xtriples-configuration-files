
→ view Google drive for working version :
[SchemaToRDF directory](https://drive.google.com/drive/folders/10jUaF5o2TwY4hIdwCg-bEr--_aYLWLjX?usp=sharing)

---                        

→ External ressource example based on :
http://www.constancecrompton.com/2020/7_ringwald_CraikSiteIndex_2020-05-27_all.xml


→ Focused on :

```xml
//tei:div[@type='HistoricalPeople']/tei:listPerson/tei:person
```



\---                        

→ Depending on :

* wikidata ontology for Nationality 
* cwrc : gender / occupation
* cidoc-crm : Person class / Events class 

\---                        

→ Ontologic questions still waiting solutions :

- [ ] timespan / dates question in events
- [ ] relationships in time ?



