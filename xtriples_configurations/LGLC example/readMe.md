
→ view Google drive for working version :
[SchemaToRDF directory](https://drive.google.com/drive/folders/1h2nQJW3vyz_b539aLzpY0-wC1WqSqOY8?usp=sharing)

---                        

→ External ressource example based on :
http://www.constancecrompton.com/2020/7_places.xml


→ Focused on :

```xml
//tei:place[@type='city']
//tei:place[@type='country']
//tei:place[@type='province'] /!\ USA state and Canadian province need to be separated
```



\---                        

→ Depending on :

* geoname : ParentFeatures
* wgs84_pos : for geo-coord
* cidoc-crm : Place class / Time span class 

\---                        

→ Ontologic questions still waiting solutions :

- [ ] places : how to speak about past places



